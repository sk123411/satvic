package store.satvicday.shop.adapter;

import android.app.Activity;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import store.satvicday.shop.R;
import store.satvicday.shop.activity.packs.adapter.PackCategoryAdapter;
import store.satvicday.shop.databinding.FragmentAllPacksBinding;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.Utils;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.packs.Pack;

import static store.satvicday.shop.helper.ApiConfig.AddMultipleProductInCart;


public class AllPacksFragment extends Fragment {



    private FragmentAllPacksBinding binding;
    private List<Pack> packArrayList;
    private Session session;
    private Activity activity;
    TextView daysLeftText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentAllPacksBinding.inflate(inflater);
        setHasOptionsMenu(true);
        session = new Session(getContext());

        binding.packsListView.setLayoutManager(new GridLayoutManager(getContext(), 3));

        GetPacks();

        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.all_packs));


        //Hide the toolbar
//        TextView daysLeftText = getActivity().findViewById(R.id.tbDaysLeftText);
//        daysLeftText.setVisibility(View.GONE);
//

//        toolbar.setTitle(getResources().getString(R.string.app_name));

//
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

        return binding.getRoot();
    }

    private void GetPacks() {


        binding.progress.setVisibility(View.VISIBLE);
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id", session.getData(Constant.ID));

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        JSONObject object = new JSONObject(response);
                        packArrayList = new ArrayList<>();
                        packArrayList.clear();
                        if (!object.getBoolean(Constant.ERROR)) {

                            packArrayList = Utils.decryptData(object,
                                    Pack.class);
                            binding.packsListView.setAdapter(
                                    new PackCategoryAdapter(packArrayList,getContext()));

                            binding.progress.setVisibility(View.GONE);

                        } else {
                        }
                        binding.progress.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, getActivity(), Constant.GET_PACKS, params, false);
    }



    @Override
    public void onPause() {
        super.onPause();

        TextView datsLeft = getActivity().findViewById(R.id.tbDaysLeftText);
        datsLeft.setVisibility(View.VISIBLE);
        if (Constant.CartValues.size() > 0) {
            AddMultipleProductInCart(session, activity, Constant.CartValues);

        }
    }

}
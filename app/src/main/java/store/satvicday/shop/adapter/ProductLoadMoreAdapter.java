package store.satvicday.shop.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PathDashPathEffect;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.LoginActivity;
import store.satvicday.shop.activity.MainActivity;
import store.satvicday.shop.activity.packs.AddCustomProductActivity;
import store.satvicday.shop.activity.packs.GetLeftPrice;
import store.satvicday.shop.activity.packs.PackProductActivity;
import store.satvicday.shop.activity.packs.adapter.CallGetLeftPrice;
import store.satvicday.shop.activity.subscribe.SubscribeActivity;
import store.satvicday.shop.fragment.PaymentFragment;
import store.satvicday.shop.fragment.ProductDetailFragment;
import store.satvicday.shop.fragment.ProductListFragment;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.DatabaseHelper;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.Utils;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.PriceVariation;
import store.satvicday.shop.model.Product;
import store.satvicday.shop.model.packs.Pack;

import static store.satvicday.shop.helper.ApiConfig.AddMultipleProductInCart;
import static store.satvicday.shop.helper.ApiConfig.AddOrRemoveFavorite;

public class ProductLoadMoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // for load more
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    public boolean isLoading;
    public int resource;
    public ArrayList<Product> mDataset = new ArrayList<>();
    SpannableString spannableString;
    Session session;
    boolean isLogin;
    DatabaseHelper databaseHelper;
    private Context context;
    private Activity activity;
    private boolean isFavorite;
    private boolean fromSection;
    PaymentFragment.TypePurchase typePurchase;

    public ProductLoadMoreAdapter(Context context, ArrayList<Product> myDataset, int resource, boolean fromSection ) {
        this.context = context;
        this.activity = (Activity) context;
        this.mDataset = myDataset;
        this.resource = resource;
        this.session = new Session(activity);
        isLogin = session.isUserLoggedIn();
        Constant.CartValues = new HashMap<>();
        databaseHelper = new DatabaseHelper(activity);
        Paper.init(activity);
        this.fromSection = fromSection;
        typePurchase = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE);



    }

    public void add(int position, Product item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(resource, parent, false);
            return new ViewHolderRow(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_progressbar, parent, false);

            return new ViewHolderLoading(view);
        }

        return null;
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderparent, final int position) {
        boolean isPriceVariationListEmpty = false;

        try {
            if (holderparent instanceof ViewHolderRow) {
                final ViewHolderRow holder = (ViewHolderRow) holderparent;
                holder.setIsRecyclable(false);
                final Product product = mDataset.get(position);

                final ArrayList<PriceVariation> priceVariations = product.getPriceVariations();
                if (priceVariations.size()==0){
                    Paper.init(activity);
                    isPriceVariationListEmpty=true;

                }else {
                    isPriceVariationListEmpty = false;
                }

                if (priceVariations.size() == 1) {
                    holder.spinner.setVisibility(View.GONE);
                }
                if (!product.getIndicator().equals("0")) {
                    holder.imgIndicator.setVisibility(View.VISIBLE);
                    if (product.getIndicator().equals("1"))
                        holder.imgIndicator.setImageResource(R.drawable.veg_icon);
                    else if (product.getIndicator().equals("2"))
                        holder.imgIndicator.setImageResource(R.drawable.non_veg_icon);
                }
                holder.productName.setText(Html.fromHtml(product.getName()));
                holder.imgThumb.setDefaultImageResId(R.drawable.placeholder);
                holder.imgThumb.setErrorImageResId(R.drawable.placeholder);

                holder.imgThumb.setImageUrl(product.getImage(), Constant.imageLoader);

                CustomAdapter customAdapter = new CustomAdapter(context, priceVariations, holder, product);
                holder.spinner.setAdapter(customAdapter);

                holder.lytmain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (typePurchase!= PaymentFragment.TypePurchase.PACK) {
                            if (Constant.CartValues.size() > 0) {
                                AddMultipleProductInCart(session, activity, Constant.CartValues);
                            }
                        }

                        Fragment fragment = new ProductDetailFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt("vpos", priceVariations.size() == 1 ? 0 :
                                holder.spinner.getSelectedItemPosition());
                        bundle.putString("id", product.getId());
                        bundle.putString("from", "regular");
                        bundle.putInt("position", position);
                        fragment.setArguments(bundle);

                        if (typePurchase== PaymentFragment.TypePurchase.PACK){
                            Paper.init(activity);
                            Paper.book().write(Constant.PR, product);


                            AddCustomProductActivity.fm.beginTransaction().
                                    add(R.id.container, fragment).addToBackStack(null).commit();
                        }else {


                            Paper.init(activity);
                            Paper.book().write(Constant.PR, product);
                            MainActivity.fm.beginTransaction().
                                    add(R.id.container, fragment).addToBackStack(null).commit();

                        }
                    }
                });

                PaymentFragment.TypePurchase typePurchase = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE);

                if (typePurchase==PaymentFragment.TypePurchase.PACK){


                    holder.subscribeButton.setVisibility(View.GONE);
                    holder.subscribeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            activity.startActivity(new Intent(v.getContext(), PackProductActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        }
                    });

                }else {
                    holder.subscribeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (session.isUserLoggedIn()) {
                                Paper.init(activity);
                                Paper.book().write(Constant.PRODUCT, product);

                                if (product.getPriceVariations() !=null){

                                    if (Double.parseDouble(product.getPriceVariations().get(0).getDiscounted_price())
                                            ==Double.parseDouble("0")){
                                        Paper.book().write(Constant.PRODUCT_VARIATION,  product.getPriceVariations().get(1));
                                    }else {
                                        Paper.book().write(Constant.PRODUCT_VARIATION, product.getPriceVariations().get(0));
                                    }
                                }

                                activity.startActivity(new Intent(v.getContext(), SubscribeActivity.class));
                            } else {

                                Paper.init(activity);
                                Paper.book().write(Constant.PRODUCT, product);

                                if (product.getPriceVariations() !=null){

                                    if (Double.parseDouble(product.getPriceVariations().get(0).getDiscounted_price())
                                            ==Double.parseDouble("0")){
                                        Paper.book().write(Constant.PRODUCT_VARIATION,  product.getPriceVariations().get(1));
                                    }else {
                                        Paper.book().write(Constant.PRODUCT_VARIATION, product.getPriceVariations().get(0));
                                    }
                                }

                                activity.startActivity(new Intent(v.getContext(), LoginActivity.class).putExtra("fromto", "subscribe"));

                            }
                        }
                    });
                }


                if (isLogin) {

                    if (!isPriceVariationListEmpty) {

                        if (typePurchase!= PaymentFragment.TypePurchase.PACK) {
                            holder.txtqty.setText(priceVariations.get(0).getCart_count());

                        }else {

                        }
                        }
                    if (product.isIs_favorite()) {
                        holder.imgFav.setImageResource(R.drawable.ic_is_favorite);
                    } else {
                        holder.imgFav.setImageResource(R.drawable.ic_is_not_favorite);
                    }
                    final Session session = new Session(activity);

                    holder.imgFav.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isFavorite = product.isIs_favorite();
                            if (isFavorite) {
                                isFavorite = false;
                                holder.imgFav.setImageResource(R.drawable.ic_is_not_favorite);
                            } else {
                                isFavorite = true;
                                holder.imgFav.setImageResource(R.drawable.ic_is_favorite);
                            }
                            product.setIs_favorite(isFavorite);
                            AddOrRemoveFavorite(activity, session, product.getId(), isFavorite);
                        }
                    });
                } else {

                    if (!isPriceVariationListEmpty) {
                        holder.txtqty.setText(databaseHelper.CheckOrderExists(product.getPriceVariations().get(0).getId(), product.getId()));
                    }
                    if (databaseHelper.getFavouriteById(product.getId())) {
                        holder.imgFav.setImageResource(R.drawable.ic_is_favorite);
                    } else {
                        holder.imgFav.setImageResource(R.drawable.ic_is_not_favorite);
                    }

                    holder.imgFav.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isFavorite = databaseHelper.getFavouriteById(product.getId());

                            if (isFavorite) {
                                isFavorite = false;
                                holder.imgFav.setImageResource(R.drawable.ic_is_not_favorite);
                            } else {
                                isFavorite = true;
                                holder.imgFav.setImageResource(R.drawable.ic_is_favorite);
                            }
                            databaseHelper.AddOrRemoveFavorite(product.getId(), isFavorite);
                        }
                    });
                }



                Paper.init(activity);

                if (product.getPriceVariations() != null&&!isPriceVariationListEmpty){

                    if (Double.parseDouble(priceVariations.get(0).getDiscounted_price())
                            == Double.parseDouble("0")){

                    }else {

                        Paper.book().write(Constant.PRODUCT_VARIATION, priceVariations.get(0));

                    }
                }
                if (!isPriceVariationListEmpty) {
                    if (typePurchase!= PaymentFragment.TypePurchase.PACK){
                    if (product.getEnabled_status().equals("1")){

                        holder.subscribeButton.setVisibility(View.VISIBLE);
                    }else {

                        holder.subscribeButton.setVisibility(View.GONE);
                    }

                    }else {

                        holder.subscribeButton.setVisibility(View.GONE);
                    }

                    SetSelectedData(holder, priceVariations.get(0));
                }
            } else if (holderparent instanceof ViewHolderLoading) {
                ViewHolderLoading loadingViewHolder = (ViewHolderLoading) holderparent;
                loadingViewHolder.progressBar.setIndeterminate(true);

            }

        }catch (Exception e){

            Toast.makeText(activity, activity.getResources().getString(R.string.error_finding_result),
                    Toast.LENGTH_LONG).show();

        }





    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    @Override
    public int getItemViewType(int position) {


        return mDataset.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public long getItemId(int position) {
        Product product = mDataset.get(position);
        if (product != null)
            return Integer.parseInt(product.getId());
        else
            return position;
    }

    public void setLoaded() {
        isLoading = false;
    }

    @SuppressLint("SetTextI18n")
    public void SetSelectedData(final ViewHolderRow holder, final PriceVariation extra) {

    try{


        holder.Measurement.setText(extra.getMeasurement() + extra.getMeasurement_unit_name());
        holder.productPrice.setText(activity.getResources().getString(R.string.offer_price) + Constant.SETTING_CURRENCY_SYMBOL + extra.getProductPrice());

        if (session.isUserLoggedIn()) {

            if (typePurchase!= PaymentFragment.TypePurchase.PACK) {

                if (Constant.CartValues.containsKey(extra.getId())) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        holder.txtqty.setText("" + Constant.CartValues.get(extra.getId()));
                    }
                }else {
                    holder.txtqty.setText(extra.getCart_count());

                }
            }else {
                if (Constant.PackCartValues.containsKey(extra.getId())) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        holder.txtqty.setText("" + Constant.PackCartValues.get(extra.getId()));
                    }
                }else {
                    holder.txtqty.setText("0");
                }

            }
        } else {
            if (session.getData(extra.getId()) != null) {
                holder.txtqty.setText(session.getData(extra.getId()));
            } else {
                holder.txtqty.setText(extra.getCart_count());
            }
        }

        holder.txtstatus.setText(extra.getServe_for());

        if (extra.getDiscounted_price().equals("0") || extra.getDiscounted_price().equals("")) {
            holder.originalPrice.setVisibility(View.INVISIBLE);
            holder.showDiscount.setVisibility(View.INVISIBLE);

            holder.productPrice.setText(activity.getResources().getString(R.string.mrp) + Constant.SETTING_CURRENCY_SYMBOL + extra.getProductPrice());
        } else {
            spannableString = new SpannableString(activity.getResources().getString(R.string.mrp) + Constant.SETTING_CURRENCY_SYMBOL + extra.getPrice());
            spannableString.setSpan(new StrikethroughSpan(), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.originalPrice.setText(spannableString);
            double diff = Double.parseDouble(extra.getPrice()) - Double.parseDouble(extra.getProductPrice());
            holder.showDiscount.setText(activity.getResources().getString(R.string.you_save) + Constant.SETTING_CURRENCY_SYMBOL + diff + extra.getDiscountpercent());
        }

        if (extra.getServe_for().equalsIgnoreCase(Constant.SOLDOUT_TEXT)) {
            holder.txtstatus.setVisibility(View.VISIBLE);
            holder.txtstatus.setTextColor(Color.RED);
            holder.qtyLyt.setVisibility(View.GONE);
        } else {
            holder.txtstatus.setVisibility(View.GONE);
            holder.qtyLyt.setVisibility(View.VISIBLE);
        }

        if (isLogin) {

            Paper.init(activity);
            PaymentFragment.TypePurchase typePurchase = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE);


            if (typePurchase== PaymentFragment.TypePurchase.PACK){
                if (Constant.PackCartValues.containsKey(extra.getId())) {
                    holder.txtqty.setText("" + Constant.PackCartValues.get(extra.getId()));
                } else {
                    holder.txtqty.setText("0");
                }
            }else {

                if (Constant.CartValues.containsKey(extra.getId())) {
                    holder.txtqty.setText("" + Constant.CartValues.get(extra.getId()));
                } else {
                    holder.txtqty.setText(extra.getCart_count());
                }
            }
            Paper.init(activity);
            PaymentFragment.TypePurchase isPack = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE);
            holder.imgAdd.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View view) {
                    int count = Integer.parseInt(holder.txtqty.getText().toString());
                    if (count < Float.parseFloat(extra.getStock())) {
                        if (count < Constant.MAX_PRODUCT_LIMIT) {
                            count++;
                            holder.txtqty.setText("" + count);

                            if (isPack == PaymentFragment.TypePurchase.PACK) {
                              holder.imgAdd.setEnabled(false);
                                Pack pack = Paper.book().read(Constant.PACK_DETAILS);
                                int finalCount = count;
                                getLeftPrice(pack, new GetLeftPrice() {
                                    @Override
                                    public void onSuccess(String leftPrice) {

                                        addProductInPack(pack, extra,
                                                leftPrice, session.getData(Constant.ID),
                                                finalCount, holder.txtqty,holder.imgAdd);

                                    }
                                });

                                if (Constant.PackCartValues.containsKey(extra.getId())) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        Constant.PackCartValues.replace(extra.getId(), "" + count);
                                    }


                                } else {
                                    Constant.PackCartValues.put(extra.getId(), "" + count);
                                }

                            } else {

                                ProductListFragment.btnCart.setVisibility(View.VISIBLE);

                                if (Constant.CartValues.containsKey(extra.getId())) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        Constant.CartValues.replace(extra.getId(), "" + count);
                                    }


                                } else {
                                    Constant.CartValues.put(extra.getId(), "" + count);
                                }
                                ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                            }

                        } else {
                            Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            holder.imgMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int count = Integer.parseInt(holder.txtqty.getText().toString());
                    if (!(count <= 0)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            if (count != 0) {
                                count--;
                                holder.txtqty.setText("" + count);
                            }

                            if (isPack == PaymentFragment.TypePurchase.PACK) {

                                holder.imgMinus.setEnabled(false);
                                Pack pack = Paper.book().read(Constant.PACK_DETAILS);

                                getLeftPrice(pack, new GetLeftPrice() {
                                    @Override
                                    public void onSuccess(String leftPrice) {

                                        removeProductFromPack(pack, extra,session.getData(Constant.ID),holder.imgMinus);

                                        //Add product in pack here

                                    }
                                });

                                if (Constant.PackCartValues.containsKey(extra.getId())) {

                                        Constant.PackCartValues.replace(extra.getId(), "" + count);


                                } else {
                                    Constant.PackCartValues.put(extra.getId(), "" + count);
                                }


                            } else {

                                ProductListFragment.btnCart.setVisibility(View.VISIBLE);

                                if (Constant.CartValues.containsKey(extra.getId())) {
                                    Constant.CartValues.replace(extra.getId(), "" + count);
                                } else {
                                    Constant.CartValues.put(extra.getId(), "" + count);
                                }
                            }


                            ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                        }
                    }
                }
            });
        } else {

            holder.txtqty.setText(databaseHelper.CheckOrderExists(extra.getId(), extra.getProduct_id()));

            holder.imgAdd.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View view) {
                    int count = Integer.parseInt(holder.txtqty.getText().toString());
                    if (count < Float.parseFloat(extra.getStock())) {
                        if (count < Constant.MAX_PRODUCT_LIMIT) {
                            count++;
                            holder.txtqty.setText("" + count);
                            databaseHelper.AddOrderData(extra.getId(), extra.getProduct_id(), "" + count);
                            databaseHelper.getTotalItemOfCart(activity);
                            activity.invalidateOptionsMenu();
                        } else {
                            Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            holder.imgMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int count = Integer.parseInt(holder.txtqty.getText().toString());
                    if (!(count <= 0)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            if (count != 0) {
                                count--;
                                holder.txtqty.setText("" + count);
                            }
                            databaseHelper.AddOrderData(extra.getId(), extra.getProduct_id(), "" + count);
                            databaseHelper.getTotalItemOfCart(activity);
                            activity.invalidateOptionsMenu();
                        }
                    }
                }
            });



        }

    }catch (Exception e){
        Toast.makeText(activity, activity.getResources().getString(R.string.error_finding_result),
                Toast.LENGTH_LONG).show();

    }






    }

    private void removeProductFromPack(Pack pack, PriceVariation extra,String user_ID,ImageButton minusButton) {

        HashMap<String,String> map = new HashMap<>();
        map.put(Constant.PACK_ID, pack.getId());
        map.put(Constant.USER_ID,user_ID);

        ArrayList<Pack> packs = new ArrayList<>();
        ArrayList<Pack.PackProduct> packProducts= new ArrayList<>();


        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {


                try {

                    minusButton.setEnabled(true);

                    packs.addAll(Utils.decryptData(new JSONObject(message),Pack.class));

                    for (Pack pack1:packs){

                        packProducts.addAll(pack1.getProduct());

                    }

                    for (Pack.PackProduct packProduct: packProducts){
                        if (extra.getProduct_id().equals(packProduct.getId())){
                            deleteProduct(packProduct.getVariable_id(), new CallGetLeftPrice() {
                                @Override
                                public void onSuccess(String result) {

                                    getLeftPrice(pack, new GetLeftPrice() {
                                        @Override
                                        public void onSuccess(String price) {

                                                    if (Integer.parseInt(price)>=0) {
                                                        AddCustomProductActivity.binding.toolbar.tbDaysLeftText.setText(
                                                                context.getResources().getString(R.string.rupee_symbol) + " " + price + "\n"
                                                                        + context.getResources().getString(R.string.left));
                                                    }
                                        }
                                    });
                                }
                            });

                            break;
                        }else {
                  //          Toast.makeText(context,"Product not added in the pack", Toast.LENGTH_SHORT).show();

                        }
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                    minusButton.setEnabled(true);

                }

            }
        },activity, Constant.GET_VARIABLE_PRODUCTS,map,true);

    }

    private void deleteProduct(String variable_id, CallGetLeftPrice callGetLeftPrice) {

        HashMap<String,String> params = new HashMap<>();
        params.put("delete_variable_product","1");
        params.put(Constant.ID,variable_id);


        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                Toast.makeText(context,"Product Cleared", Toast.LENGTH_SHORT).show();
                callGetLeftPrice.onSuccess("call");

            }
        },activity,Constant.DELETE_PRODUCT_VARIABLE,params,true);



    }

    private void addProductInPack(Pack pack,
                                  PriceVariation extra,
                                  String leftPrice, String userid,
                                  int count,
                                  TextView counText,
                                  ImageButton imgAdd

    ) {
        HashMap<String, String> map = new HashMap<>();
        map.put(Constant.PACK_ID, pack.getId());
        map.put(Constant.PRODUCT_ID, extra.getProduct_id());
        map.put(Constant.USER_ID, userid);
        map.put("leftprice", leftPrice);

        if (Double.parseDouble(leftPrice)>=Double.parseDouble(extra.getDiscounted_price())) {

            ApiConfig.RequestToVolley(new VolleyCallback() {
                @Override
                public void onSuccess(boolean result, String message) {


                    try {
                        Toast.makeText(context,"Product added",Toast.LENGTH_SHORT).show();
                        getLeftPrice(pack, new GetLeftPrice() {
                            @Override
                            public void onSuccess(String result) {

                                imgAdd.setEnabled(true);

                                if (Integer.parseInt(result)>=0) {
                                  AddCustomProductActivity.binding.toolbar.tbDaysLeftText.setText(
                                        context.getResources().getString(R.string.rupee_symbol) +" "+  result+"\n"
                                                + context.getResources().getString(R.string.left));
                                }

                            }
                        });
                    }catch (Exception e){

                        imgAdd.setEnabled(true);

                    }



                }
            }, activity, Constant.ADD_VARIABLE_PRODUCT,map,true );

        }else {

            count--;
            counText.setText(String.valueOf(count));
            Toast.makeText(activity,activity.getResources().getString(R.string.product_amount_more),Toast.LENGTH_SHORT).show();

        }
    }

    private void getLeftPrice(Pack pack, GetLeftPrice getLeftPrice) {

        final int[] price = {0};
        ArrayList<Pack> packs = new ArrayList<>();
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id", session.getData(Constant.ID));

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                try {
                    packs.addAll(Utils.decryptData(new JSONObject(message), Pack.class));

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        packs.stream().filter(
                                pack1 -> {

                                    if (pack1.getId().equals(pack.getId())) {

                                        getLeftPrice.onSuccess(String.valueOf(pack1.getLeftPrice()));


                                        return true;
                                    }
                                    return false;
                                }
                        ).collect(Collectors.toList());
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, activity, Constant.GET_PACKS, params, true);

    }

    private class ViewHolderLoading extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ViewHolderLoading(View view) {
            super(view);
            progressBar = view.findViewById(R.id.itemProgressbar);


//            if (fromSection){
//                progressBar.setVisibility(View.GONE);
//            }else {
//                progressBar.setVisibility(View.VISIBLE);
//            }
        }
    }

    public class ViewHolderRow extends RecyclerView.ViewHolder {
        public ImageButton imgAdd, imgMinus;
        TextView productName, productPrice, txtqty, Measurement, showDiscount, originalPrice, txtstatus;
        NetworkImageView imgThumb;
        ImageView imgFav, imgIndicator;
        RelativeLayout lytmain;
        AppCompatSpinner spinner;
        LinearLayout qtyLyt;
        TextView subscribeButton;

        public ViewHolderRow(View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.productName);
            productPrice = itemView.findViewById(R.id.txtprice);
            showDiscount = itemView.findViewById(R.id.showDiscount);
            originalPrice = itemView.findViewById(R.id.txtoriginalprice);
            Measurement = itemView.findViewById(R.id.txtmeasurement);
            txtstatus = itemView.findViewById(R.id.txtstatus);
            imgThumb = itemView.findViewById(R.id.imgThumb);
            imgIndicator = itemView.findViewById(R.id.imgIndicator);
            imgAdd = itemView.findViewById(R.id.btnaddqty);
            imgMinus = itemView.findViewById(R.id.btnminusqty);
            txtqty = itemView.findViewById(R.id.txtqty);
            qtyLyt = itemView.findViewById(R.id.qtyLyt);
            imgFav = itemView.findViewById(R.id.imgFav);
            lytmain = itemView.findViewById(R.id.lytmain);
            spinner = itemView.findViewById(R.id.spinner);
            subscribeButton = itemView.findViewById(R.id.pdSubscribeButton);



        }

    }

    public class CustomAdapter extends BaseAdapter {
        Context context;
        ArrayList<PriceVariation> extraList;
        LayoutInflater inflter;
        ViewHolderRow holder;
        Product product;

        public CustomAdapter(Context applicationContext, ArrayList<PriceVariation> extraList, ViewHolderRow holder, Product product) {
            this.context = applicationContext;
            this.extraList = extraList;
            this.holder = holder;
            this.product = product;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return extraList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @SuppressLint({"SetTextI18n", "ViewHolder", "InflateParams"})
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.lyt_spinner_item, null);
            TextView measurement = view.findViewById(R.id.txtmeasurement);
            TextView price = view.findViewById(R.id.txtprice);


            PriceVariation extra = extraList.get(i);
            measurement.setText(extra.getMeasurement() + " " + extra.getMeasurement_unit_name());
            price.setText(Constant.SETTING_CURRENCY_SYMBOL + extra.getProductPrice());

            if (extra.getServe_for().equalsIgnoreCase(Constant.SOLDOUT_TEXT)) {
                measurement.setTextColor(context.getResources().getColor(R.color.red));
                price.setTextColor(context.getResources().getColor(R.color.red));
            } else {
                measurement.setTextColor(context.getResources().getColor(R.color.black));
                price.setTextColor(context.getResources().getColor(R.color.black));
            }

            holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    PriceVariation priceVariation = extraList.get(i);

                    Paper.init(activity);

                    Paper.book().write(Constant.PRODUCT_VARIATION, priceVariation);

                    SetSelectedData(holder, priceVariation);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });




            return view;
        }
    }

}

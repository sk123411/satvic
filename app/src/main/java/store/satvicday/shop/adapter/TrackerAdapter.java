package store.satvicday.shop.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import store.satvicday.shop.R;
import store.satvicday.shop.activity.DisbaleCancelButtonInterface;
import store.satvicday.shop.activity.MainActivity;
import store.satvicday.shop.fragment.TrackerDetailFragment;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.OrderTracker;

public class TrackerAdapter extends RecyclerView.Adapter<TrackerAdapter.CartItemHolder> {

    Activity activity;
    ArrayList<OrderTracker> orderTrackerArrayList;

    public TrackerAdapter(Activity activity, ArrayList<OrderTracker> orderTrackerArrayList) {
        this.activity = activity;
        this.orderTrackerArrayList = orderTrackerArrayList;
    }

    @Override
    public CartItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_trackorder, null);
        CartItemHolder cartItemHolder = new CartItemHolder(v);
        return cartItemHolder;
    }

    @Override
    public void onBindViewHolder(final CartItemHolder holder, final int position) {
        final OrderTracker order = orderTrackerArrayList.get(position);
        holder.txtorderid.setText(order.getOrder_id());
        String[] date = order.getDate_added().split("\\s+");
        holder.txtorderdate.setText(date[0]);

        holder.carddetail.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Fragment fragment = new TrackerDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id", "");
                bundle.putSerializable("model", order);

                fragment.setArguments(bundle);
                MainActivity.fm.beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();
            }
        });

        if (order.getStatus().equalsIgnoreCase("cancelled")) {
            holder.lyttracker.setVisibility(View.GONE);
        } else {
            if (order.getStatus().equals("returned")) {


                holder.l4.setVisibility(View.VISIBLE);
                holder.returnLyt.setVisibility(View.VISIBLE);
            }
            holder.lyttracker.setVisibility(View.VISIBLE);
            ApiConfig.setOrderTrackerLayout(activity, order, holder);
        }


        if (order.getStatus().equals("delivered")){

            holder.cardfeedback.setVisibility(View.VISIBLE);


            holder.cardfeedback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Dialog dialog = new Dialog(activity);
                    dialog.setContentView(R.layout.feedback_rating);
                    EditText ratingText = dialog.findViewById(R.id.frEditText);
                    RatingBar ratingBar = dialog.findViewById(R.id.frRating);
                    Button send = dialog.findViewById(R.id.frSendFeedback);
                    dialog.show();

                    send.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String ratingBarText = ratingText.getText().toString()+ "Rating: "
                                    + ratingBar.getRating();

                            if (ratingBar.getRating()>0){

                                sendFeedback(order.getOrder_id(), order.getUser_id(),ratingBarText,dialog);

                            }else {

                            }


                        }
                    });


                }
            });
        }else {
            holder.cardfeedback.setVisibility(View.GONE);

        }


        if (order.getSubscribe().equals("1")||!order.getPack_date().isEmpty()){
            holder.delieveryLayout.setVisibility(View.VISIBLE);

            if (!order.getPack_date().isEmpty()){

                holder.txtOrderDelDate.setText(order.getPack_date());
            }else {
                holder.txtOrderDelDate.setText(order.getSubscribed_date());

            }

        }else {
            holder.delieveryLayout.setVisibility(View.GONE);

        }


        holder.recyclerView.setAdapter(new ItemsAdapter(activity, orderTrackerArrayList.get(position).itemsList, false));
        holder.recyclerView.setNestedScrollingEnabled(false);

    }

    private void sendFeedback(String order_id, String user_id, String ratingBarText, Dialog dialog) {


        Map<String, String> params = new HashMap<String, String>();
        params.put(Constant.UPDATE_ORDER_STATUS, Constant.GetVal);
        params.put(Constant.ID, order_id);
        params.put(Constant.USER_ID, user_id);
        params.put(Constant.FEEDBACK, ratingBarText);

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                // System.out.println("=================*cancelorder- " + response);
                if (result) {
                    try {
                        JSONObject object = new JSONObject(response);
                        if (!object.getBoolean(Constant.ERROR)) {
                            Toast.makeText(activity, activity.getResources().getString(R.string.feedback_sent), Toast.LENGTH_LONG).show();

                            dialog.dismiss();
                        }
                        Toast.makeText(activity, object.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, activity, Constant.ORDERPROCESS_URL, params, false);





    }




    @Override
    public int getItemCount() {

        return orderTrackerArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class CartItemHolder extends RecyclerView.ViewHolder {
        TextView txtorderid, txtorderdate, txtOrderDelDate;
        NetworkImageView imgorder;
        LinearLayout lyttracker, returnLyt;
        CardView carddetail, cardfeedback;
        RecyclerView recyclerView;
        View l4;

        LinearLayout delieveryLayout;
        public CartItemHolder(View itemView) {
            super(itemView);
            txtorderid = itemView.findViewById(R.id.txtorderid);
            txtorderdate = itemView.findViewById(R.id.txtorderdate);
            imgorder = itemView.findViewById(R.id.imgorder);
            lyttracker = itemView.findViewById(R.id.lyttracker);
            l4 = itemView.findViewById(R.id.l4);
            returnLyt = itemView.findViewById(R.id.returnLyt);
            carddetail = itemView.findViewById(R.id.carddetail);
            recyclerView = itemView.findViewById(R.id.recyclerView);
            cardfeedback = itemView.findViewById(R.id.cardfeedback);
            delieveryLayout = itemView.findViewById(R.id.lytodelievery);
            txtOrderDelDate = itemView.findViewById(R.id.txtorderdelDet);

            recyclerView.setLayoutManager(new LinearLayoutManager(activity));

        }
    }

}

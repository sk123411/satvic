package store.satvicday.shop.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import store.satvicday.shop.R;
import store.satvicday.shop.fragment.CartFragment;
import store.satvicday.shop.helper.AppController;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.model.Cart;


public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // for load more
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    public boolean isLoading;
    Activity activity;
    ArrayList<Cart> items;
    CartFragment cartFragment;


    public CartAdapter(Activity activity, ArrayList<Cart> items, CartFragment cartFragment) {
        this.activity = activity;
        this.items = items;
        this.cartFragment = cartFragment;

    }

    public void add(int position, Cart item) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void setLoaded() {
        isLoading = false;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.lyt_cartlist, parent, false);
            return new ProductHolderItems(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_progressbar, parent, false);
            return new ViewHolderLoading(view);
        }

        return null;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderparent, final int position) {

        if (holderparent instanceof ProductHolderItems) {
            final ProductHolderItems holder = (ProductHolderItems) holderparent;
            final Cart cart = items.get(position);

            double price = Double.parseDouble(cart.getItems().get(0).getDiscounted_price());

            holder.imgproduct.setImageUrl(cart.getItems().get(0).getImage(), Constant.imageLoader);
            holder.txtproductname.setText(cart.getItems().get(0).getName());
            holder.txtmeasurement.setText(cart.getItems().get(0).getMeasurement() + "\u0020" + cart.getItems().get(0).getUnit());
            holder.txtprice.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(Double.parseDouble(cart.getItems().get(0).getDiscounted_price())));


            if (cart.getItems().get(0).getDiscounted_price().equals("0")) {
                holder.txtprice.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(Double.parseDouble(cart.getItems().get(0).getPrice())));
                price = Double.parseDouble(cart.getItems().get(0).getPrice());
            } else if (!cart.getItems().get(0).getDiscounted_price().equalsIgnoreCase(cart.getItems().get(0).getPrice())) {
                holder.txtoriginalprice.setPaintFlags(holder.txtoriginalprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.txtoriginalprice.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(Double.parseDouble(cart.getItems().get(0).getPrice())));
            }

            holder.txtQuantity.setText(cart.getQty());


            if (Constant.CheckOutCartValues.containsKey(cart.getItems().get(0).getId())) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Constant.CheckOutCartValues.replace(cart.getItems().get(0).getId(), "" + cart.getQty());
                } else {
                    Constant.CheckOutCartValues.remove(cart.getItems().get(0).getProduct_variant_id());
                    Constant.CheckOutCartValues.put(cart.getItems().get(0).getId(), "" + cart.getQty());
                }
            } else {
                Constant.CheckOutCartValues.put(cart.getItems().get(0).getId(), "" +cart.getQty());
            }









            holder.txttotalprice.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(price * Integer.parseInt(cart.getQty())));

            final double finalPrice = price;
            holder.btnaddqty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (AppController.isConnected(activity)) {
                        if (!(Integer.parseInt(holder.txtQuantity.getText().toString()) >= Float.parseFloat(cart.getItems().get(0).getStock()))) {
                            if (!(Integer.parseInt(holder.txtQuantity.getText().toString()) + 1 > Constant.MAX_PRODUCT_LIMIT)) {
                                int count = Integer.parseInt(holder.txtQuantity.getText().toString());
                                count++;
                                holder.txtQuantity.setText("" + count);
                                holder.txttotalprice.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(finalPrice * count));
                                Constant.FLOAT_TOTAL_AMOUNT = Constant.FLOAT_TOTAL_AMOUNT + finalPrice;
                                if (CartFragment.values.containsKey(items.get(position).getProduct_variant_id())) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        CartFragment.values.replace(items.get(position).getProduct_variant_id(), "" + count);
                                    } else {
                                        CartFragment.values.remove(items.get(position).getProduct_variant_id());
                                        CartFragment.values.put(items.get(position).getProduct_variant_id(), "" + count);
                                    }
                                } else {
                                    CartFragment.values.put(items.get(position).getProduct_variant_id(), "" + count);
                                }



                                if (Constant.CheckOutCartValues.containsKey(items.get(position).getProduct_variant_id())) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        Constant.CheckOutCartValues.replace(items.get(position).getProduct_variant_id(), "" + count);
                                    } else {
                                        Constant.CheckOutCartValues.remove(items.get(position).getProduct_variant_id());
                                        Constant.CheckOutCartValues.put(items.get(position).getProduct_variant_id(), "" + count);
                                    }
                                } else {
                                    Constant.CheckOutCartValues.put(items.get(position).getProduct_variant_id(), "" +count);
                                }




                                if (Constant.CartValues.containsKey(items.get(position).getProduct_variant_id())) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        Constant.CartValues.replace(items.get(position).getProduct_variant_id(), "" + count);
                                    }


                                } else {
                                    Constant.CartValues.put(items.get(position).getProduct_variant_id(), "" + count);
                                }










                                CartFragment.SetData();
                            } else {
                                Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });

            holder.btnminusqty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (AppController.isConnected(activity)) {
                        if (Integer.parseInt(holder.txtQuantity.getText().toString()) > 1) {
                            int count = Integer.parseInt(holder.txtQuantity.getText().toString());
                            count--;
                            holder.txtQuantity.setText("" + count);
                            holder.txttotalprice.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(finalPrice * count));
                            Constant.FLOAT_TOTAL_AMOUNT = Constant.FLOAT_TOTAL_AMOUNT - finalPrice;
                            if (CartFragment.values.containsKey(items.get(position).getProduct_variant_id())) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    CartFragment.values.replace(items.get(position).getProduct_variant_id(), "" + count);
                                } else {
                                    CartFragment.values.remove(items.get(position).getProduct_variant_id());
                                    CartFragment.values.put(items.get(position).getProduct_variant_id(), "" + count);
                                }
                            } else {
                                CartFragment.values.put(items.get(position).getProduct_variant_id(), "" + count);
                            }




                            if (Constant.CheckOutCartValues.containsKey(items.get(position).getProduct_variant_id())) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    Constant.CheckOutCartValues.replace(items.get(position).getProduct_variant_id(), "" + count);
                                } else {
                                    Constant.CheckOutCartValues.remove(items.get(position).getProduct_variant_id());
                                    Constant.CheckOutCartValues.put(items.get(position).getProduct_variant_id(), "" + count);
                                }
                            } else {
                                Constant.CheckOutCartValues.put(items.get(position).getProduct_variant_id(), "" +count);
                            }


                            if (Constant.CartValues.containsKey(items.get(position).getProduct_variant_id())) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    Constant.CartValues.replace(items.get(position).getProduct_variant_id(), "" + count);
                                }


                            } else {
                                Constant.CartValues.put(items.get(position).getProduct_variant_id(), "" + count);
                            }




                            CartFragment.SetData();
                        }
                    }
                }
            });

            holder.imgdelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (AppController.isConnected(activity)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        builder.setTitle(activity.getResources().getString(R.string.deleteproducttitle));
                        builder.setIcon(android.R.drawable.ic_delete);
                        builder.setMessage(activity.getResources().getString(R.string.deleteproductmsg));

                        builder.setCancelable(false);
                        builder.setPositiveButton(activity.getResources().getString(R.string.remove), new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                Constant.FLOAT_TOTAL_AMOUNT -= (finalPrice * Integer.parseInt(holder.txtQuantity.getText().toString()));

                                if (CartFragment.values.containsKey(items.get(position).getProduct_variant_id())) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        CartFragment.values.replace(items.get(position).getProduct_variant_id(), "0");
                                    } else {
                                        CartFragment.values.remove(items.get(position).getProduct_variant_id());
                                        CartFragment.values.put(items.get(position).getProduct_variant_id(), "0");
                                    }
                                } else {
                                    CartFragment.values.put(items.get(position).getProduct_variant_id(), "0");
                                }



                                if (Constant.CheckOutCartValues.containsKey(items.get(position).getProduct_variant_id())) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        Constant.CheckOutCartValues.replace(items.get(position).getProduct_variant_id(), "" + "0");
                                    } else {
                                        Constant.CheckOutCartValues.remove(items.get(position).getProduct_variant_id());
                                        Constant.CheckOutCartValues.put(items.get(position).getProduct_variant_id(), "" + "0");
                                    }
                                } else {
                                    Constant.CheckOutCartValues.put(items.get(position).getProduct_variant_id(), "" +"0");
                                }


                                if (Constant.CartValues.containsKey(items.get(position).getProduct_variant_id())) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        Constant.CartValues.replace(items.get(position).getProduct_variant_id(), "" + "0");
                                    }


                                } else {
                                    Constant.CartValues.put(items.get(position).getProduct_variant_id(), "" + "0");
                                }




//                                CartFragment.SetData();

                                items.remove(cart);
                                notifyItemChanged(position, items.size());

                                notifyDataSetChanged();

                                CartFragment.SetData();


                                Constant.TOTAL_CART_ITEM = getItemCount();
                                activity.invalidateOptionsMenu();
//                               // cartFragment.getCartData();
//                                CartFragment.SetData();




                                if (getItemCount() == 0) {
                                    CartFragment.lytempty.setVisibility(View.VISIBLE);
                                    CartFragment.lyttotal.setVisibility(View.GONE);
                                }
                            }

                        });

                        builder.setNegativeButton(activity.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }

                }
            });

        } else if (holderparent instanceof ViewHolderLoading) {
            ViewHolderLoading loadingViewHolder = (ViewHolderLoading) holderparent;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }




    @Override
    public long getItemId(int position) {
        Cart cart = items.get(position);
        if (cart != null)
            return Integer.parseInt(cart.getId());
        else
            return position;
    }

    static class ViewHolderLoading extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ViewHolderLoading(View view) {
            super(view);
            progressBar = view.findViewById(R.id.itemProgressbar);
        }

        public void refreshData() {

        }
    }

    public static class ProductHolderItems extends RecyclerView.ViewHolder {
        NetworkImageView imgproduct;
        ImageView imgdelete, btnminusqty, btnaddqty;
        TextView txtproductname, txtmeasurement, txtprice, txtoriginalprice, txtQuantity, txttotalprice;

        public ProductHolderItems(@NonNull View itemView) {
            super(itemView);
            imgproduct = itemView.findViewById(R.id.imgproduct);

            imgdelete = itemView.findViewById(R.id.imgdelete);
            btnminusqty = itemView.findViewById(R.id.btnminusqty);
            btnaddqty = itemView.findViewById(R.id.btnaddqty);

            txtproductname = itemView.findViewById(R.id.txtproductname);
            txtmeasurement = itemView.findViewById(R.id.txtmeasurement);
            txtprice = itemView.findViewById(R.id.txtprice);
            txtoriginalprice = itemView.findViewById(R.id.txtoriginalprice);
            txtQuantity = itemView.findViewById(R.id.txtQuantity);
            txttotalprice = itemView.findViewById(R.id.txttotalprice);
        }

        public void refreshData() {



        }

    }
}
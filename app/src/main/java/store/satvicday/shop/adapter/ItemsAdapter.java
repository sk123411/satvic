package store.satvicday.shop.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.dynamic.IFragmentWrapper;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.DisbaleCancelButtonInterface;
import store.satvicday.shop.activity.MainActivity;
import store.satvicday.shop.fragment.TrackerDetailFragment;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.OrderTracker;

import static store.satvicday.shop.fragment.TrackerDetailFragment.btnCancel;
import static store.satvicday.shop.fragment.TrackerDetailFragment.btnReturn;
import static store.satvicday.shop.fragment.TrackerDetailFragment.pBar;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.CartItemHolder> {

    Activity activity;
    ArrayList<OrderTracker> orderTrackerArrayList;
    String from = "";
    Session session;
    HashMap<String, String> packAll = new HashMap<>();

    boolean disableButton;
    TrackerDetailFragment trackerDetailFragment;
    private ArrayList<OrderTracker> tempOrderList;
    private boolean isLastItem = false;
    private HashMap<String, String> temporaruList = new HashMap<>();
    int count = 0;

    public ItemsAdapter(Activity activity, ArrayList<OrderTracker> orderTrackerArrayList, Boolean disableButton) {
        this.activity = activity;
        this.orderTrackerArrayList = orderTrackerArrayList;
        this.disableButton = disableButton;

    }


    public ItemsAdapter(Activity activity, ArrayList<OrderTracker> orderTrackerArrayList, String from, boolean disableButton, TrackerDetailFragment trackerDetailFragment) {
        this.activity = activity;
        this.orderTrackerArrayList = orderTrackerArrayList;
        this.from = from;
        session = new Session(activity);
        this.disableButton = disableButton;
        this.trackerDetailFragment = trackerDetailFragment;

    }

    @Override
    public CartItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_lyt, null);
        CartItemHolder cartItemHolder = new CartItemHolder(v);
        return cartItemHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final CartItemHolder holder, final int position) {


        final OrderTracker order = orderTrackerArrayList.get(position);
        String payType = "";
        if (order.getPayment_method().equalsIgnoreCase("cod"))
            payType = activity.getResources().getString(R.string.cod);
        else
            payType = order.getPayment_method();
        String activeStatus = order.getActiveStatus().substring(0, 1).toUpperCase() + order.getActiveStatus().substring(1).toLowerCase();
        holder.txtqty.setText(order.getQuantity());
        holder.txtprice.setText(Constant.SETTING_CURRENCY_SYMBOL + order.getPrice());
        holder.txtpaytype.setText(activity.getResources().getString(R.string.via) + payType);
        holder.txtstatus.setText(activeStatus);
        holder.txtstatusdate.setText(order.getActiveStatusDate());
        holder.txtname.setText(order.getName() + "(" + order.getMeasurement() + order.getUnit() + ")");
        holder.imgorder.setDefaultImageResId(R.drawable.placeholder);
        holder.imgorder.setErrorImageResId(R.drawable.placeholder);
        holder.imgorder.setImageUrl(order.getImage(), Constant.imageLoader);

        ArrayList<String> strings = new ArrayList<>();
        strings.add(order.getActiveStatus());


        if (!order.getActiveStatus().equalsIgnoreCase("cancelled")) {

            ApiConfig.setOrderTrackerLayout(activity, order, holder);

        }


        //Visible cancel and return item button login

        temporaruList.clear();

        for (OrderTracker orderTracker : orderTrackerArrayList) {

            temporaruList.put(orderTracker.getId(), "0");


            if (orderTracker.getCancelable_status().equalsIgnoreCase("1")) {


                if (orderTracker.getReturn_request().equalsIgnoreCase("1")) {

                    if (temporaruList.containsKey(orderTracker.getId())) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            temporaruList.replace(orderTracker.getId(), "1");
                        } else {

                            temporaruList.remove(orderTracker.getId());
                            temporaruList.put(order.getId(), "1");

                        }

                    }
                }
            }


        }



        if (order.getCancelable_status().equalsIgnoreCase("1") || order.getActiveStatus().equalsIgnoreCase("returned")) {


            if (order.getReturn_request().equalsIgnoreCase("1")) {
                holder.btnCancel.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                holder.btnCancel.setEnabled(false);
                holder.btnReturn.setEnabled(false);

            } else {
                holder.btnReturn.setEnabled(true);
                holder.btnCancel.setEnabled(true);

            }

        }


        if (disableButton) {
            holder.btnCancel.setBackgroundColor(activity.getResources().getColor(R.color.gray));
            holder.btnCancel.setEnabled(false);
            holder.btnReturn.setEnabled(false);
            holder.btnReturn.setBackgroundColor(activity.getResources().getColor(R.color.gray));

        }


        holder.carddetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new TrackerDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id", "");
                bundle.putSerializable("model", order);
                fragment.setArguments(bundle);
                MainActivity.fm.beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();
            }
        });


        holder.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
                // Setting Dialog Message
                if (order.getActiveStatus().equals(Constant.CANCELLED)) {
                    alertDialog.setTitle(activity.getResources().getString(R.string.cancel_order));
                    alertDialog.setMessage(activity.getResources().getString(R.string.cancel_msg));
                } else if (order.getActiveStatus().equals(Constant.RETURNED)) {
                    alertDialog.setTitle(activity.getResources().getString(R.string.return_order));
                    alertDialog.setMessage(activity.getResources().getString(R.string.return_msg));
                }

                new AlertDialog.Builder(activity)
                        .setTitle(activity.getString(R.string.cancel_order))
                        .setMessage(activity.getString(R.string.cancel_msg))
                        .setNegativeButton(activity.getString(R.string.bank_card), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                holder.btnCancel.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                                holder.btnCancel.setEnabled(false);

                                if (temporaruList.containsKey(order.getId())) {

                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        temporaruList.replace(order.getId(), "1");
                                    } else {
                                        temporaruList.remove(order.getId());
                                        temporaruList.put(order.getId(), "1");

                                    }
                                }

                                if (temporaruList.containsValue("0")) {
                                    isLastItem = false;
                                } else {
                                    isLastItem = true;
                                }


                                if (order.getSubscribe().equals("0") && orderTrackerArrayList.size() > 1 && !isLastItem) {

                                    updateOrderItem(session.getData(Constant.ID), order.getOrder_id(),
                                            order.getId(), activity.getResources().getString(R.string.bank_card));

                                } else {
                                    updateSubscribeOrderItem(session.getData(Constant.ID), order.getOrder_id(), order.getId(),
                                            activity.getResources().getString(R.string.bank_card));
                                }


                            }
                        })
                        .setPositiveButton(view.getContext().getString(R.string.wallet),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {


                                        holder.btnCancel.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                                        holder.btnCancel.setEnabled(false);


                                        if (temporaruList.containsKey(order.getId())) {

                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                temporaruList.replace(order.getId(), "1");
                                            } else {
                                                temporaruList.remove(order.getId());
                                                temporaruList.put(order.getId(), "1");

                                            }
                                        }

                                        if (temporaruList.containsValue("0")) {
                                            isLastItem = false;
                                        } else {
                                            isLastItem = true;
                                        }


                                        if (order.getSubscribe().equals("0") && orderTrackerArrayList.size() > 1 && !isLastItem) {


                                            updateOrderItem(session.getData(Constant.ID), order.getOrder_id(),
                                                    order.getId(), activity.getResources().getString(R.string.wallet));

                                        } else {


                                            updateSubscribeOrderItem(session.getData(Constant.ID), order.getOrder_id(), order.getId(),
                                                    activity.getResources().getString(R.string.wallet));


                                        }


                                        dialog.dismiss();
                                    }
                                }).show();

            }
        });

        holder.btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                Date date = new Date();
                //System.out.println (myFormat.format (date));
                String inputString1 = order.getActiveStatusDate();
                String inputString2 = myFormat.format(date);
                try {
                    Date date1 = myFormat.parse(inputString1);
                    Date date2 = myFormat.parse(inputString2);
                    long diff = date2.getTime() - date1.getTime();
                    if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) <= Constant.ORDER_DAY_LIMIT) {


                        new AlertDialog.Builder(activity)
                                .setTitle(activity.getString(R.string.cancel_order))
                                .setMessage(activity.getString(R.string.cancel_msg))
                                .setNegativeButton(activity.getString(R.string.bank_card), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {


                                        holder.btnReturn.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                                        holder.btnReturn.setEnabled(false);


                                        if (temporaruList.containsKey(order.getId())) {

                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                temporaruList.replace(order.getId(), "1");
                                            } else {
                                                temporaruList.remove(order.getId());
                                                temporaruList.put(order.getId(), "1");

                                            }
                                        }

                                        if (temporaruList.containsValue("0")) {
                                            isLastItem = false;
                                        } else {
                                            isLastItem = true;
                                        }


                                        if (order.getSubscribe().equals("0") && orderTrackerArrayList.size() > 1 && !isLastItem) {


                                            updateOrderItem(session.getData(Constant.ID), order.getOrder_id(),
                                                    order.getId(), activity.getResources().getString(R.string.bank_card));

                                        } else {


                                            updateSubscribeOrderItem(session.getData(Constant.ID), order.getOrder_id(), order.getId(),
                                                    activity.getResources().getString(R.string.bank_card));


                                        }

                                    }
                                })
                                .setPositiveButton(view.getContext().getString(R.string.wallet),
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                holder.btnReturn.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                                                holder.btnReturn.setEnabled(false);


                                                if (temporaruList.containsKey(order.getId())) {

                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                        temporaruList.replace(order.getId(), "1");
                                                    } else {
                                                        temporaruList.remove(order.getId());
                                                        temporaruList.put(order.getId(), "1");

                                                    }
                                                }

                                                if (temporaruList.containsValue("0")) {
                                                    isLastItem = false;
                                                } else {
                                                    isLastItem = true;
                                                }


                                                if (order.getSubscribe().equals("0") && orderTrackerArrayList.size() > 1 && !isLastItem) {


                                                    updateOrderItem(session.getData(Constant.ID), order.getOrder_id(),
                                                            order.getId(), activity.getResources().getString(R.string.wallet));

                                                } else {


                                                    updateSubscribeOrderItem(session.getData(Constant.ID), order.getOrder_id(), order.getId(),
                                                            activity.getResources().getString(R.string.wallet));


                                                }

                                            }
                                        }).show();


                        //updateOrderStatus(activity, order, Constant.RETURNED, holder, from);


                    } else {
                        final Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getResources().getString(R.string.product_return) + Constant.ORDER_DAY_LIMIT + activity.getString(R.string.day_max_limit), Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(activity.getResources().getString(R.string.ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                snackbar.dismiss();

                            }
                        });
                        snackbar.setActionTextColor(Color.RED);
                        View snackbarView = snackbar.getView();
                        TextView textView = snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        textView.setMaxLines(5);
                        snackbar.show();

                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });


        if (from.equals("detail")) {


            Paper.init(activity);
            HashMap<String, Boolean> isAllCancelled = Paper.book().read(Constant.RETURNED_ALL);


            if (!order.getPack_date().equals("") && !order.getPack_id().equals("")) {
                holder.btnCancel.setVisibility(View.GONE);
                holder.btnReturn.setVisibility(View.GONE);
                if (order.getReturn_request().equalsIgnoreCase("1")) {


                    if (order.getActiveStatus().equalsIgnoreCase("returned")) {
                        btnCancel.setEnabled(false);
                        packAll.put(order.getId(), "1");


                    } else {
                        packAll.put(order.getId(), "0");
                    }


                    if (!packAll.containsValue("0")) {


                        btnCancel.setText(activity.getResources().getString(R.string.return_approved));
                        btnCancel.setEnabled(false);
                    } else {
                        btnCancel.setText(activity.getResources().getString(R.string.return_request));
                        btnCancel.setEnabled(false);
                    }


                }


            } else if (order.getActiveStatus().equalsIgnoreCase("cancelled")) {
                holder.txtstatus.setTextColor(Color.RED);
                holder.btnCancel.setVisibility(View.GONE);
            } else if (order.getActiveStatus().equalsIgnoreCase("delivered")) {

//                holder.btnCancel.setVisibility(View.VISIBLE);
                if (isAllCancelled.get(order.getOrder_id())) {


                    holder.btnReturn.setVisibility(View.VISIBLE);

                    holder.btnReturn.setVisibility(View.VISIBLE);
                    holder.btnReturn.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                    holder.btnReturn.setEnabled(false);

                } else {
                    if (order.getReturn_status().equalsIgnoreCase("1")) {


                        if (order.getReturn_request().equalsIgnoreCase("1")) {

                            holder.btnReturn.setVisibility(View.VISIBLE);
                            holder.btnReturn.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                            holder.btnReturn.setEnabled(false);
                        } else {
                            holder.btnReturn.setVisibility(View.VISIBLE);

                        }

                    } else {
                        holder.btnReturn.setVisibility(View.GONE);
                    }


                }
            } else if (order.getActiveStatus().equalsIgnoreCase("returned")) {
//
                holder.btnCancel.setVisibility(View.GONE);
                holder.btnReturn.setVisibility(View.GONE);


            } else {
                if (order.getCancelable_status().equalsIgnoreCase("1")) {
                    if (order.getTill_status().equalsIgnoreCase("received")) {
                        if (order.getActiveStatus().equalsIgnoreCase("received")) {


                            if (isAllCancelled.get(order.getOrder_id())) {
                                holder.btnCancel.setVisibility(View.GONE);

                            } else {


                                if (order.getReturn_request().equalsIgnoreCase("1")) {

                                    holder.btnCancel.setVisibility(View.VISIBLE);
                                    holder.btnCancel.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                                    holder.btnCancel.setEnabled(false);

                                } else {

                                    holder.btnCancel.setVisibility(View.VISIBLE);

                                }
                            }

                        } else {
                            holder.btnCancel.setVisibility(View.GONE);
                        }
                    } else if (order.getTill_status().equalsIgnoreCase("processed")) {
                        if (order.getActiveStatus().equalsIgnoreCase("received") ||
                                order.getActiveStatus().equalsIgnoreCase("processed")) {


                            if (isAllCancelled.get(order.getOrder_id())) {

                                holder.btnCancel.setVisibility(View.VISIBLE);
                                holder.btnCancel.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                                holder.btnCancel.setEnabled(false);
                            }
                        } else {
                            holder.btnCancel.setVisibility(View.GONE);


                        }


                    } else if (order.getTill_status().equalsIgnoreCase("shipped")) {
                        if (order.getActiveStatus().equalsIgnoreCase("received") || order.getActiveStatus().equalsIgnoreCase("processed") || order.getActiveStatus().equalsIgnoreCase("shipped")) {


                            if (order.getReturn_request().equalsIgnoreCase("1")) {

                                if (isAllCancelled.get(order.getOrder_id())) {

                                    holder.btnCancel.setVisibility(View.VISIBLE);
                                    holder.btnCancel.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                                    holder.btnCancel.setEnabled(false);

                                } else {


                                }

                            } else {
                                holder.btnCancel.setVisibility(View.VISIBLE);

                            }
                        } else {
                            holder.btnCancel.setVisibility(View.GONE);
                        }
                    } else if (order.getActiveStatus().equalsIgnoreCase("delivered")) {

                        if (order.getReturn_request().equalsIgnoreCase("1")) {


                            if (isAllCancelled.get(order.getOrder_id())) {

                                holder.btnReturn.setVisibility(View.VISIBLE);
                                holder.btnReturn.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                                holder.btnReturn.setEnabled(false);

                            } else {


                            }
                        } else {

                            if (order.getReturn_status().equalsIgnoreCase("1")) {
                                holder.btnReturn.setVisibility(View.VISIBLE);

                            }
                        }


                    }
                } else {
                    holder.btnCancel.setVisibility(View.GONE);
                }
            }

            holder.lyttracker.setVisibility(View.VISIBLE);


//
//            for (int i = 0; i < order.getOrderStatusArrayList().size(); i++) {
//                int img = activity.getResources().getIdentifier("img" + i, "id", activity.getPackageName());
//                int view = activity.getResources().getIdentifier("l" + i, "id", activity.getPackageName());
//                int txt = activity.getResources().getIdentifier("txt" + i, "id", activity.getPackageName());
//                int textview = activity.getResources().getIdentifier("txt" + i + "" + i, "id", activity.getPackageName());
//
//
//                if (img != 0 && holder.itemView.findViewById(img) != null) {
//                    ImageView imageView = holder.itemView.findViewById(img);
//                    imageView.setColorFilter(activity.getResources().getColor(R.color.colorPrimary));
//                }
//
//                if (view != 0 && holder.itemView.findViewById(view) != null) {
//                    View view1 = holder.itemView.findViewById(view);
//                    view1.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));
//                }
//
//                if (txt != 0 && holder.itemView.findViewById(txt) != null) {
//                    TextView view1 = holder.itemView.findViewById(txt);
//                    view1.setTextColor(activity.getResources().getColor(R.color.black));
//                }
//
//                if (textview != 0 && holder.itemView.findViewById(textview) != null) {
//                    TextView view1 = holder.itemView.findViewById(textview);
//                    String str = order.getDate_added();
//                    String[] splited = str.split("\\s+");
//                    view1.setText(splited[0] + "\n" + splited[1]);
//                }
//            }


            ApiConfig.setOrderTrackerLayout(activity, order, holder);


            if (order.getActiveStatus().equalsIgnoreCase("cancelled")) {
                holder.lyttracker.setVisibility(View.GONE);
                holder.btnCancel.setVisibility(View.VISIBLE);
                holder.btnCancel.setText("cancelled");


            } else {
//                if (order.getActiveStatus().equals("returned")) {
//
////                    TrackerDetailFragment.btnCancel.setText(activity.getResources().getString(R.string.return_request_approved));
//                    holder.l4.setVisibility(View.VISIBLE);
//                    holder.returnLyt.setVisibility(View.VISIBLE);
//
//                }
//                holder.lyttracker.setVisibility(View.VISIBLE);

            }


        }


    }


    private boolean checkReturnRequestProduct(ArrayList<OrderTracker> list) {

        int count = 0;
        for (OrderTracker orderTracker : list) {

            if (!orderTracker.getReturn_request().equals("1")) {
                count++;

            }
        }

        if (count == 1) {
            isLastItem = true;
        } else {
            isLastItem = false;
        }
        return isLastItem;


    }

    private void updateOrderStatus(final Activity activity,
                                   final OrderTracker order,
                                   final String status,
                                   final CartItemHolder holder, final String from) {

        final Map<String, String> params = new HashMap<>();
        params.put(Constant.UPDATE_ORDER_ITEM_STATUS, Constant.GetVal);
        params.put(Constant.ORDER_ITEM_ID, order.getId());
        params.put(Constant.ORDER_ID, order.getOrder_id());


        params.put(Constant.STATUS, status);


        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        // Setting Dialog Message
        if (status.equals(Constant.CANCELLED)) {
            alertDialog.setTitle(activity.getResources().getString(R.string.cancel_order));
            alertDialog.setMessage(activity.getResources().getString(R.string.cancel_msg));
        } else if (status.equals(Constant.RETURNED)) {
            alertDialog.setTitle(activity.getResources().getString(R.string.return_order));
            alertDialog.setMessage(activity.getResources().getString(R.string.return_msg));

        }

        alertDialog.setTitle(activity.getResources().getString(R.string.cancel_order));
        alertDialog.setMessage(activity.getResources().getString(R.string.cancel_msg));
        final AlertDialog alertDialog1 = alertDialog.create();

        // Setting OK Button
        alertDialog.setPositiveButton(activity.getResources().getString(R.string.wallet), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (pBar != null)
                    pBar.setVisibility(View.VISIBLE);
                holder.btnCancel.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                holder.btnCancel.setEnabled(false);
                holder.btnReturn.setEnabled(false);
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constant.UPDATE_ORDER_STATUS, Constant.GetVal);
                params.put(Constant.ID, order.getOrder_id());
                params.put(Constant.RETURN_METHOD, activity.getString(R.string.wallet));
                params.put(Constant.RETURN_REQUEST, Constant.GetVal);
                pBar.setVisibility(View.VISIBLE);


                ApiConfig.RequestToVolley(new VolleyCallback() {
                    @Override
                    public void onSuccess(boolean result, String response) {
                        // System.out.println("=================*cancelorder- " + response);
                        if (result) {
                            try {
                                JSONObject object = new JSONObject(response);
                                if (!object.getBoolean(Constant.ERROR)) {
                                    Constant.isOrderCancelled = true;


                                    holder.btnCancel.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                                    holder.btnCancel.setEnabled(false);
                                    holder.btnReturn.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                                    holder.btnReturn.setEnabled(false);
                                    //ApiConfig.getWalletBalance(activity, new Session(getContext()));
                                }
                                Toast.makeText(activity, object.getString("message"), Toast.LENGTH_LONG).show();
                                pBar.setVisibility(View.GONE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, activity, Constant.ORDERPROCESS_URL, params, false);


            }
        });
        alertDialog.setNegativeButton(activity.getResources().getString(R.string.bank_card), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                holder.btnCancel.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                holder.btnCancel.setEnabled(false);
                holder.btnReturn.setEnabled(false);


                Map<String, String> params = new HashMap<String, String>();
                params.put(Constant.UPDATE_ORDER_STATUS, Constant.GetVal);
                params.put(Constant.ID, order.getOrder_id());
                params.put(Constant.RETURN_METHOD, activity.getString(R.string.bank_card));
                params.put(Constant.RETURN_REQUEST, Constant.GetVal);
                pBar.setVisibility(View.VISIBLE);
                ApiConfig.RequestToVolley(new VolleyCallback() {
                    @Override
                    public void onSuccess(boolean result, String response) {
                        // System.out.println("=================*cancelorder- " + response);
                        if (result) {
                            try {
                                JSONObject object = new JSONObject(response);
                                if (!object.getBoolean(Constant.ERROR)) {
                                    Constant.isOrderCancelled = true;

                                    holder.btnCancel.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                                    holder.btnCancel.setEnabled(false);
                                    holder.btnReturn.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                                    holder.btnReturn.setEnabled(false);
                                    //ApiConfig.getWalletBalance(activity, new Session(getContext()));

                                }
                                Toast.makeText(activity, object.getString("message"), Toast.LENGTH_LONG).show();
                                pBar.setVisibility(View.GONE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, activity, Constant.ORDERPROCESS_URL, params, false);


                alertDialog1.dismiss();
            }
        });
        // Showing Alert Message
        alertDialog.show();


    }

    private void addReturnRequest(String user_id, String order_id, String order_item_id, String returnMethod
    ) {
        HashMap<String, String> params = new HashMap<>();
        params.put(Constant.USER_ID, user_id);
        params.put(Constant.ORDER_ID, order_id);
        params.put(Constant.ORDER_ITEM_ID, order_item_id);

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                try {
                    JSONObject resultData = new JSONObject(message);
                    Toast.makeText(activity, activity.getResources().getString(
                            R.string.return_request), Toast.LENGTH_LONG).show();

                    if (!resultData.getBoolean(Constant.ERROR)) {
                        //trackerDetailFragment.SetData(orderTracker,true);
                        btnCancel.setEnabled(false);
                        btnCancel.setText("Order cancellation request sent");
                        Toast.makeText(activity, activity.getResources().getString(
                                R.string.return_request), Toast.LENGTH_LONG).show();


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, activity, "https://store.satvicday.com/api-firebase/add_singlereturn_requests.php", params, true);


    }


    private void updateOrderItem(String user_id, String order_id, String order_item_id, String returnMethod
    ) {
        HashMap<String, String> params = new HashMap<>();


        params.put("singleupdate_order_status", "1");
        params.put(Constant.USER_ID, user_id);
        params.put(Constant.ID, order_id);
        params.put(Constant.ORDER_ITEM_ID, order_item_id);
        params.put(Constant.RETURN_REQUEST, "1");
        params.put(Constant.RETURN_METHOD, returnMethod);


        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                try {
                    JSONObject resultData = new JSONObject(message);
                    Toast.makeText(activity, activity.getResources().getString(
                            R.string.return_request), Toast.LENGTH_LONG).show();

                    if (!resultData.getBoolean(Constant.ERROR)) {
                        //trackerDetailFragment.SetData(orderTracker,true);

                        addReturnRequest(user_id, order_id, order_item_id, returnMethod);


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, activity, Constant.ORDERPROCESS_URL, params, true);


    }


    private void updateSubscribeOrderItem(String user_id, String order_id, String order_item_id, String returnMethod
    ) {
        HashMap<String, String> params = new HashMap<>();
        params.put(Constant.UPDATE_ORDER_STATUS, Constant.GetVal);
        params.put(Constant.ID, order_id);
        params.put(Constant.RETURN_METHOD, returnMethod);
        params.put(Constant.RETURN_REQUEST, Constant.GetVal);

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                try {
                    JSONObject resultData = new JSONObject(message);

                    if (!resultData.getBoolean(Constant.ERROR)) {
                        //trackerDetailFragment.SetData(orderTracker,true);

                        addReturnRequestSubscribe(user_id, order_id);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, activity, Constant.ORDERPROCESS_URL, params, true);


    }

    private void addReturnRequestSubscribe(String user_id, String order_id) {
        HashMap<String, String> params = new HashMap<>();
        params.put(Constant.USER_ID, user_id);
        params.put(Constant.ORDER_ID, order_id);
        params.put("ordertypes", "1");


        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                try {
                    JSONObject resultData = new JSONObject(message);
                    Toast.makeText(activity, activity.getResources().getString(
                            R.string.return_request), Toast.LENGTH_LONG).show();
                    btnCancel.setText("Order cancellation request sent");

                    if (!resultData.getBoolean(Constant.ERROR)) {

                        btnCancel.setText("Order cancellation request sent");
                        Toast.makeText(activity, activity.getResources().getString(
                                R.string.return_request), Toast.LENGTH_LONG).show();


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, activity, "https://store.satvicday.com/api-firebase/add_return_requests.php", params, true);

    }

    @Override
    public int getItemCount() {
        return orderTrackerArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public class CartItemHolder extends RecyclerView.ViewHolder {
        TextView txtqty, txtprice, txtpaytype, txtstatus, txtstatusdate, txtname;
        NetworkImageView imgorder;
        CardView carddetail;
        RecyclerView recyclerView;
        public Button btnCancel, btnReturn;
        View l4;
        LinearLayout lyttracker, returnLyt;

        public CartItemHolder(View itemView) {
            super(itemView);

            txtqty = itemView.findViewById(R.id.txtqty);
            txtprice = itemView.findViewById(R.id.txtprice);
            txtpaytype = itemView.findViewById(R.id.txtpaytype);
            txtstatus = itemView.findViewById(R.id.txtstatus);
            txtstatusdate = itemView.findViewById(R.id.txtstatusdate);
            txtname = itemView.findViewById(R.id.txtname);
            btnCancel = itemView.findViewById(R.id.btnCancel);
            imgorder = itemView.findViewById(R.id.imgorder);
            carddetail = itemView.findViewById(R.id.carddetail);
            recyclerView = itemView.findViewById(R.id.recyclerView);
            btnReturn = itemView.findViewById(R.id.btnReturn);
            lyttracker = itemView.findViewById(R.id.lyttracker);
            l4 = itemView.findViewById(R.id.l4);
            returnLyt = itemView.findViewById(R.id.returnLyt);


        }


    }

}

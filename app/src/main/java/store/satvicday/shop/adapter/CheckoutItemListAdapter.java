package store.satvicday.shop.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import store.satvicday.shop.R;
import store.satvicday.shop.fragment.CheckoutFragment;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.model.Cart;

/**
 * Created by shree1 on 3/16/2017.
 */

public class CheckoutItemListAdapter extends RecyclerView.Adapter<CheckoutItemListAdapter.ItemHolder> {

    public ArrayList<Cart> carts;
    public Activity activity;

    public CheckoutItemListAdapter(Activity activity, ArrayList<Cart> carts) {
        this.activity = activity;
        this.carts = carts;
    }

    @Override
    public int getItemCount() {
        return carts.size();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ItemHolder holder, final int position) {
        final Cart cart = carts.get(position);

        float price;
        if (cart.getItems().get(0).getDiscounted_price().equals("0")) {
            price = Float.parseFloat(cart.getItems().get(0).getPrice());
        } else {
            price = Float.parseFloat(cart.getItems().get(0).getDiscounted_price());
        }

        double itemTotal = price * (Integer.parseInt(cart.getQty()));

        try{
            double itemTaxAmt = (itemTotal * (Double.parseDouble(cart.getItems().get(0).getTax_percentage()) / 100));

        }catch (NumberFormatException e){
            Toast.makeText(activity,"Some error occured, Please add different product",Toast.LENGTH_SHORT).show();
            CheckoutFragment.tvConfirmOrder.setBackgroundColor(activity.getResources().getColor(R.color.gray));

            CheckoutFragment.tvConfirmOrder.setEnabled(false);

        }


        holder.tvItemName.setText(cart.getItems().get(0).getName() + "\n(" + cart.getItems().get(0).getMeasurement() + " " + ApiConfig.toTitleCase(cart.getItems().get(0).getUnit()) + ")\nTax (" + cart.getItems().get(0).getTax_percentage() + "%)");
//        holder.tvTax.setText(cart.getItems().get(0).getTax_percentage() + "% tax on " + itemTotal);
        holder.tvQty.setText(cart.getQty());
        holder.tvPrice.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(price));
        holder.tvSubTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(itemTotal));


        try{

//

            if (Constant.CheckOutCartValues.containsKey(cart.getProduct_variant_id())) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Constant.CheckOutCartValues.replace(cart.getProduct_variant_id(), "" + cart.getQty());
                } else {
                    Constant.CheckOutCartValues.remove(cart.getProduct_variant_id());
                    Constant.CheckOutCartValues.put(cart.getProduct_variant_id(), "" + cart.getQty());
                }
            } else {
                Constant.CheckOutCartValues.put(cart.getProduct_variant_id(), "" + cart.getQty());
            }
        }catch (Exception e){


        }



    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_checkout_item_list, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ItemHolder extends RecyclerView.ViewHolder {

        public TextView tvItemName, tvQty, tvPrice, tvSubTotal;
        ;//, tvTax;

        public ItemHolder(View itemView) {
            super(itemView);

            tvItemName = itemView.findViewById(R.id.tvItemName);
            tvQty = itemView.findViewById(R.id.tvQty);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvSubTotal = itemView.findViewById(R.id.tvSubTotal);
//            tvTax = itemView.findViewById(R.id.tvTax);
        }


    }
}


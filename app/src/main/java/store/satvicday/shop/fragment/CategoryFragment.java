package store.satvicday.shop.fragment;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.packs.adapter.PackCategoryAdapter;
import store.satvicday.shop.adapter.CategoryAdapter;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.AppController;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.Utils;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.Category;
import store.satvicday.shop.model.Product;
import store.satvicday.shop.model.packs.Pack;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class CategoryFragment extends Fragment {

    public static ArrayList<Category> categoryArrayList;
    TextView txtnodata;
    RecyclerView categoryrecycleview;
    SwipeRefreshLayout swipeLayout;
    View root;
    Activity activity;
    ProgressBar progressBar;

    private PaymentFragment.TypePurchase typePurchase;
    private Pack pack;
    private String from;
    private Session session;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_category, container, false);

        activity = getActivity();
        Paper.init(activity);
        session = new Session(activity);
        typePurchase = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE);

        setHasOptionsMenu(true);

        pack = Paper.book().read(Constant.PACK_DETAILS);




        txtnodata = root.findViewById(R.id.txtnodata);
        swipeLayout = root.findViewById(R.id.swipeLayout);
        progressBar = root.findViewById(R.id.progressBar);
        categoryrecycleview = root.findViewById(R.id.categoryrecycleview);
        categoryrecycleview.setLayoutManager(new GridLayoutManager(getContext(), Constant.GRIDCOLUMN));
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (AppController.isConnected(activity)) {
                    if (typePurchase != PaymentFragment.TypePurchase.PACK){
                        from = getArguments().getString("from");
                        if (from!=null) {
                            if (from.equals("allpack")) {
                                GetPacks();
                            }else {
                                GetCategory();
                            }
                        }else {
                            GetCategory();

                        }
                    }else {
                        GetPackCategory(pack.getCategoryId());

                    }
                }
                swipeLayout.setRefreshing(false);
            }
        });



            if (typePurchase != PaymentFragment.TypePurchase.PACK){
                from = getArguments().getString("from");
                if (from!=null) {

                    if (from.equals("allpack")) {
                        GetPacks();

                    }else {
                        GetCategory();

                    }
                }else {
                    GetCategory();

                }
            }else {


                from = getArguments().getString("from");
                if (from!=null){

                    if (from.equals("allpack")){
                        GetPacks();

                    }else {


                        GetPackCategory(pack.getCategoryId());

                    }


            }else {


                    GetPackCategory(pack.getCategoryId());

                }
        }




        return root;
    }


    private void GetPacks() {


        HashMap<String,String> params = new HashMap<>();
        params.put("user_id", session.getData(Constant.ID));

        ApiConfig.RequestToVolley(new VolleyCallback() {
        private List<Pack> packArrayList = new ArrayList<>();

            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        JSONObject object = new JSONObject(response);
                        packArrayList = new ArrayList<>();
                        packArrayList.clear();
                        if (!object.getBoolean(Constant.ERROR)) {

                            packArrayList = Utils.decryptData(object,
                                    Pack.class);
                            categoryrecycleview.setAdapter(
                                    new PackCategoryAdapter(packArrayList,getContext()));


                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, getActivity(), Constant.GET_PACKS, params, false);
    }


    private void GetPackCategory(String categoryId) {


        ArrayList<Category> filterList = new ArrayList<>();
        progressBar.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<String, String>();

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                //System.out.println("======cate " + response);



                if (result) {
                    try {
                        JSONObject object = new JSONObject(response);
                        categoryArrayList = new ArrayList<>();
                        categoryArrayList.clear();
                        if (!object.getBoolean(Constant.ERROR)) {

                            JSONArray jsonArray = object.getJSONArray(Constant.DATA);
                            Gson gson = new Gson();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                Category category = new Category();
                                category.setId(jsonObject.getString(Constant.ID));
                                category.setName(jsonObject.getString(Constant.NAME));
                                category.setSubtitle(jsonObject.getString(Constant.SUBTITLE));
                                category.setImage(jsonObject.getString(Constant.IMAGE));
                                category.setStatus(jsonObject.getString(Constant.STATUS));
                                categoryArrayList.add(category);
                            }

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                               categoryArrayList.stream().filter(
                                       category ->  {
                                           String[] catIds = categoryId.split(",");
                                           for (int i = 0; i <catIds.length ; i++) {
                                               if (catIds[i].equals(category.getId())){
                                                   filterList.add(category);
                                               }
                                           }
                                           return false;
                                       }
                                ).collect(Collectors.toList());


                            }else {
                                for (Category category:categoryArrayList){
                                    String[] catIds = categoryId.split(",");
                                    for (int i = 0; i <catIds.length ; i++) {
                                        if (catIds[i].equals(category.getId())){
                                            filterList.add(category);
                                        }
                                    }

                                }




                            }

                            categoryrecycleview.setAdapter(new CategoryAdapter(getContext(), activity, filterList,
                                    R.layout.lyt_category,"pack"));


                            progressBar.setVisibility(View.GONE);

                        } else {
                            txtnodata.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            categoryrecycleview.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        progressBar.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                }
            }
        }, activity, Constant.CategoryUrl, params, false);


    }

    private void GetCategory() {
        progressBar.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<String, String>();
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                //System.out.println("======cate " + response);
                if (result) {
                    try {
                        JSONObject object = new JSONObject(response);
                        categoryArrayList = new ArrayList<>();
                        categoryArrayList.clear();
                        if (!object.getBoolean(Constant.ERROR)) {
                            JSONArray jsonArray = object.getJSONArray(Constant.DATA);
                            Gson gson = new Gson();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                Category category = new Category();
                                category.setId(jsonObject.getString(Constant.ID));
                                category.setName(jsonObject.getString(Constant.NAME));
                                category.setSubtitle(jsonObject.getString(Constant.SUBTITLE));
                                category.setImage(jsonObject.getString(Constant.IMAGE));
                                category.setStatus(jsonObject.getString(Constant.STATUS));
                                categoryArrayList.add(category);
                            }
                            categoryrecycleview.setAdapter(new CategoryAdapter(getContext(), activity, categoryArrayList, R.layout.lyt_category,"category"));
                            progressBar.setVisibility(View.GONE);
                        } else {
                            txtnodata.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            categoryrecycleview.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        progressBar.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                }
            }
        }, activity, Constant.CategoryUrl, params, false);



    }

    @Override
    public void onResume() {
        super.onResume();

        if (typePurchase == PaymentFragment.TypePurchase.PACK) {

            Pack pack = Paper.book().read(Constant.PACK_DETAILS);
            Constant.TOOLBAR_TITLE = getResources().getString(R.string.rupee_symbol)
                    +" "+ pack.getPackValue()+" "+getResources().getString(R.string.pack);

        }else if (from.equals("allpack")){

            Constant.TOOLBAR_TITLE = getResources().getString(R.string.all_packs);


        }else {


            Constant.TOOLBAR_TITLE = getArguments().getString("name");
            hideKeyboard();
        }

        getActivity().invalidateOptionsMenu();
        hideKeyboard();
    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(root.getApplicationWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (typePurchase== PaymentFragment.TypePurchase.PACK){
            menu.findItem(R.id.toolbar_cart).setVisible(false);
            menu.findItem(R.id.toolbar_sort).setVisible(false);
            menu.findItem(R.id.toolbar_search).setVisible(false);

        }else {

            menu.findItem(R.id.toolbar_cart).setVisible(true);
            menu.findItem(R.id.toolbar_sort).setVisible(false);
            menu.findItem(R.id.toolbar_search).setVisible(true);

        }



        }


}
package store.satvicday.shop.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.flutterwave.raveandroid.RavePayActivity;
import com.flutterwave.raveandroid.RaveUiManager;
import com.flutterwave.raveandroid.rave_java_commons.RaveConstants;
import com.google.gson.Gson;
import com.razorpay.Checkout;
import com.shreyaspatil.easyupipayment.EasyUpiPayment;
import com.shreyaspatil.easyupipayment.exception.AppNotFoundException;
import com.shreyaspatil.easyupipayment.listener.PaymentStatusListener;
import com.shreyaspatil.easyupipayment.model.TransactionDetails;
import com.wangsun.upi.payment.model.PaymentDetail;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import store.satvicday.shop.R;
import store.satvicday.shop.activity.DrawerActivity;
import store.satvicday.shop.activity.GetWalletPrice;
import store.satvicday.shop.activity.MainActivity;
import store.satvicday.shop.activity.PayPalWebActivity;
import store.satvicday.shop.activity.PayStackActivity;
import store.satvicday.shop.activity.WalletRefresh;
import store.satvicday.shop.activity.packs.GetLeftPrice;
import store.satvicday.shop.adapter.WalletTransactionAdapter;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.PaymentModelClass;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.WalletTransaction;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class WalletTransactionFragment extends Fragment implements PaymentStatusListener {
    public static String amount, msg;
    public static boolean payFromWallet = false;
    View root;
    RecyclerView recyclerView;
    ArrayList<WalletTransaction> walletTransactions;
    SwipeRefreshLayout swipeLayout;
    NestedScrollView scrollView;
    RelativeLayout tvAlert;
    WalletTransactionAdapter walletTransactionAdapter;
    int total = 0;
    Activity activity;
    int offset = 0;
    public static TextView tvBalance;
    TextView tvAlertTitle, tvAlertSubTitle;
    Button btnRechargeWallet;
    private Session session;
    private boolean isLoadMore = false;
    private String paymentMethod = null;
    public static  WalletRefresh walletRefresh;

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_wallet_transection, container, false);

        activity = getActivity();
        Log.e("getTransactionData", "data");




        session = new Session(activity);
        scrollView = root.findViewById(R.id.scrollView);
        recyclerView = root.findViewById(R.id.recyclerView);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(linearLayoutManager);
        swipeLayout = root.findViewById(R.id.swipeLayout);
        tvAlert = root.findViewById(R.id.tvAlert);
        tvAlertTitle = root.findViewById(R.id.tvAlertTitle);
        tvAlertSubTitle = root.findViewById(R.id.tvAlertSubTitle);
        tvBalance = root.findViewById(R.id.tvBalance);
        btnRechargeWallet = root.findViewById(R.id.btnRechargeWallet);


        tvAlertTitle.setText(getString(R.string.no_wallet_history_found));
        tvAlertSubTitle.setText(getString(R.string.you_have_not_any_wallet_history_yet));

        setHasOptionsMenu(true);
        swipeLayout.setColorSchemeResources(R.color.colorPrimary);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(false);
                offset = 0;
                getTransactionData(activity, session);
            }
        });


       ApiConfig.getWalletBalance(activity, session, new GetWalletPrice() {
           @Override
           public void onSuccesPrice(Double result) {
               tvBalance.setText(Constant.formater.format(result));
           }
       });




        getTransactionData(activity, session);



        btnRechargeWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                final View dialogView = inflater.inflate(R.layout.dialog_wallet_recharge, null);
                alertDialog.setView(dialogView);
                alertDialog.setCancelable(true);
                final AlertDialog dialog = alertDialog.create();
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                TextView tvDialogSend, tvDialogCancel, edtAmount, edtMsg;
                LinearLayout lytPayOption, lytFlutterWave, lytPayU, lytPayPal, lytRazorPay, lytPayStack, lytUpiPay;
                RadioButton rbPayU, rbPayPal, rbRazorPay, rbPayStack, rbFlutterWave,rbUpi;

                edtAmount = dialogView.findViewById(R.id.edtAmount);
                edtMsg = dialogView.findViewById(R.id.edtMsg);
                tvDialogCancel = dialogView.findViewById(R.id.tvDialogCancel);
                tvDialogSend = dialogView.findViewById(R.id.tvDialogRecharge);
                lytPayOption = dialogView.findViewById(R.id.lytPayOption);
                lytPayStack = dialogView.findViewById(R.id.lytPayStack);
                rbPayStack = dialogView.findViewById(R.id.rbPayStack);
                rbFlutterWave = dialogView.findViewById(R.id.rbFlutterWave);
                rbPayU = dialogView.findViewById(R.id.rbPayU);
                rbPayPal = dialogView.findViewById(R.id.rbPayPal);
                rbRazorPay = dialogView.findViewById(R.id.rbRazorPay);
                lytPayPal = dialogView.findViewById(R.id.lytPayPal);
                lytRazorPay = dialogView.findViewById(R.id.lytRazorPay);
                lytPayU = dialogView.findViewById(R.id.lytPayU);
                lytFlutterWave = dialogView.findViewById(R.id.lytFlutterWave);
                lytUpiPay = dialogView.findViewById(R.id.lytUPI);
                rbUpi = dialogView.findViewById(R.id.rbUPI);

                Map<String, String> params = new HashMap<>();
                params.put(Constant.SETTINGS, Constant.GetVal);
                params.put(Constant.GET_PAYMENT_METHOD, Constant.GetVal);
                //  System.out.println("=====params " + params.toString());
                ApiConfig.RequestToVolley(new VolleyCallback() {
                    @Override
                    public void onSuccess(boolean result, String response) {

                        if (result) {
                            try {
                                JSONObject objectbject = new JSONObject(response);
                                if (!objectbject.getBoolean(Constant.ERROR)) {
                                    if (objectbject.has("payment_methods")) {
                                        JSONObject object = objectbject.getJSONObject(Constant.PAYMENT_METHODS);

                                        if (object.has("cod_payment_method")) {
                                            Constant.COD = object.getString(Constant.cod_payment_method);
                                        }

                                        if (object.has("paypal_payment_method")) {
                                            Constant.PAYPAL = object.getString(Constant.paypal_method);
                                        }

                                        if (object.has("payumoney_payment_method")) {
                                            Constant.PAYUMONEY = object.getString(Constant.payu_method);
                                            Constant.MERCHANT_KEY = object.getString(Constant.PAY_M_KEY);
                                            Constant.MERCHANT_ID = object.getString(Constant.PAYU_M_ID);
                                            Constant.MERCHANT_SALT = object.getString(Constant.PAYU_SALT);
                                        }

                                        if (object.has("razorpay_payment_method")) {
                                            Constant.RAZORPAY = object.getString(Constant.razor_pay_method);
                                            Constant.RAZOR_PAY_KEY_VALUE = object.getString(Constant.RAZOR_PAY_KEY);
                                        }

                                        if (object.has("paystack_payment_method")) {
                                            Constant.PAYSTACK = object.getString(Constant.paystack_method);
                                            Constant.PAYSTACK_KEY = object.getString(Constant.paystack_public_key);
                                        }

                                        if (object.has("flutterwave_payment_method")) {
                                            Constant.FLUTTERWAVE = object.getString(Constant.flutterwave_payment_method);
                                            Constant.FLUTTERWAVE_ENCRYPTION_KEY_VAL = object.getString(Constant.flutterwave_encryption_key);
                                            Constant.FLUTTERWAVE_PUBLIC_KEY_VAL = object.getString(Constant.flutterwave_public_key);
                                            Constant.FLUTTERWAVE_SECRET_KEY_VAL = object.getString(Constant.flutterwave_secret_key);
                                        }
                                        Constant.VPA_PAYEE_ADDRESS = object.getString(Constant.VPA_PAYEE_ADDRESS_KEY);
                                        Constant.VPA_PAYEE_NAME = object.getString(Constant.VPA_PAYEE_NAME_KEY);
                                        Constant.VPA_MERCHANT_CODE = object.getString(Constant.VPA_MERCHANT_CODE_KEY);






                                        if (Constant.FLUTTERWAVE.equals("0") && Constant.PAYPAL.equals("0") && Constant.PAYUMONEY.equals("0") && Constant.COD.equals("0") && Constant.RAZORPAY.equals("0") && Constant.PAYSTACK.equals("0")) {
                                            lytPayOption.setVisibility(View.GONE);
                                        } else {
                                            lytPayOption.setVisibility(View.VISIBLE);

                                            if (Constant.PAYUMONEY.equals("1")) {
                                                lytPayU.setVisibility(View.VISIBLE);
                                            }
                                            if (Constant.RAZORPAY.equals("1")) {
                                                lytRazorPay.setVisibility(View.VISIBLE);
                                            }
                                            if (Constant.PAYSTACK.equals("1")) {
                                                lytPayStack.setVisibility(View.VISIBLE);
                                            }
                                            if (Constant.FLUTTERWAVE.equals("1")) {
                                                lytFlutterWave.setVisibility(View.VISIBLE);
                                            }
                                            if (Constant.PAYPAL.equals("1")) {
                                                lytPayPal.setVisibility(View.VISIBLE);
                                            }

                                            rbPayU.setOnClickListener(v -> {
                                                rbPayU.setChecked(true);
                                                rbPayPal.setChecked(false);
                                                rbRazorPay.setChecked(false);
                                                rbPayStack.setChecked(false);
                                                rbFlutterWave.setChecked(false);
                                                paymentMethod = rbPayU.getTag().toString();

                                            });

                                            rbPayPal.setOnClickListener(v -> {
                                                rbPayU.setChecked(false);
                                                rbPayPal.setChecked(true);
                                                rbRazorPay.setChecked(false);
                                                rbPayStack.setChecked(false);
                                                rbFlutterWave.setChecked(false);
                                                paymentMethod = rbPayPal.getTag().toString();

                                            });

                                            rbRazorPay.setOnClickListener(v -> {
                                                rbPayU.setChecked(false);
                                                rbPayPal.setChecked(false);
                                                rbRazorPay.setChecked(true);
                                                rbPayStack.setChecked(false);
                                                rbFlutterWave.setChecked(false);
                                                paymentMethod = rbRazorPay.getTag().toString();
                                                Checkout.preload(getContext());
                                            });

                                            rbPayStack.setOnClickListener(v -> {
                                                rbPayU.setChecked(false);
                                                rbPayPal.setChecked(false);
                                                rbRazorPay.setChecked(false);
                                                rbPayStack.setChecked(true);
                                                rbFlutterWave.setChecked(false);
                                                paymentMethod = rbPayStack.getTag().toString();

                                            });

                                            rbFlutterWave.setOnClickListener(v -> {
                                                rbPayU.setChecked(false);
                                                rbPayPal.setChecked(false);
                                                rbRazorPay.setChecked(false);
                                                rbPayStack.setChecked(false);
                                                rbFlutterWave.setChecked(true);
                                                paymentMethod = rbFlutterWave.getTag().toString();

                                            });


                                            rbUpi.setOnClickListener(v -> {
                                                rbPayU.setChecked(false);
                                                rbPayPal.setChecked(false);
                                                rbRazorPay.setChecked(false);
                                                rbPayStack.setChecked(false);
                                                rbFlutterWave.setChecked(false);
                                                rbUpi.setChecked(true);

                                                paymentMethod = rbUpi.getTag().toString();
                                            });

                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, activity, Constant.SETTING_URL, params, false);

                tvDialogSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edtAmount.getText().toString().equals("")) {
                            edtAmount.setError(getString(R.string.alert_enter_amount));
                            edtAmount.requestFocus();
                        } else {
                            if (Double.parseDouble(edtAmount.getText().toString().trim()) <= 0) {
                                edtAmount.setError(getString(R.string.alert_recharge));
                                edtAmount.requestFocus();
                            } else {
                                if (paymentMethod != null) {
                                    amount = edtAmount.getText().toString().trim();
                                    msg = edtMsg.getText().toString().trim();
                                    dialog.dismiss();
                                    RechargeWallet();
                                    dialog.dismiss();
                                } else {
                                    Toast.makeText(activity, getString(R.string.select_payment_method), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                });

                tvDialogCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });


        walletRefresh = new WalletRefresh() {
            @Override
            public void onBack(boolean yes) {
                getTransactionData(activity, session);

            }
        };

        return root;
    }

    public void AddWalletBalance(Activity activity, Session session, String amount, String msg, String txID) {

        Map<String, String> params = new HashMap<>();
        params.put(Constant.ADD_WALLET_BALANCE, Constant.GetVal);
        params.put(Constant.USER_ID, session.getData(Constant.ID));
        params.put(Constant.AMOUNT, amount);
        params.put(Constant.TYPE, Constant.CREDIT);
        params.put(Constant.MESSAGE, msg + ", Transaction ID : " + txID);

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                    try {
                        JSONObject object = new JSONObject(response);
                        if (!object.getBoolean(Constant.ERROR)) {



                                WalletTransactionFragment.tvBalance.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(Double.parseDouble(object.getString("new_balance"))));
                                DrawerActivity.tvWallet.setText(Constant.SETTING_CURRENCY_SYMBOL +  Constant.formater.format(Double.parseDouble(object.getString("new_balance"))));
                                WalletTransactionFragment.walletRefresh.onBack(true);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

            }
        }, activity, Constant.TRANSACTION_URL, params, true);
    }


    public void StartPayPalPayment(final Map<String, String> sendParams) {

        final Map<String, String> params = new HashMap<>();
        params.put(Constant.FIRST_NAME, sendParams.get(Constant.USER_NAME));
        params.put(Constant.LAST_NAME, sendParams.get(Constant.USER_NAME));
        params.put(Constant.PAYER_EMAIL, sendParams.get(Constant.EMAIL));
        params.put(Constant.ITEM_NAME, "Wallet Recharge");
        params.put(Constant.ITEM_NUMBER, System.currentTimeMillis() + Constant.randomNumeric(3));
        params.put(Constant.CURRENCY, "INR");
        params.put(Constant.AMOUNT, sendParams.get(Constant.FINAL_TOTAL));
        // System.out.println("======params paypal "+params.toString());
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                //System.out.println("=====url paypal == "+response );
                Intent intent = new Intent(getContext(), PayPalWebActivity.class);
                intent.putExtra("url", response);
                intent.putExtra("item_no", params.get(Constant.ITEM_NUMBER));
                intent.putExtra("params", (Serializable) sendParams);
                startActivity(intent);
            }
        }, getActivity(), Constant.PAPAL_URL, params, true);
    }

    public void callPayStack(final Map<String, String> sendParams) {
        Intent intent = new Intent(activity, PayStackActivity.class);
        intent.putExtra("params", (Serializable) sendParams);
        startActivity(intent);
    }

    private void StartFlutterWavePayment() {
        new RaveUiManager(activity).setAmount(Double.parseDouble(amount))
                .setCurrency("USD")
                .setEmail(session.getData(Constant.EMAIL))
                .setfName(session.getData(Constant.FIRST_NAME))
                .setlName(session.getData(Constant.FIRST_NAME))
                .setPublicKey(Constant.FLUTTERWAVE_PUBLIC_KEY_VAL)
                .setEncryptionKey(Constant.FLUTTERWAVE_ENCRYPTION_KEY_VAL)
                .setTxRef(System.currentTimeMillis() + "Ref")
                .acceptAccountPayments(true)
                .acceptCardPayments(true)
                .acceptMpesaPayments(true)
                .acceptAchPayments(true)
                .acceptGHMobileMoneyPayments(true)
                .acceptUgMobileMoneyPayments(true)
                .acceptZmMobileMoneyPayments(true)
                .acceptRwfMobileMoneyPayments(true)
                .acceptSaBankPayments(true)
                .acceptUkPayments(true)
                .acceptBankTransferPayments(true)
                .acceptUssdPayments(true)
                .acceptBarterPayments(true)
                .acceptFrancMobileMoneyPayments(true)
                .allowSaveCardFeature(true)
                .onStagingEnv(true)
                .isPreAuth(true)
                .shouldDisplayFee(true)
                .showStagingLabel(true)
                .initialize();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != RaveConstants.RAVE_REQUEST_CODE && data != null) {
            new PaymentModelClass(getActivity()).TrasactionMethod(data, getActivity(), "wallet");
        } else if (requestCode == RaveConstants.RAVE_REQUEST_CODE && data != null) {
            try {
                JSONObject details = new JSONObject(data.getStringExtra("response"));
                JSONObject jsonObject = details.getJSONObject(Constant.DATA);

                if (resultCode == RavePayActivity.RESULT_SUCCESS) {
                    AddWalletBalance(activity, new Session(activity), amount, msg, jsonObject.getString("txRef"));
                    Toast.makeText(getContext(), getString(R.string.wallet_recharged), Toast.LENGTH_LONG).show();

                } else if (resultCode == RavePayActivity.RESULT_ERROR) {
                    Toast.makeText(getContext(), getString(R.string.order_error), Toast.LENGTH_LONG).show();
                } else if (resultCode == RavePayActivity.RESULT_CANCELLED) {
                    Toast.makeText(getContext(), getString(R.string.order_cancel), Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void RechargeWallet() {
        HashMap<String, String> sendparams = new HashMap();
        if (paymentMethod.equals(getString(R.string.pay_u))) {
            sendparams.put(Constant.MOBILE, session.getData(Session.KEY_MOBILE));
            sendparams.put(Constant.USER_NAME, session.getData(Session.KEY_NAME));
            sendparams.put(Constant.EMAIL, session.getData(Session.KEY_EMAIL));
            new PaymentModelClass(getActivity()).OnPayClick(getActivity(), sendparams, "Wallet Recharge", amount);
        } else if (paymentMethod.equals(getString(R.string.paypal))) {
            sendparams.put(Constant.AMOUNT, amount);
            sendparams.put(Constant.FIRST_NAME, session.getData(Constant.NAME));
            sendparams.put(Constant.LAST_NAME, session.getData(Constant.NAME));
            sendparams.put(Constant.PAYER_EMAIL, session.getData(Constant.EMAIL));
            sendparams.put(Constant.ITEM_NAME, "Wallet Recharge");
            sendparams.put(Constant.ITEM_NUMBER, System.currentTimeMillis() + Constant.randomNumeric(3));
            StartPayPalPayment(sendparams);
        } else if (paymentMethod.equals(getString(R.string.razor_pay))) {
            payFromWallet = true;
            CreateOrderId(Double.parseDouble(amount));
        } else if (paymentMethod.equals(getString(R.string.paystack))) {
            sendparams.put(Constant.FINAL_TOTAL, amount);
            callPayStack(sendparams);
        } else if (paymentMethod.equals(getString(R.string.flutterwave))) {
            StartFlutterWavePayment();
        }else if (paymentMethod.equals(getString(R.string.upi_pay))) {
            sendparams.put(Constant.AMOUNT, amount);
            sendparams.put(Constant.FIRST_NAME, session.getData(Constant.NAME));
            sendparams.put(Constant.LAST_NAME, session.getData(Constant.NAME));
            startUPIPayment(Double.parseDouble(amount), "Wallet Recharge");
        }
    }
    private void startUPIPayment(double amount, String orderDes) {

        final String vpa_address = Constant.VPA_PAYEE_ADDRESS.toLowerCase().trim();
        final String vpa_name = Constant.VPA_PAYEE_NAME.toLowerCase().trim();


        PaymentDetail payment = new PaymentDetail(
                vpa_address,  //vpa/upi = your vpa/upi
                vpa_name,       //name = your name
                "",//payeeMerchantCode = only if you have merchantCode else pass empty string
                "",                     //txnRefId =  if you pass empty string we will generate txnRefId for you
                orderDes,          //description =
                String.valueOf(Constant.formater.format(amount)));

        MainActivity.initializeWalletRechargeUPI((MainActivity) activity,payment);

    }


    public void CreateOrderId(double payAmount) {

        String[] amount = String.valueOf(payAmount * 100).split("\\.");

        Map<String, String> params = new HashMap<>();
        params.put("amount", amount[0]);
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        JSONObject object = new JSONObject(response);
                        if (!object.getBoolean(Constant.ERROR)) {
                            startPayment(object.getString("id"), object.getString("amount"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, getActivity(), Constant.Get_RazorPay_OrderId, params, true);

    }

    public void startPayment(String orderId, String payAmount) {
        Checkout checkout = new Checkout();
        checkout.setKeyID(Constant.RAZOR_PAY_KEY_VALUE);
        checkout.setImage(R.drawable.ic_launcher);

        try {
            JSONObject options = new JSONObject();
            options.put(Constant.NAME, session.getData(Session.KEY_NAME));
            options.put(Constant.ORDER_ID, orderId);
            options.put(Constant.CURRENCY, "INR");
            options.put(Constant.AMOUNT, payAmount);

            JSONObject preFill = new JSONObject();
            preFill.put(Constant.EMAIL, session.getData(Session.KEY_EMAIL));
            preFill.put(Constant.CONTACT, session.getData(Session.KEY_MOBILE));
            options.put("prefill", preFill);

            checkout.open(getActivity(), options);
        } catch (Exception e) {
            Log.d("Payment : ", "Error in starting Razorpay Checkout", e);
        }
    }

    public void getTransactionData(Activity activity, Session session) {

        walletTransactions = new ArrayList<>();


        Map<String, String> params = new HashMap<String, String>();
        params.put(Constant.GET_USER_TRANSACTION, Constant.GetVal);
        params.put(Constant.USER_ID, session.getData(Constant.ID));
        params.put(Constant.TYPE, Constant.TYPE_WALLET_TRANSACTION);
        params.put(Constant.OFFSET, "" + offset);
        params.put(Constant.LIMIT, "" + Constant.LOAD_ITEM_LIMIT);

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        JSONObject objectbject = new JSONObject(response);
                        if (!objectbject.getBoolean(Constant.ERROR)) {
                            total = Integer.parseInt(objectbject.getString(Constant.TOTAL));
                            session.setData(Constant.TOTAL, String.valueOf(total));

                            JSONObject object = new JSONObject(response);
                            JSONArray jsonArray = object.getJSONArray(Constant.DATA);

                            Gson g = new Gson();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                if (jsonObject1 != null) {
                                    WalletTransaction transaction = g.fromJson(jsonObject1.toString(), WalletTransaction.class);
                                    walletTransactions.add(transaction);
                                } else {
                                    break;
                                }

                            }
                                walletTransactionAdapter = new WalletTransactionAdapter(activity, walletTransactions);
                                walletTransactionAdapter.setHasStableIds(true);
                                recyclerView.setAdapter(walletTransactionAdapter);
                                scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                                    @Override
                                    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                                        // if (diff == 0) {
                                        if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                                            LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                                            if (walletTransactions.size() < total) {
                                                if (!isLoadMore) {
                                                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == walletTransactions.size() - 1) {
                                                        //bottom of list!
                                                        walletTransactions.add(null);
                                                        walletTransactionAdapter.notifyItemInserted(walletTransactions.size() - 1);
                                                        new Handler().postDelayed(new Runnable() {
                                                            @Override
                                                            public void run() {

                                                                offset += Constant.LOAD_ITEM_LIMIT;
                                                                Map<String, String> params = new HashMap<>();
                                                                params.put(Constant.GET_USER_TRANSACTION, Constant.GetVal);
                                                                params.put(Constant.USER_ID, session.getData(Constant.ID));
                                                                params.put(Constant.TYPE, Constant.TYPE_WALLET_TRANSACTION);
                                                                params.put(Constant.OFFSET, "" + offset);
                                                                params.put(Constant.LIMIT, "" + Constant.LOAD_ITEM_LIMIT);



                                                                ApiConfig.RequestToVolley(new VolleyCallback() {
                                                                    @Override
                                                                    public void onSuccess(boolean result, String response) {

                                                                        if (result) {
                                                                            try {
                                                                                JSONObject objectbject1 = new JSONObject(response);
                                                                                if (!objectbject1.getBoolean(Constant.ERROR)) {

                                                                                    session.setData(Constant.TOTAL, objectbject1.getString(Constant.TOTAL));

                                                                                    walletTransactions.remove(walletTransactions.size() - 1);
                                                                                    walletTransactionAdapter.notifyItemRemoved(walletTransactions.size());

                                                                                    JSONObject object = new JSONObject(response);
                                                                                    JSONArray jsonArray = object.getJSONArray(Constant.DATA);

                                                                                    Gson g = new Gson();


                                                                                    for (int i = 0; i < jsonArray.length(); i++) {
                                                                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                                                                        if (jsonObject1 != null) {
                                                                                            WalletTransaction walletTransaction = g.fromJson(jsonObject1.toString(), WalletTransaction.class);
                                                                                            walletTransactions.add(walletTransaction);
                                                                                        } else {
                                                                                            break;
                                                                                        }
                                                                                    }
                                                                                    walletTransactionAdapter.notifyDataSetChanged();
                                                                                    walletTransactionAdapter.setLoaded();
                                                                                    isLoadMore = false;
                                                                                }
                                                                            } catch (JSONException e) {
                                                                                e.printStackTrace();
                                                                            }
                                                                        }
                                                                    }



                                                                }, activity, Constant.TRANSACTION_URL, params, false);

                                                            }
                                                        }, 0);
                                                        isLoadMore = true;
                                                    }

                                                }
                                            }
                                        }
                                    }
                                });

                        } else {
                            recyclerView.setVisibility(View.GONE);
                            tvAlert.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }, activity, Constant.TRANSACTION_URL, params, true);

    }



    @Override
    public void onResume() {
        super.onResume();
        Constant.TOOLBAR_TITLE = getString(R.string.wallet_history);
        Session.setCount(Constant.UNREAD_WALLET_COUNT, 0, getContext());
        ApiConfig.updateNavItemCounter(DrawerActivity.navigationView, R.id.menu_wallet_history,
                Session.getCount(Constant.UNREAD_WALLET_COUNT, getContext()));
        hideKeyboard();
        activity.invalidateOptionsMenu();


    }


    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(root.getApplicationWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.toolbar_cart).setVisible(false);
        menu.findItem(R.id.toolbar_sort).setVisible(false);
        menu.findItem(R.id.toolbar_search).setVisible(false);
    }

    @Override
    public void onTransactionCancelled() {

        Toast.makeText(activity, "Transaction cancelled", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTransactionCompleted(@NotNull TransactionDetails transactionDetails) {

        AddWalletBalance(activity, new Session(activity), WalletTransactionFragment.amount, WalletTransactionFragment.msg, "razorpay");


    }

    @Override
    public void onStart() {
        super.onStart();

    }
}
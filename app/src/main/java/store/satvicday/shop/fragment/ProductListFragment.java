package store.satvicday.shop.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.BackPressedOnPack;
import store.satvicday.shop.activity.MainActivity;
import store.satvicday.shop.adapter.ProductLoadMoreAdapter;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.AppController;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.Utils;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.PriceVariation;
import store.satvicday.shop.model.Product;
import store.satvicday.shop.model.Slider;
import store.satvicday.shop.model.packs.Pack;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static store.satvicday.shop.helper.ApiConfig.AddMultipleProductInCart;
import static store.satvicday.shop.helper.ApiConfig.GetSettings;


public class ProductListFragment extends Fragment {
    public static ArrayList<Product> productArrayList;
    public static ProductLoadMoreAdapter mAdapter;
    View root;
    Session session;
    int total;
    int position;
    boolean isSort = true;
    Activity activity;
    ProgressBar progressBar;
    int offset = 0;
    private String id, filterBy, from, fromPack;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeLayout;
    private int filterIndex;
    private TextView tvAlert;
    private boolean isLoadMore = false;
    public static Button btnCart;
    private PaymentFragment.TypePurchase typePurchase;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_product_list, container, false);


        offset = 0;
        activity = getActivity();

        session = new Session(activity);
        Paper.init(activity);
        typePurchase = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE);
        setHasOptionsMenu(true);


        from = getArguments().getString("from");
        fromPack = getArguments().getString("from_pack");
        progressBar = root.findViewById(R.id.progressBar);

        if (fromPack.equals(Constant.PACK)) {
            Paper.init(activity);
            Paper.book().write(Constant.CUSTOM_TRANSACTION_TYPE, PaymentFragment.TypePurchase.PACK);

        }


        id = getArguments().getString("id");
        swipeLayout = root.findViewById(R.id.swipeLayout);
        tvAlert = root.findViewById(R.id.tvAlert);
        recyclerView = root.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        btnCart = root.findViewById(R.id.btnCart);

        GetSettings(activity);
        filterIndex = -1;


        if (from.equals("regular")) {
            if (AppController.isConnected(activity)) {
                GetData();
            }
        } else if (from.equals("pack")) {
            if (AppController.isConnected(activity)) {


                GetData();
            }
        } else if (from.equals("similar")) {

            isSort = false;

        } else if (from.equals("section")) {
            progressBar.setVisibility(View.GONE);


            if (AppController.isConnected(activity)) {
                isSort = false;
                progressBar.setVisibility(View.GONE);
                position = getArguments().getInt("position", 0);

                ProductLoadMoreAdapter mAdapter = new
                        ProductLoadMoreAdapter(activity, HomeFragment.sectionList.get(position).getProductList(),
                        R.layout.lyt_item_list, true);
                recyclerView.setAdapter(mAdapter);


            }
        } else if (from.equals(Constant.PRODUCT_DETAIL)) {


            progressBar.setVisibility(View.GONE);
            GetSimilarData();
        }


        swipeLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(false);
                offset = 0;

                if (!from.equals("Section")) {
                    GetData();
                }
                // GetSimilarData();

            }
        });


        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.fm.beginTransaction().add(R.id.container, new CartFragment()).addToBackStack(null).commit();

            }
        });


        ProductDetailFragment.backPressedOnPackl = new BackPressedOnPack() {
            @Override
            public void onBack(boolean yes) {
                GetData();

            }
        };


        return root;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.toolbar_sort) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(activity.getResources().getString(R.string.filterby));
            builder.setSingleChoiceItems(Constant.filtervalues, filterIndex, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    filterIndex = item;
                    switch (item) {
                        case 0:
                            filterBy = Constant.NEW;
                            break;
                        case 1:
                            filterBy = Constant.OLD;
                            break;
                        case 2:
                            filterBy = Constant.HIGH;
                            break;
                        case 3:
                            filterBy = Constant.LOW;
                            break;


                    }


                    if (item != -1)
                        ReLoadData();
                    dialog.dismiss();
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        } else if (item.getItemId() == android.R.id.home) {
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);

        if (typePurchase == PaymentFragment.TypePurchase.PACK) {
            menu.findItem(R.id.toolbar_cart).setVisible(false);
            menu.findItem(R.id.toolbar_sort).setVisible(false);
            menu.findItem(R.id.toolbar_search).setVisible(false);

        } else {

            menu.findItem(R.id.toolbar_cart).setVisible(true);
            menu.findItem(R.id.toolbar_search).setVisible(true);
            menu.findItem(R.id.toolbar_sort).setVisible(isSort);
            menu.findItem(R.id.toolbar_cart).setIcon(ApiConfig.buildCounterDrawable(Constant.TOTAL_CART_ITEM, R.drawable.ic_cart, activity));

        }

    }


    public void GetData() {


        progressBar.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<>();
        params.put(Constant.SUB_CATEGORY_ID, id);
        params.put(Constant.USER_ID, session.getData(Constant.ID));
        params.put(Constant.LIMIT, "" + Constant.LOAD_ITEM_LIMIT);
        params.put(Constant.OFFSET, "" + offset);

        productArrayList = new ArrayList<>();


        if (filterIndex != -1) {
            params.put(Constant.SORT, filterBy);
        }

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {

                if (result) {
                    try {
                        JSONObject objectbject = new JSONObject(response);
                        if (!objectbject.getBoolean(Constant.ERROR)) {
                            total = Integer.parseInt(objectbject.getString(Constant.TOTAL));
                            //  if (offset == 0) {
                            productArrayList = new ArrayList<>();
                            tvAlert.setVisibility(View.GONE);
                            //}
                            JSONObject object = new JSONObject(response);
                            JSONArray jsonArray = object.getJSONArray(Constant.DATA);

                            //Filetring pack products
                            if (fromPack.equals(Constant.PACK)) {


                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    productArrayList.clear();
                                    productArrayList.addAll(ApiConfig.GetProductList(jsonArray).stream()
                                            .filter(product -> {
                                                        if ((Double.parseDouble(product.getPriceVariations().get(0)
                                                                .getDiscounted_price()) % 5 == 0 && !product.getType().equals("fixed"))
                                                        ) {

                                                            return true;
                                                        } else {

                                                            Toast.makeText(activity, "One of the products price not divisible by 5", Toast.LENGTH_LONG).show();
                                                        }
                                                        return false;
                                                    }
                                            ).collect(Collectors.toList()));

                                } else {


                                    ArrayList<Product> filterProductList = new ArrayList<>();
                                    for (Product product : productArrayList) {

                                        if ((Double.parseDouble(product.getPriceVariations().get(0)
                                                .getDiscounted_price()) % 5 == 0) && !product.getType().equals("fixed")) {


                                            filterProductList.add(product);
                                        } else {

                                            Toast.makeText(activity, "One of the products price not divisible by 5", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                    productArrayList.clear();
                                    productArrayList.addAll(filterProductList);

                                }

                            } else {


                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    productArrayList.addAll(ApiConfig.GetProductList(jsonArray)
                                            .stream().filter(product -> {
                                                if (!product.getType().equals("fixed")) {
                                                    return true;
                                                }
                                                return false;
                                            }).collect(Collectors.toList()));
                                } else {
                                    ArrayList<Product> filterProductList = new ArrayList<>();
                                    for (Product product : productArrayList) {

                                        if (!product.getType().equals("fixed")) {

                                            filterProductList.add(product);
                                        }

                                    }
                                    productArrayList.addAll(filterProductList);

                                }

                            }


                            //  if (offset == 0) {
                            mAdapter = new ProductLoadMoreAdapter(activity, productArrayList,
                                    R.layout.lyt_item_list, false);
                            mAdapter.setHasStableIds(true);
                            recyclerView.setAdapter(mAdapter);

                            progressBar.setVisibility(View.GONE);
//                                nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//                                    @Override
//                                    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//
//                                        // if (diff == 0) {
//                                        if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
//                                            LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//                                            if (productArrayList.size() < total) {
//                                                if (!isLoadMore) {
//                                                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == productArrayList.size() - 1) {
//                                                        //bottom of list!
//                                                        productArrayList.add(null);
//                                                        mAdapter.notifyItemInserted(productArrayList.size() - 1);
//
//                                                        offset += Integer.parseInt("" + Constant.LOAD_ITEM_LIMIT);
//                                                        Map<String, String> params = new HashMap<>();
//                                                        params.put(Constant.SUB_CATEGORY_ID, id);
//                                                        params.put(Constant.USER_ID, session.getData(Constant.ID));
//                                                        params.put(Constant.LIMIT, "" + Constant.LOAD_ITEM_LIMIT);
//                                                        params.put(Constant.OFFSET, offset + "");
//                                                        if (filterIndex != -1)
//                                                            params.put(Constant.SORT, filterBy);
//
//                                                        ApiConfig.RequestToVolley(new VolleyCallback() {
//                                                            @Override
//                                                            public void onSuccess(boolean result, String response) {
//
//                                                                if (result) {
//                                                                    try {
//                                                                        JSONObject objectbject = new JSONObject(response);
//                                                                        if (!objectbject.getBoolean(Constant.ERROR)) {
//
//                                                                            JSONObject object = new JSONObject(response);
//                                                                            JSONArray jsonArray = object.getJSONArray(Constant.DATA);
//                                                                            productArrayList.remove(productArrayList.size() - 1);
//                                                                            mAdapter.notifyItemRemoved(productArrayList.size());
//
//
//                                                                            if (fromPack.equals(Constant.PACK)){
//
//
//                                                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                                                                                    productArrayList.addAll(ApiConfig.GetProductList(jsonArray).stream()
//                                                                                            .filter(product -> {
//                                                                                                        if (Double.parseDouble(
//                                                                                                                product.getPriceVariations().get(0)
//                                                                                                                .getDiscounted_price()) % 5 == 0&&!product.getType().equals("fixed")
//                                                                                                        ){
//
//                                                                                                            return true;
//                                                                                                        }
//                                                                                                        return false;
//                                                                                                    }
//                                                                                            ).collect(Collectors.toList()));
//
//                                                                                }else {
//                                                                                    ArrayList<Product> filterProductList = new ArrayList<>();
//                                                                                    for (Product product:productArrayList){
//
//                                                                                        if (Double.parseDouble(
//                                                                                                product.getPriceVariations().get(0)
//                                                                                                        .getDiscounted_price()) % 5 == 0&&!product.getType().equals("fixed")
//                                                                                                        ){
//
//                                                                                            filterProductList.add(product);
//
//                                                                                        }
//
//                                                                                    }
//                                                                                    productArrayList.addAll(filterProductList);
//
//
//                                                                                }
//
//                                                                            }else {
//
//
//                                                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                                                                                    productArrayList.addAll(ApiConfig.GetProductList(jsonArray)
//                                                                                            .stream().filter(product -> {
//                                                                                                if (!product.getType().equals("fixed")) {
//                                                                                                    return true;
//                                                                                                }
//                                                                                                return false;
//                                                                                            }).collect(Collectors.toList()));
//                                                                                } else {
//                                                                                    ArrayList<Product> filterProductList = new ArrayList<>();
//                                                                                    for (Product product : productArrayList) {
//
//                                                                                        if (!product.getType().equals("fixed")) {
//
//                                                                                            filterProductList.add(product);
//
//                                                                                        }
//
//                                                                                    }
//                                                                                    productArrayList.addAll(filterProductList);
//
//
//                                                                                }
//
//                                                                            }
//                                                                            mAdapter.notifyDataSetChanged();
//                                                                            mAdapter.setLoaded();
//                                                                            isLoadMore = false;
//                                                                        }
//                                                                    } catch (JSONException e) {
//                                                                        e.printStackTrace();
//                                                                    }
//                                                                }
//                                                            }
//                                                        }, activity, Constant.GET_PRODUCT_BY_SUB_CATE, params, false);
//                                                        isLoadMore = true;
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }
//                                });
                            //    }
                        } else {
                            if (offset == 0) {

                                progressBar.setVisibility(View.GONE);
                                tvAlert.setVisibility(View.GONE);
                            }
                        }

                    } catch (JSONException e) {

                        progressBar.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                }
            }
        }, activity, Constant.GET_PRODUCT_BY_SUB_CATE, params, false);
    }


    private void GetSimilarData() {

        progressBar.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<>();
        params.put(Constant.GET_SIMILAR_PRODUCT, Constant.GetVal);
        params.put(Constant.PRODUCT_ID, id);
        params.put(Constant.CATEGORY_ID, getArguments().getString("cat_id"));
        params.put(Constant.USER_ID, session.getData(Constant.ID));
        params.put(Constant.LIMIT, "" + Constant.LOAD_ITEM_LIMIT);
        params.put(Constant.OFFSET, "" + offset);

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {

                if (result) {
                    try {
                        JSONObject objectbject = new JSONObject(response);
                        if (!objectbject.getBoolean(Constant.ERROR)) {

                            progressBar.setVisibility(View.GONE);

                            total = Integer.parseInt(objectbject.getString(Constant.TOTAL));
                            if (offset == 0) {
                                productArrayList = new ArrayList<>();
                                tvAlert.setVisibility(View.GONE);
                            }
                            JSONObject object = new JSONObject(response);
                            JSONArray jsonArray = object.getJSONArray(Constant.DATA);

                            productArrayList.clear();


                            if (typePurchase == PaymentFragment.TypePurchase.PACK) {
                                ArrayList<Product> filterProductList = new ArrayList<>();

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    productArrayList.addAll(ApiConfig.GetProductList(jsonArray).stream()
                                            .filter(product -> {
                                                        if ((Double.parseDouble(product.getPriceVariations().get(0)
                                                                .getDiscounted_price()) % 5 == 0 && !product.getType().equals("fixed"))
                                                        ) {


                                                            return true;
                                                        }else {

                                                            Toast.makeText(activity, "One of the products price not divisible by 5",Toast.LENGTH_LONG).show();
                                                        }
                                                        return false;
                                                    }
                                            ).collect(Collectors.toList()));
                                } else {
                                    for (Product product : productArrayList) {

                                        if ((Double.parseDouble(product.getPriceVariations().get(0)
                                                .getDiscounted_price()) % 5 == 0)) {
                                            productArrayList.add(product);
                                        }
                                    }
                                }

                            } else {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    productArrayList.addAll(ApiConfig.GetProductList(jsonArray)
                                            .stream().filter(product -> {
                                                if (!product.getType().equals("fixed")) {
                                                    return true;
                                                }
                                                return false;
                                            }).collect(Collectors.toList()));
                                } else {


                                    for (Product product : ApiConfig.GetProductList(jsonArray)) {

                                        if (!product.getType().equals("fixed")) {
                                            productArrayList.add(product);


                                        }

                                    }


                                }

                            }

//                            if (offset == 0) {
                            mAdapter = new ProductLoadMoreAdapter(activity, productArrayList, R.layout.lyt_item_list, false);
                            mAdapter.setHasStableIds(true);
                            recyclerView.setAdapter(mAdapter);
                            progressBar.setVisibility(View.GONE);
//                                nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//                                    @Override
//                                    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//
//                                        // if (diff == 0) {
//                                        if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
//                                            LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//                                            if (productArrayList.size() < total) {
//                                                if (!isLoadMore) {
//                                                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == productArrayList.size() - 1) {
//                                                        //bottom of list!
//                                                        productArrayList.add(null);
//                                                        mAdapter.notifyItemInserted(productArrayList.size() - 1);
//
//                                                        offset += Integer.parseInt("" + Constant.LOAD_ITEM_LIMIT);
//                                                        Map<String, String> params = new HashMap<>();
//                                                        params.put(Constant.GET_SIMILAR_PRODUCT, Constant.GetVal);
//                                                        params.put(Constant.PRODUCT_ID, id);
//                                                        params.put(Constant.CATEGORY_ID, getArguments().getString("cat_id"));
//                                                        params.put(Constant.USER_ID, session.getData(Constant.ID));
//                                                        params.put(Constant.LIMIT, "" + Constant.LOAD_ITEM_LIMIT);
//                                                        params.put(Constant.OFFSET, "" + offset);
//
//                                                        ApiConfig.RequestToVolley(new VolleyCallback() {
//                                                            @Override
//                                                            public void onSuccess(boolean result, String response) {
//
//                                                                if (result) {
//                                                                    try {
//                                                                        JSONObject objectbject = new JSONObject(response);
//                                                                        if (!objectbject.getBoolean(Constant.ERROR)) {
//
//                                                                            Log.d("SIMILARRRRRRRRR", response.toString());
//                                                                            JSONObject object = new JSONObject(response);
//                                                                            JSONArray jsonArray = object.getJSONArray(Constant.DATA);
//                                                                            productArrayList.remove(productArrayList.size() - 1);
//                                                                            mAdapter.notifyItemRemoved(productArrayList.size());
//
//                                                                            productArrayList.addAll(ApiConfig.GetProductList(jsonArray));
//                                                                            mAdapter.notifyDataSetChanged();
//                                                                            mAdapter.setLoaded();
//                                                                            isLoadMore = false;
//                                                                        }
//                                                                    } catch (JSONException e) {
//                                                                        e.printStackTrace();
//                                                                    }
//                                                                }
//                                                            }
//                                                        }, activity, Constant.GET_SIMILAR_PRODUCT_URL, params, false);
//                                                        isLoadMore = true;
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }
//                                });
                            //     }
                        } else {
                            if (offset == 0) {
                                progressBar.setVisibility(View.GONE);
                                tvAlert.setVisibility(View.GONE);
                            }
                        }
                    } catch (JSONException e) {

                        progressBar.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                }
            }
        }, activity, Constant.GET_SIMILAR_PRODUCT_URL, params, false);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();


        if (typePurchase == PaymentFragment.TypePurchase.PACK) {

            Pack pack = Paper.book().read(Constant.PACK_DETAILS);
            Constant.TOOLBAR_TITLE = getResources().getString(R.string.rupee_symbol)
                    + " " + pack.getPackValue() + " " + getResources().getString(R.string.pack);
        } else {
            if (!from.equals("section")) {
                GetData();
                setHasOptionsMenu(true);
                Constant.TOOLBAR_TITLE = getArguments().getString("name");

            }

        }


        getActivity().invalidateOptionsMenu();
        hideKeyboard();


    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(root.getApplicationWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        AddMultipleProductInCart(session, activity, Constant.CartValues);
    }

    private void ReLoadData() {
        if (AppController.isConnected(activity)) {
            if (!from.equals("section")) {
                GetData();
            }


        }


    }


}
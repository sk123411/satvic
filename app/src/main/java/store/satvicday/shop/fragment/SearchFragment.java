package store.satvicday.shop.fragment;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.GetSearchProducts;
import store.satvicday.shop.adapter.ProductAdapter;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.Product;

import static store.satvicday.shop.helper.ApiConfig.AddMultipleProductInCart;


public class SearchFragment extends Fragment {
    public static ArrayList<Product> productArrayList;
    public static ProductAdapter productAdapter;
    public ProgressBar progressBar;
    View root;
    RecyclerView recycleview;
    TextView noResult, msg;
    Session session;
    Activity activity;
    SearchView searchview;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_search, container, false);
        activity = getActivity();
        session = new Session(getContext());

        SearchRequestAll(session,activity);
        setHasOptionsMenu(true);
        recycleview = root.findViewById(R.id.recycleview);
        productArrayList = new ArrayList<>();
        noResult = root.findViewById(R.id.noResult);
        msg = root.findViewById(R.id.msg);
        progressBar = root.findViewById(R.id.pBar);
        searchview = root.findViewById(R.id.searchview);
        swipeRefreshLayout = root.findViewById(R.id.refreshLayout);


        progressBar.setVisibility(View.GONE);
        Constant.CartValues = new HashMap<>();
        recycleview.setLayoutManager(new LinearLayoutManager(getContext()));
        searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if (!query.equals("")){
                    Paper.init(activity);
                    ArrayList<Product> savedList = Paper.book().read(Constant.FILTER_PRODUCTS);
                    ArrayList<Product> tempList = new ArrayList<>();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        tempList.addAll(savedList.stream().filter(product -> {
                            if (product.getName().toLowerCase().contains(query)){
                                return true;
                            }else {
                                return false;
                            }
                        }).collect(Collectors.toList()));
                    }else {

                        for (Product product: savedList) {

                            if (product.getName().toLowerCase().contains(query)) {

                                tempList.add(product);
                            }
                        }
                    }
                    productAdapter = new ProductAdapter(tempList, R.layout.lyt_item_list, activity);
                    recycleview.setAdapter(productAdapter);

                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!newText.equals("")) {
                    Paper.init(activity);
                    ArrayList<Product> savedList = Paper.book().read(Constant.FILTER_PRODUCTS);
                    ArrayList<Product> tempList = new ArrayList<>();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        tempList.addAll(savedList.stream().filter(product -> {
                            if (product.getName().toLowerCase().contains(newText)){
                                return true;
                            }else {
                                return false;
                            }
                        }).collect(Collectors.toList()));
                    }else {

                        for (Product product: savedList) {

                            if (product.getName().toLowerCase().contains(newText)) {

                                tempList.add(product);
                            }
                        }
                    }
                    productAdapter = new ProductAdapter(tempList, R.layout.lyt_item_list, activity);
                    recycleview.setAdapter(productAdapter);

                } else {
                    newText.length();
                }
//                if (Constant.CartValues.size() > 0) {
//                    AddMultipleProductInCart(session, activity, Constant.CartValues);
//                }
                return false;
            }
        });



        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (productAdapter!=null&&productAdapter.productList.size() > 0){
                    productAdapter.notifyDataSetChanged();
                }


                swipeRefreshLayout.setRefreshing(false);
            }
        });



        return root;
    }


    public void SearchRequest(final String query, GetSearchProducts getSearchProducts) {  //json request for product search
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constant.TYPE, Constant.PRODUCT_SEARCH);
        params.put(Constant.USER_ID, session.getData(Constant.ID));
        params.put(Constant.SEARCH, query);
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        productArrayList = new ArrayList<>();
                        JSONObject objectbject = new JSONObject(response);
                        if (!objectbject.getBoolean(Constant.ERROR)) {

                            JSONObject object = new JSONObject(response);
                            JSONArray jsonArray = object.getJSONArray(Constant.DATA);

                            productArrayList = ApiConfig.GetProductList(jsonArray);
                            ArrayList<Product> filterProductList = new ArrayList<>();
                            for (Product product:productArrayList){
                                if (!product.getType().toLowerCase().equals("fixed")){
                                    filterProductList.add(product);
                                }
                            }

                            productAdapter = new ProductAdapter(filterProductList, R.layout.lyt_item_list, activity);
                            recycleview.setAdapter(productAdapter);
                            noResult.setVisibility(View.GONE);
                            msg.setVisibility(View.GONE);
                        } else {
                            noResult.setVisibility(View.VISIBLE);
                            msg.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            productArrayList.clear();
                            recycleview.setAdapter(new ProductAdapter(productArrayList, R.layout.lyt_item_list, activity));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, activity, Constant.PRODUCT_SEARCH_URL, params, false);

    }



    public static void SearchRequestAll(Session session,Activity activity) {  //json request for product search
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constant.TYPE, Constant.PRODUCT_SEARCH);
        params.put(Constant.USER_ID, session.getData(Constant.ID));
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        ArrayList<Product> productArrayList = new ArrayList<>();
                        JSONObject objectbject = new JSONObject(response);
                        if (!objectbject.getBoolean(Constant.ERROR)) {

                            JSONObject object = new JSONObject(response);
                            JSONArray jsonArray = object.getJSONArray(Constant.DATA);

                            productArrayList = ApiConfig.GetProductList(jsonArray);
                            ArrayList<Product> filterProductList = new ArrayList<>();
                            for (Product product:productArrayList){
                                if (!product.getType().toLowerCase().equals("fixed")){
                                    filterProductList.add(product);
                                }
                            }


                            Paper.init(activity);
                            Paper.book().write(Constant.FILTER_PRODUCTS, filterProductList);

                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, activity, Constant.PRODUCT_SEARCH_URL, params, false);

    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        menu.findItem(R.id.toolbar_cart).setVisible(true);
        menu.findItem(R.id.toolbar_search).setVisible(false);
        menu.findItem(R.id.toolbar_sort).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        Constant.TOOLBAR_TITLE = getString(R.string.search);
        activity.invalidateOptionsMenu();

        searchview.setIconifiedByDefault(true);
        searchview.setFocusable(true);
        searchview.setIconified(false);
        searchview.requestFocusFromTouch();

        session = new Session(activity);
        activity = getActivity();

    }

    @Override
    public void onPause() {
        super.onPause();
        if (Constant.CartValues.size() > 0) {
            AddMultipleProductInCart(session, activity, Constant.CartValues);
        }
    }
}
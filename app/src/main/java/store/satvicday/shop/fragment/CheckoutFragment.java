package store.satvicday.shop.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.BackPressedOnPack;
import store.satvicday.shop.activity.BackPressedOnPackSecond;
import store.satvicday.shop.activity.GetWalletPrice;
import store.satvicday.shop.activity.MainActivity;
import store.satvicday.shop.activity.checkout.CheckoutActivity;
import store.satvicday.shop.activity.checkout.CheckoutItemAdapter;
import store.satvicday.shop.activity.subscribe.GetProductTax;
import store.satvicday.shop.activity.subscribe.Subscribe;
import store.satvicday.shop.adapter.CheckoutItemListAdapter;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.AppController;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.Cart;
import store.satvicday.shop.model.Category;
import store.satvicday.shop.model.Product;
import store.satvicday.shop.model.packs.Pack;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static store.satvicday.shop.helper.ApiConfig.AddMultipleProductInCart;

public class CheckoutFragment extends Fragment {
    public static String pCode = "", appliedCode = "", deliveryCharge = "0";
    public static LinearLayout deliveryLyt;
    public double pCodeDiscount = 0.0, subtotal = 0.0, dCharge = 0.0, taxAmt = 0.0, total = 0.0;

    public TextView tvPayment, tvDelivery;
    public static TextView tvConfirmOrder;
    public ArrayList<String> variantIdList, qtyList;
    public TextView tvTaxPercent, tvTaxAmt, tvAlert, tvTotalBeforeTax, tvDeliveryCharge, tvSubTotal, tvPromoCode, tvPCAmount, tvPreTotal, tvSubtotalTag, tvWeekTag, tvWeekTag_;
    public LinearLayout lytTax, processLyt;
    RecyclerView recyclerView;
    private LinearLayout additionalRegularLayout, weekLayout;
    public RadioButton ckStandardRadio, ckExpressRadio;
    private RelativeLayout confirmLayout;
    View root;
    RelativeLayout confirmLyt;
    boolean isApplied;
    ImageView imgRefresh;
    Button btnApply;
    ProgressBar progressBar, progressBar2;
    EditText edtPromoCode;
    Session session;
    Activity activity;
    CheckoutItemListAdapter checkoutItemListAdapter;
    public static ArrayList<Cart> carts;
    double totalPercentage = 0.0;
    private PaymentFragment.TypePurchase typePurchase;
    Pack pack;
    Subscribe subscribe;
    RecyclerView checkoutItemList;
    private TextView ckStandardRadioText, ckExpressRadioText;
    private boolean isMember;
    public static BackPressedOnPack backPressedOnPack;
    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_checkout, container, false);

        activity = getActivity();
        session = new Session(activity);
        Paper.init(activity);


        typePurchase = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE);
        if (session.getData(Constant.MEMBER).equals("0")) {
            isMember = false;
        } else {
            isMember = true;
        }


        progressBar = root.findViewById(R.id.progressBar);
        progressBar2 = root.findViewById(R.id.progressBar2);

        lytTax = root.findViewById(R.id.lytTax);
        tvTaxAmt = root.findViewById(R.id.tvTaxAmt);
        tvTaxPercent = root.findViewById(R.id.tvTaxPercent);
        tvDelivery = root.findViewById(R.id.tvDelivery);
        tvPayment = root.findViewById(R.id.tvPayment);
        tvPCAmount = root.findViewById(R.id.tvPCAmount);
        tvPromoCode = root.findViewById(R.id.tvPromoCode);
        tvAlert = root.findViewById(R.id.tvAlert);
        edtPromoCode = root.findViewById(R.id.edtPromoCode);
        tvSubTotal = root.findViewById(R.id.tvSubTotal);
        tvDeliveryCharge = root.findViewById(R.id.tvDeliveryCharge);
        deliveryLyt = root.findViewById(R.id.deliveryLyt);
        confirmLyt = root.findViewById(R.id.confirmLyt);
        tvConfirmOrder = root.findViewById(R.id.tvConfirmOrder);
        processLyt = root.findViewById(R.id.processLyt);
        imgRefresh = root.findViewById(R.id.imgRefresh);
        tvTotalBeforeTax = root.findViewById(R.id.tvTotalBeforeTax);
        additionalRegularLayout = root.findViewById(R.id.ckregularDeliveryLyt);
        weekLayout = root.findViewById(R.id.lytWeek);
        tvWeekTag = root.findViewById(R.id.tvWeekTag_);
        tvWeekTag_ = root.findViewById(R.id.tvWeekTag);


        ckStandardRadio = root.findViewById(R.id.ckStandardRadio);
        ckExpressRadio = root.findViewById(R.id.ckExpressRadio);
        ckExpressRadioText = root.findViewById(R.id.ckExpressRadioText);
        ckStandardRadioText = root.findViewById(R.id.ckStandardRadioText);

        recyclerView = root.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        carts = new ArrayList<>();


        setHasOptionsMenu(true);


        //Hide days left text
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Checkout");

        TextView daysLeftText = toolbar.findViewById(R.id.tbDaysLeftText);
        daysLeftText.setVisibility(View.GONE);

        tvPreTotal = root.findViewById(R.id.tvPreTotal);
        tvPreTotal = root.findViewById(R.id.tvPreTotal);
        tvSubtotalTag = root.findViewById(R.id.tvSubTotalTag);

        btnApply = root.findViewById(R.id.btnApply);
        subscribe = Paper.book().read(Constant.SUBSCRIBE_DETAILS);


        checkoutItemList = root.findViewById(R.id.checkoutItemlist);

        if (typePurchase == PaymentFragment.TypePurchase.REGULAR) {
            additionalRegularLayout.setVisibility(View.VISIBLE);
            checkoutItemList.setVisibility(View.VISIBLE);
            weekLayout.setVisibility(View.GONE);

            setSavedCheckoutItems();


        } else {
            weekLayout.setVisibility(View.VISIBLE);

            additionalRegularLayout.setVisibility(View.GONE);
            checkoutItemList.setVisibility(View.GONE);


        }


        tvConfirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new AddressListFragment();
                Bundle bundle = new Bundle();
                Paper.init(activity);

                if (typePurchase == PaymentFragment.TypePurchase.PACK || typePurchase == PaymentFragment.TypePurchase.SUBSCRIBE) {


                    bundle.putDouble("subtotal", Double.parseDouble(Constant.formater.format(subtotal)));
                    bundle.putDouble("total", Double.parseDouble(Constant.formater.format(total)));
                    bundle.putDouble("taxAmt", Double.parseDouble(Constant.formater.format(taxAmt)));

                    if (typePurchase == PaymentFragment.TypePurchase.SUBSCRIBE) {
                        bundle.putDouble("tax", Constant.SETTING_TAX);

                    }

                    bundle.putDouble("pCodeDiscount", Double.parseDouble(Constant.formater.format(pCodeDiscount)));
                    bundle.putString("pCode", pCode);
                    bundle.putDouble("dCharge", dCharge);
                    AddressListFragment.selectedAddress = "";

                    if (typePurchase == PaymentFragment.TypePurchase.PACK) {
                        bundle.putStringArrayList("variantIdList", pack.getVariantIdList());
                        bundle.putStringArrayList("qtyList", pack.getQtyList());
                        bundle.putDouble("taxAmt", Double.parseDouble(Constant.formater.format(0)));

                    } else {

                    }

                    bundle.putString("from", "process");

                    Paper.book().write(Constant.FROM_CHECKOUT, false);


                    fragment.setArguments(bundle);
                    CheckoutActivity.fm.beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();

                } else {

                    bundle.putDouble("subtotal", Double.parseDouble(Constant.formater.format(subtotal)));
                    bundle.putDouble("total", Double.parseDouble(Constant.formater.format(total)));
                    bundle.putDouble("taxAmt", Double.parseDouble(Constant.formater.format(taxAmt)));
                    bundle.putDouble("tax", Constant.SETTING_TAX);
                    bundle.putDouble("pCodeDiscount", Double.parseDouble(Constant.formater.format(pCodeDiscount)));
                    bundle.putString("pCode", pCode);
                    bundle.putDouble("dCharge", dCharge);
                    AddressListFragment.selectedAddress = "";
                    bundle.putStringArrayList("variantIdList", variantIdList);
                    bundle.putStringArrayList("qtyList", qtyList);
                    bundle.putString("from", "process");
                    fragment.setArguments(bundle);

                    Paper.book().write(Constant.FROM_CHECKOUT, false);

                    MainActivity.fm.beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();


                }


            }
        });



        confirmLyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        imgRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isApplied) {
                    btnApply.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                    btnApply.setText("Apply");
                    edtPromoCode.setText("");
                    tvPromoCode.setVisibility(View.GONE);
                    tvPCAmount.setVisibility(View.GONE);
                    isApplied = false;
                    appliedCode = "";
                    pCode = "";
                    SetDataTotal();
                }
            }
        });


        if (AppController.isConnected(activity)) {


            if (typePurchase == PaymentFragment.TypePurchase.PACK) {
                ArrayList<Cart> tempCarts = Paper.book().read(Constant.CART_ITEMS);
                carts.addAll(tempCarts);
                checkoutItemListAdapter = new CheckoutItemListAdapter(getActivity(), carts);
                recyclerView.setAdapter(checkoutItemListAdapter);
                pack = Paper.book().read(Constant.PACK_DETAILS);


                subtotal = pack.getPack_date_count() * Integer.parseInt(pack.getPackValue());
                total = subtotal;

                confirmLyt.setVisibility(View.VISIBLE);
                deliveryLyt.setVisibility(View.VISIBLE);
                weekLayout.setVisibility(View.VISIBLE);

                SetDataTotal();


            } else if (typePurchase == PaymentFragment.TypePurchase.SUBSCRIBE) {
                ArrayList<Cart> tempCarts = Paper.book().read(Constant.CART_ITEMS);
                carts.addAll(tempCarts);
                checkoutItemListAdapter = new CheckoutItemListAdapter(getActivity(), carts);
                recyclerView.setAdapter(checkoutItemListAdapter);
                subscribe = Paper.book().read(Constant.SUBSCRIBE_DETAILS);


                total = subtotal;
                subtotal = Double.parseDouble(subscribe.getFinalTotal());

                confirmLyt.setVisibility(View.VISIBLE);
                deliveryLyt.setVisibility(View.VISIBLE);
                weekLayout.setVisibility(View.VISIBLE);

                deliveryCharge = "0";
                SetDataTotal();


            } else {

                getCartData();




//                    root.setFocusableInTouchMode(true);
//                    root.requestFocus();
//                    root.setOnKeyListener(new View.OnKeyListener() {
//                        @Override
//                        public boolean onKey(View v, int keyCode, KeyEvent event) {
//
//                            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
//
//                                backPressedOnPack.onBack(true);
//
//                            }
//
//                            return false;
//                        }
//                    });


            }


            ProductDetailFragment.backPressedOnPackSecond = new BackPressedOnPackSecond() {
                @Override
                public void onBack(boolean yes) {

                    getCartData();


                }
            };


            PromoCodeCheck();


        }




        return root;
    }

    private void setSavedCheckoutItems() {
        progressBar2.setVisibility(View.VISIBLE);
        ArrayList<Product> savedList = Paper.book().read(Constant.FILTER_PRODUCTS);
        ArrayList<Product> filterlist = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

            savedList.stream().filter(product -> {
                if (product.getPriceVariations().size() >= 1) {
                    if (Double.parseDouble(
                            product.getPriceVariations().get(0).getDiscounted_price()) <= 100.00) {
                        filterlist.add(product);

                        return true;
                    }
                } else {
                }
                return false;
            }).collect(Collectors.toList());


        } else {

            for (Product product : savedList) {
                if (product.getPriceVariations().size() >= 1) {
                    if (Double.parseDouble(
                            product.getPriceVariations().get(0).getDiscounted_price()) <= 100.00) {
                        filterlist.add(product);
                    }
                } else {

                }

            }


        }
        progressBar2.setVisibility(View.GONE);
        checkoutItemList.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false));
        checkoutItemList.setAdapter(new CheckoutItemAdapter(filterlist, getContext(),
                CheckoutFragment.this));

    }

    private void setCheckoutItems() {
        progressBar2.setVisibility(View.VISIBLE);

        ArrayList<Product> filterlist = new ArrayList<>();
        HashMap<String, String> param = new HashMap<>();
        param.put(Constant.TYPE, Constant.PRODUCT_SEARCH);
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {
                try {

                    JSONObject object = new JSONObject(message);
                    if (!object.getBoolean(Constant.ERROR)) {

                        JSONObject jsonObject = new JSONObject(message);
                        JSONArray jsonArray = jsonObject.getJSONArray(Constant.DATA);
                        ArrayList<Product> productArrayList = ApiConfig.GetProductList(jsonArray);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            ApiConfig.GetProductList(jsonArray).stream().filter(product -> {
                                if (product.getPriceVariations().size() >= 1) {
                                    if (Double.parseDouble(
                                            product.getPriceVariations().get(0).getDiscounted_price()) <= 100.00) {
                                        filterlist.add(product);

                                        return true;
                                    }
                                } else {
                                }
                                return false;
                            }).collect(Collectors.toList());


                        } else {
                            progressBar2.setVisibility(View.GONE);

                            for (Product product : productArrayList) {
                                if (product.getPriceVariations().size() >= 1) {
                                    if (Double.parseDouble(
                                            product.getPriceVariations().get(0).getDiscounted_price()) <= 100.00) {
                                        filterlist.add(product);
                                    }
                                } else {

                                }

                            }
                        }


                        checkoutItemList.setLayoutManager(new LinearLayoutManager(getContext(),
                                LinearLayoutManager.HORIZONTAL, false));
                        checkoutItemList.setAdapter(new CheckoutItemAdapter(filterlist, getContext(), CheckoutFragment.this));
                        progressBar2.setVisibility(View.GONE);

                    }


                } catch (JSONException e) {

                }


            }
        }, activity, "https://store.satvicday.com/api-firebase/products-search.php", param, false);


    }


    public void getCartData() {


        ApiConfig.getCartItemCount(activity, session);
        taxAmt = 0.0;
        total = 0.0;
        carts.clear();
        ckStandardRadio.setChecked(false);
        ckExpressRadio.setChecked(false);

        progressBar.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<>();
        params.put(Constant.GET_USER_CART, Constant.GetVal);
        params.put(Constant.USER_ID, session.getData(Constant.ID));
        params.put(Constant.LIMIT, "" + Constant.TOTAL_CART_ITEM);

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray(Constant.DATA);
                        Gson gson = new Gson();
                        variantIdList = new ArrayList<>();
                        qtyList = new ArrayList<>();

                        carts.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Cart cart = gson.fromJson(String.valueOf(jsonArray.getJSONObject(i)), Cart.class);
                            variantIdList.add(cart.getProduct_variant_id());
                            qtyList.add(cart.getQty());


                            totalPercentage += (Double.parseDouble(cart.getItems().get(0).getTax_percentage()));

                            float price;
                            if (cart.getItems().get(0).getDiscounted_price().equals("0")) {
                                price = Float.parseFloat(cart.getItems().get(0).getPrice());
                            } else {
                                price = Float.parseFloat(cart.getItems().get(0).getDiscounted_price());
                            }

                            double itemTotal = price * (Integer.parseInt(cart.getQty()));

                            double itemTaxAmt=0.0;

                                 itemTaxAmt = (itemTotal * (Double.parseDouble(cart.getItems().get(0).getTax_percentage()) / 100));

                            taxAmt += itemTaxAmt;
                            total += itemTotal;
                            carts.add(cart);
                        }
                        checkoutItemListAdapter = new CheckoutItemListAdapter(getActivity(), carts);
                        recyclerView.setAdapter(checkoutItemListAdapter);
                        setSavedCheckoutItems();

                        confirmLyt.setVisibility(View.VISIBLE);
                        deliveryLyt.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);

                        SetDataTotal();
                        setSavedCheckoutItems();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, activity, Constant.CART_URL, params, false);
    }

    @SuppressLint("SetTextI18n")
    public void SetDataTotal() {
        //  final double[] initialSubtotal= new Double();


                double[] initialSubtotal = new double[1];
                tvTotalBeforeTax.setText(Constant.SETTING_CURRENCY_SYMBOL +
                        Constant.formater.format(Double.parseDouble("" + total)));
                subtotal = total;
                initialSubtotal[0] = subtotal;


                if (total <= Constant.SETTING_MINIMUM_AMOUNT_FOR_FREE_DELIVERY
                ) {

                    if (typePurchase == PaymentFragment.TypePurchase.SUBSCRIBE
                    ) {
                        weekLayout.setVisibility(View.VISIBLE);

                        tvSubtotalTag.setText(activity.getResources().getString(R.string.total_amt)
                                + "(" + subscribe.getDaysCount() + "x" + subscribe.getPriceVariation().getDiscounted_price() + ")");

                        tvDeliveryCharge.setText(activity.getResources().getString(R.string.free));
                        deliveryCharge = "0";
                        dCharge = 0.0;
                        tvWeekTag_.setText(activity.getResources().getString(R.string.days));
                        tvWeekTag.setText(String.valueOf(subscribe.getDaysCount()));

                        GetProductDetail(subscribe.getPriceVariation().getProduct_id(), new GetProductTax() {
                            @Override
                            public void getTax(String tax) {



                                    Subscribe subscribe = Paper.book().read(Constant.SUBSCRIBE_DETAILS);
                                    Constant.SETTING_TAX = Double.parseDouble(tax);

                                    double taxAfterTotal;
                                    taxAmt = Double.parseDouble(tax);
                                    subscribe.setTaxAmt(String.valueOf(taxAmt / 100.00));
                                    taxAfterTotal = Double.parseDouble(subscribe.getFinalTotal()) * taxAmt / 100.00;
                                    taxAmt = taxAfterTotal;
                                    subtotal = Double.parseDouble(subscribe.getFinalTotal()) + taxAmt;
                                    tvTaxPercent.setText("Tax (" + Constant.formater.format((taxAmt * 100) / Double.parseDouble(subscribe.getFinalTotal())) + "%)");
                                    tvTaxAmt.setText("+ " + Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + taxAmt)));
                                    tvSubTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + subtotal)));
                                    tvTotalBeforeTax.setText(Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + subscribe.getFinalTotal())));
                                    taxAmt = Double.parseDouble(subscribe.getPriceVariation().getDiscounted_price()) * Double.parseDouble(tax) / 100.00;
                                    subscribe.setFinalTotal(String.valueOf(subtotal));
                                    Paper.book().write(Constant.SUBSCRIBE_DETAILS, subscribe);



                            }
                        });

                    } else if (typePurchase == PaymentFragment.TypePurchase.PACK) {
                        weekLayout.setVisibility(View.VISIBLE);

                        if (pack != null && typePurchase == PaymentFragment.TypePurchase.PACK) {
                            tvWeekTag.setText(pack.getPack_date_count() == 0 ? "" : String.valueOf(pack.getPack_date_count()));
                            tvWeekTag_.setText(activity.getResources().getString(R.string.week));

                        }
                        tvSubtotalTag.setText(activity.getResources().getString(R.string.total_amt)
                                + "(" + pack.getPack_date_count() + "x" + pack.getPackValue() + ")");
                        tvDeliveryCharge.setText(activity.getResources().getString(R.string.free));
                        deliveryCharge = "0";
                        dCharge = 0.0;


                    } else {

                        try {

                            String isMember = session.getData(Constant.MEMBER);


                            if (isMember.equals("1")) {
                                tvDeliveryCharge.setText(activity.getResources().getString(R.string.free));
                                deliveryCharge = "0";
                                dCharge = 0.0;
                                ckStandardRadio.setEnabled(false);

                            } else {
                                tvDeliveryCharge.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(Constant.SETTING_DELIVERY_CHARGE));

                                subtotal = Double.parseDouble(Constant.formater.format(subtotal + Constant.SETTING_DELIVERY_CHARGE));
                                subtotal = Double.parseDouble(Constant.formater.format(subtotal));

                            }

                        } catch (Exception e) {

                        }
                        if (pCode.isEmpty()) {
                            subtotal = (subtotal + taxAmt);
                        } else {
                            subtotal = (subtotal + taxAmt - pCodeDiscount);
                        }
                        dCharge = tvDeliveryCharge.getText().toString().equals(activity.getString(R.string.free)) ? 0.0 : Constant.SETTING_DELIVERY_CHARGE;
                        tvTaxPercent.setText("Tax (" + Constant.formater.format((taxAmt * 100) / total) + "%)");
                        tvTaxAmt.setText("+ " + Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + taxAmt)));
                        tvSubTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + subtotal)));
                        ckStandardRadio.setChecked(true);


                    }


                } else {
                    weekLayout.setVisibility(View.VISIBLE);
                    if (typePurchase == PaymentFragment.TypePurchase.SUBSCRIBE
                    ) {
                        weekLayout.setVisibility(View.VISIBLE);

                        tvSubtotalTag.setText(activity.getResources().getString(R.string.total_amt)
                                + "(" + subscribe.getDaysCount() + "x" + subscribe.getPriceVariation().getDiscounted_price() + ")");

                        tvDeliveryCharge.setText(activity.getResources().getString(R.string.free));
                        deliveryCharge = "0";
                        dCharge = 0.0;
                        tvWeekTag_.setText(activity.getResources().getString(R.string.days));
                        tvWeekTag.setText(String.valueOf(subscribe.getDaysCount()));

                        GetProductDetail(subscribe.getPriceVariation().getProduct_id(), new GetProductTax() {
                            @Override
                            public void getTax(String tax) {

                                    Subscribe subscribe = Paper.book().read(Constant.SUBSCRIBE_DETAILS);
                                    Constant.SETTING_TAX = Double.parseDouble(tax);

                                    double taxAfterTotal;
                                    taxAmt = Double.parseDouble(tax);
                                    subscribe.setTaxAmt(String.valueOf(taxAmt / 100.00));
                                    taxAfterTotal = Double.parseDouble(subscribe.getFinalTotal()) * taxAmt / 100.00;
                                    taxAmt = taxAfterTotal;
                                    subtotal = Double.parseDouble(subscribe.getFinalTotal()) + taxAmt;
                                    tvTaxPercent.setText("Tax (" + Constant.formater.format((taxAmt * 100) / Double.parseDouble(subscribe.getFinalTotal())) + "%)");
                                    tvTaxAmt.setText("+ " + Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + taxAmt)));
                                    tvSubTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + subtotal)));
                                    tvTotalBeforeTax.setText(Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + subscribe.getFinalTotal())));
                                    taxAmt = Double.parseDouble(subscribe.getPriceVariation().getDiscounted_price()) * Double.parseDouble(tax) / 100.00;
                                    subscribe.setFinalTotal(String.valueOf(subtotal));
                                    Paper.book().write(Constant.SUBSCRIBE_DETAILS, subscribe);



                            }
                        });


                    } else if (typePurchase == PaymentFragment.TypePurchase.PACK) {

                        weekLayout.setVisibility(View.VISIBLE);
                        tvSubtotalTag.setText(activity.getResources().getString(R.string.total_amt)
                                + "(" + pack.getPack_date_count() + "x" + pack.getPackValue() + ")");

                        tvDeliveryCharge.setText(activity.getResources().getString(R.string.free));
                        deliveryCharge = "0";
                        dCharge = 0.0;

                        if (pack != null && typePurchase == PaymentFragment.TypePurchase.PACK) {
                            tvWeekTag.setText(pack.getPack_date_count() == 0 ? "" : String.valueOf(pack.getPack_date_count()));
                            tvWeekTag_.setText(activity.getResources().getString(R.string.week));

                        }


                    } else {

                        try {

                            String isMember = session.getData(Constant.MEMBER);


                            if (isMember.equals("1")) {
                                tvDeliveryCharge.setText(activity.getResources().getString(R.string.free));
                                deliveryCharge = "0";
                                dCharge = 0.0;
                                ckStandardRadio.setEnabled(false);

                            } else {
                                tvDeliveryCharge.setText(activity.getResources().getString(R.string.free));

                                dCharge = tvDeliveryCharge.getText().toString().equals(activity.getString(R.string.free)) ? 0.0 : Constant.SETTINGS_EXPRESS_DELIVERY_CHARGE;
                                subtotal = Double.parseDouble(Constant.formater.format(subtotal + Constant.SETTING_DELIVERY_CHARGE));

                                subtotal = Double.parseDouble(Constant.formater.format(subtotal ));

                            }
                        } catch (Exception e) {


                        }

                        if (pCode.isEmpty()) {
                            subtotal = (subtotal + taxAmt);
                        } else {
                            subtotal = (subtotal + taxAmt - pCodeDiscount);
                        }


                        deliveryCharge = "0";
                        tvTaxPercent.setText("Tax (" + Constant.formater.format((taxAmt * 100) / total) + "%)");
                        tvTaxAmt.setText("+ " + Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + taxAmt)));
                        tvSubTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + subtotal)));


                    }

                }


                if (typePurchase != PaymentFragment.TypePurchase.SUBSCRIBE) {
                    tvTaxPercent.setText("Tax (" + Constant.formater.format((taxAmt * 100) / total) + "%)");

                    tvTaxAmt.setText("+ " + Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + taxAmt)));

                    Constant.SETTING_TAX = Double.parseDouble(Constant.formater.format((taxAmt * 100) / total));

                    tvSubTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + subtotal)));

                }

                ckStandardRadio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ckExpressRadio.setChecked(false);
                        ckStandardRadio.setChecked(true);

                        subtotal = initialSubtotal[0];
                        total = subtotal;

                        if (initialSubtotal[0] <= Constant.SETTING_MINIMUM_AMOUNT_FOR_FREE_DELIVERY
                                && !isMember) {

                            tvDeliveryCharge.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(Constant.SETTING_DELIVERY_CHARGE));
                            subtotal = Double.parseDouble(Constant.formater.format( initialSubtotal[0] + taxAmt+ Constant.SETTING_DELIVERY_CHARGE));

                        } else {

                            tvDeliveryCharge.setText(activity.getResources().getString(R.string.free));
                            deliveryCharge = "0";

                        }

                        dCharge = tvDeliveryCharge.getText().toString().equals(activity.getString(R.string.free)) ? 0.0 : Constant.SETTING_DELIVERY_CHARGE;

                        tvTaxPercent.setText("Tax (" + Constant.formater.format((taxAmt * 100) / total) + "%)");
                        tvTaxAmt.setText("+ " + Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + taxAmt)));
                        tvSubTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + subtotal)));

                    }
                });
                ckExpressRadio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ckExpressRadio.setChecked(true);
                        ckStandardRadio.setChecked(false);
                        ckStandardRadio.setEnabled(true);


                        subtotal = initialSubtotal[0] ;
                        total = subtotal;
                        tvDeliveryCharge.setText(String.valueOf(Constant.formater.format(dCharge)));
                        tvDeliveryCharge.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(Constant.SETTINGS_EXPRESS_DELIVERY_CHARGE));
                        subtotal = Double.parseDouble(Constant.formater.format( initialSubtotal[0] + taxAmt + Constant.SETTINGS_EXPRESS_DELIVERY_CHARGE));


                        dCharge = tvDeliveryCharge.getText().toString().equals(activity.getResources().getString(R.string.free)) ? 0.0 : Constant.SETTINGS_EXPRESS_DELIVERY_CHARGE;

                        tvTaxPercent.setText("Tax (" + Constant.formater.format((taxAmt * 100) / total) + "%)");
                        tvTaxAmt.setText("+ " + Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + taxAmt)));
                        tvSubTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + "" + Constant.formater.format(Double.parseDouble("" + subtotal)));


                    }
                });




    }

    public void PromoCodeCheck() {
        btnApply.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                final String promoCode = edtPromoCode.getText().toString().trim();
                if (promoCode.isEmpty()) {
                    tvAlert.setVisibility(View.VISIBLE);
                    tvAlert.setText("Enter Promo Code");
                } else if (isApplied && promoCode.equals(appliedCode)) {
                    Toast.makeText(getContext(), "promo code already applied", Toast.LENGTH_SHORT).show();
                } else {
                    if (isApplied) {
                        SetDataTotal();
                    }
                    tvAlert.setVisibility(View.GONE);
                    btnApply.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                    Map<String, String> params = new HashMap<>();
                    params.put(Constant.VALIDATE_PROMO_CODE, Constant.GetVal);
                    params.put(Constant.USER_ID, session.getData(Session.KEY_ID));
                    params.put(Constant.PROMO_CODE, promoCode);
                    params.put(Constant.TOTAL, String.valueOf(total));

                    ApiConfig.RequestToVolley(new VolleyCallback() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onSuccess(boolean result, String response) {
                            if (result) {
                                try {
                                    JSONObject object = new JSONObject(response);
                                    //   System.out.println("===res " + response);
                                    if (!object.getBoolean(Constant.ERROR)) {
                                        pCode = object.getString(Constant.PROMO_CODE);
                                        tvPromoCode.setText(getString(R.string.promo_code) + "(" + pCode + ")");
                                        btnApply.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.light_green));
                                        btnApply.setText("Applied");
                                        isApplied = true;
                                        appliedCode = edtPromoCode.getText().toString();
                                        tvPCAmount.setVisibility(View.VISIBLE);
                                        tvPromoCode.setVisibility(View.VISIBLE);
                                        dCharge = tvDeliveryCharge.getText().toString().equals(activity.getString(R.string.free)) ? 0.0 : Constant.SETTING_DELIVERY_CHARGE;
                                        subtotal = (object.getDouble(Constant.DISCOUNTED_AMOUNT) + taxAmt + dCharge);
                                        pCodeDiscount = Double.parseDouble(object.getString(Constant.DISCOUNT));
                                        tvPCAmount.setText("- " + Constant.SETTING_CURRENCY_SYMBOL + pCodeDiscount);
                                        tvSubTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + subtotal);
                                    } else {
                                        btnApply.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                                        btnApply.setText("Apply");
                                        tvAlert.setVisibility(View.VISIBLE);
                                        tvAlert.setText(object.getString("message"));
                                    }
                                    progressBar.setVisibility(View.GONE);
                                    btnApply.setVisibility(View.VISIBLE);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }, activity, Constant.PROMO_CODE_CHECK_URL, params, false);

                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Paper.init(activity);
        Constant.TOOLBAR_TITLE = activity.getResources().getString(R.string.checkout);
        activity.invalidateOptionsMenu();
        hideKeyboard();
    }


    @Override
    public void onPause() {
        super.onPause();
        AddMultipleProductInCart(session, activity, Constant.CartValues);
    }


    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(root.getApplicationWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.toolbar_cart).setVisible(false);
        menu.findItem(R.id.toolbar_sort).setVisible(false);
        menu.findItem(R.id.toolbar_search).setVisible(false);
    }


    private void GetProductDetail(final String productid, GetProductTax getProductTax) {
        root.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constant.PRODUCT_ID, productid);
        if (session.isUserLoggedIn()) {
            params.put(Constant.USER_ID, session.getData(Constant.ID));
        }

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        JSONObject objectbject = new JSONObject(response);
                        if (!objectbject.getBoolean(Constant.ERROR)) {



                            JSONObject object = new JSONObject(response);
                            JSONArray jsonArray = object.getJSONArray(Constant.DATA);


                            JSONObject object1 = jsonArray.getJSONObject(0);


                            if (object1.getString("tax_percentage").isEmpty()){
                                getProductTax.getTax("0.00");


                            }else {
                                getProductTax.getTax(object1.getString("tax_percentage"));
                            }

                            root.findViewById(R.id.progressBar).setVisibility(View.GONE);
                        } else {
                            root.findViewById(R.id.progressBar).setVisibility(View.GONE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        root.findViewById(R.id.progressBar).setVisibility(View.GONE);
                    }
                }
            }
        }, activity, Constant.GET_PRODUCT_DETAIL_URL, params, false);
    }


}
package store.satvicday.shop.fragment.confirmorder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.MainActivity;
import store.satvicday.shop.activity.subscribe.Subscribe;
import store.satvicday.shop.databinding.FragmentOrderConfirmBinding;
import store.satvicday.shop.fragment.PaymentFragment;
import store.satvicday.shop.fragment.TrackOrderFragment;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.packs.Pack;

public class OrderConfirmFragment extends Fragment {

    private Session session;

    private PaymentFragment.TypePurchase typePurchase;
    private FragmentOrderConfirmBinding binding;

    private BottomNavigationView bottomNavigationView;
    public OrderConfirmFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentOrderConfirmBinding.inflate(inflater);
        Toolbar mainToolbar = getActivity().findViewById(R.id.toolbar);
        mainToolbar.setVisibility(View.GONE);
        session = new Session(getActivity());

        Paper.init(getActivity());

        typePurchase = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE);
        binding.prayerImage.setVisibility(View.VISIBLE);

        if (typePurchase== PaymentFragment.TypePurchase.MEMBERSHIP){

            binding.viewAllOrders.setVisibility(View.GONE);

            binding.odContinue.setOnClickListener(v -> {


                navigateToHome();

            });

        }else {

            if (typePurchase== PaymentFragment.TypePurchase.REGULAR){

                RemoveAllItemFromCart(getActivity(), session, new OrderClear() {
                    @Override
                    public void CartClear(String done) {

                    }
                });

            }
            binding.viewAllOrders.setVisibility(View.VISIBLE);
            binding.odContinue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToHome();

                }
            });
            binding.viewAllOrders.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(requireActivity(), MainActivity.class).
                            putExtra("from", "track_order").
                            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));

                }
            });



        }



        binding.getRoot().setFocusableInTouchMode(true);
        binding.getRoot().requestFocus();
        binding.getRoot().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    startActivity(new Intent(v.getContext(), MainActivity.class).putExtra("from", "")
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    return true;
                }
                return false;
            }
        });




        if (typePurchase== PaymentFragment.TypePurchase.PACK){

            Pack pack = Paper.book().read(Constant.PACK_DETAILS);
            binding.odTag.setText(getResources().getString(R.string.success_bought));
        }else if (typePurchase== PaymentFragment.TypePurchase.SUBSCRIBE){
            Subscribe subscribe = Paper.book().read(Constant.SUBSCRIBE_DETAILS);
            binding.odTag.setText(getResources().getString(R.string.success_bought_subscribe));
        }else if (typePurchase== PaymentFragment.TypePurchase.MEMBERSHIP){
            binding.odTag.setText(getResources().getString(R.string.place_order_member_sucessfully));


        }


        return binding.getRoot();
    }




    public void navigateToHome(){

        startActivity(new Intent(getActivity(), MainActivity.class).putExtra(
                "from", ""
        ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK));

    }


    public void RemoveAllItemFromCart(final Activity activity, Session session, OrderClear orderClear) {

        binding.progressBar.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constant.REMOVE_FROM_CART, Constant.GetVal);
        params.put(Constant.USER_ID, session.getData(Constant.ID));

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (!jsonObject.getBoolean(Constant.ERROR)) {
                            Constant.CartValues.clear();
                            Constant.TOTAL_CART_ITEM = 0;
                            activity.invalidateOptionsMenu();
                            binding.progressBar.setVisibility(View.GONE);

                            orderClear.CartClear("done");
                        }
                        binding.progressBar.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        binding.progressBar.setVisibility(View.GONE);
                        orderClear.CartClear("done");
                        e.printStackTrace();
                    }
                }else {
                    orderClear.CartClear("done");
                }
            }
        }, activity, Constant.CART_URL, params, false);
    }


    @Override
    public void onResume() {
        super.onResume();

        if (requireActivity() instanceof MainActivity){

            bottomNavigationView = requireActivity().findViewById(R.id.bottomNavigationView);
            bottomNavigationView.setVisibility(View.GONE);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (requireActivity() instanceof MainActivity){
            bottomNavigationView = requireActivity().findViewById(R.id.bottomNavigationView);
            bottomNavigationView.setVisibility(View.VISIBLE);
        }
    }
}
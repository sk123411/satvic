package store.satvicday.shop.fragment;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.BackPressedOnPack;
import store.satvicday.shop.activity.DrawerActivity;
import store.satvicday.shop.activity.GetWalletPrice;
import store.satvicday.shop.activity.MainActivity;
import store.satvicday.shop.activity.packs.adapter.GetPlanDetails;
import store.satvicday.shop.activity.packs.adapter.PackCategoryAdapter;
import store.satvicday.shop.adapter.CategoryAdapter;
import store.satvicday.shop.adapter.OfferAdapter;
import store.satvicday.shop.adapter.SectionAdapter;
import store.satvicday.shop.adapter.SliderAdapter;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.AppController;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.Utils;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.Category;
import store.satvicday.shop.model.Product;
import store.satvicday.shop.model.Slider;
import store.satvicday.shop.model.packs.Pack;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static store.satvicday.shop.helper.ApiConfig.GetTimeSlotConfig;


public class HomeFragment extends Fragment {

    public static Session session;
    public static ArrayList<Category> categoryArrayList, sectionList,featureList;
    public static List<Pack> packArrayList;
    Activity activity;
    NestedScrollView nestedScrollView;
    RelativeLayout progressBar;
    SwipeRefreshLayout swipeLayout;
    View root;
    int timerDelay = 0, timerWaiting = 0;
    private RecyclerView categoryRecyclerView, packRecyclerView, sectionView, offerView;
    private ArrayList<Slider> sliderArrayList;
    private ViewPager mPager;
    private LinearLayout mMarkersLayout;
    private int size;
    private Timer swipeTimer;
    private Handler handler;
    private Runnable Update;
    private int currentPage = 0;
    SearchView searchview;
    private LinearLayout lytCategory, lytSearchview;
    TextView tvMore, packsTvMore;

    public static void UpdateToken(final String token, Activity activity) {
        Map<String, String> params = new HashMap<>();
        params.put(Constant.TYPE, Constant.REGISTER_DEVICE);
        params.put(Constant.TOKEN, token);
        params.put(Constant.USER_ID, session.getData(Session.KEY_ID));
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        JSONObject object = new JSONObject(response);
                        if (!object.getBoolean(Constant.ERROR)) {
                            session.setData(Session.KEY_FCM_ID, token);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, activity, Constant.RegisterUrl, params, false);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_home, container, false);
        session = new Session(getContext());

        timerDelay = 3000;
        timerWaiting = 3000;

        activity = getActivity();
        GetTimeSlotConfig(session, activity);
        setHasOptionsMenu(true);

        SearchFragment.SearchRequestAll(session,activity);

        progressBar = root.findViewById(R.id.progressBar);
        swipeLayout = root.findViewById(R.id.swipeLayout);

        categoryRecyclerView = root.findViewById(R.id.categoryrecycleview);
        categoryRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        packRecyclerView = root.findViewById(R.id.packsrecycleview);
        packRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));


        sectionView = root.findViewById(R.id.sectionView);
        sectionView.setLayoutManager(new LinearLayoutManager(getContext()));
        sectionView.setNestedScrollingEnabled(false);

        offerView = root.findViewById(R.id.offerView);
        offerView.setLayoutManager(new LinearLayoutManager(getContext()));
        offerView.setNestedScrollingEnabled(false);

        nestedScrollView = root.findViewById(R.id.nestedScrollView);
        mMarkersLayout = root.findViewById(R.id.layout_markers);
        lytCategory = root.findViewById(R.id.lytCategory);
        lytSearchview = root.findViewById(R.id.lytSearchview);

        searchview = root.findViewById(R.id.searchview);

        tvMore = root.findViewById(R.id.tvMore);
        packsTvMore = root.findViewById(R.id.packstvMore);



        searchview.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchview.setIconified(true);
                searchview.onActionViewCollapsed();
                MainActivity.fm.beginTransaction().add(R.id.container, new SearchFragment()).addToBackStack(null).commit();
            }
        });

        tvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!MainActivity.categoryClicked) {
                    MainActivity.fm.beginTransaction().add(R.id.container, MainActivity.categoryFragment).show(MainActivity.categoryFragment).hide(MainActivity.active).commit();
                    MainActivity.categoryClicked = true;
                } else {
                    MainActivity.fm.beginTransaction().show(MainActivity.active).hide(MainActivity.categoryFragment).commit();
                }
                MainActivity.bottomNavigationView.setSelectedItemId(R.id.navigation_category);

                MainActivity.active = MainActivity.categoryFragment;
//                MainActivity.active = MainActivity.categoryFragment;










//                Log.d("CLICKEDDDDDD", "Category");
//                if (!MainActivity.categoryClicked) {
//                    Log.d("CLICKEDDDDDD", "Category" + MainActivity.categoryClicked);
//
//                    CategoryFragment categoryFragment = new CategoryFragment();
//                    Bundle bundle = new Bundle();
//                    bundle.putString("from","");
//                    categoryFragment.setArguments(bundle);
//                    MainActivity.fm.beginTransaction().add(R.id.container, categoryFragment).
//                            show(MainActivity.categoryFragment).hide(MainActivity.active).commit();
//                    MainActivity.active = MainActivity.categoryFragment;
//                    MainActivity.bottomNavigationView.setSelectedItemId(R.id.navigation_category);
//                    MainActivity.categoryClicked = false;
//
//                } else {
//                    Log.d("CLICKEDDDDDD", "Category" + MainActivity.categoryClicked);
//
//                    MainActivity.fm.beginTransaction().show(MainActivity.homeFragment).hide(MainActivity.categoryFragment).commit();
//                    MainActivity.active = MainActivity.homeFragment;
//                    MainActivity.bottomNavigationView.setSelectedItemId(R.id.navigation_home);
//
//                }


            }
        });



           packsTvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("from", "allpack");
                CategoryFragment categoryFragment = new CategoryFragment();
                categoryFragment.setArguments(bundle);
                MainActivity.fm.beginTransaction().add(
                        R.id.container, categoryFragment
                ).addToBackStack(null).commit();



            }
               
        });


        lytSearchview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.fm.beginTransaction().add(R.id.container, new SearchFragment()).addToBackStack(null).commit();
            }
        });

        mPager = root.findViewById(R.id.pager);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int position) {
                ApiConfig.addMarkers(position, sliderArrayList, mMarkersLayout, getContext());
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (swipeTimer != null) {
                    swipeTimer.cancel();
                }
                if (AppController.isConnected(getActivity())) {

                    ApiConfig.GetSettings(getActivity());
                    GetSlider();
                    GetCategory();
                    GetPacks();
                    SectionProductRequest();
                    GetOfferImage();
                }
                swipeLayout.setRefreshing(false);
            }
        });


        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                if (!token.equals(session.getData(Session.KEY_FCM_ID))) {
                    UpdateToken(token, getActivity());
                }
            }
        });

        if (AppController.isConnected(getActivity())) {
            ApiConfig.GetSettings(getActivity());
            GetSlider();
            GetCategory();
            GetPacks();
            SectionProductRequest();
            GetOfferImage();


            if (session.isUserLoggedIn()){

                ApiConfig.getWalletBalance(getActivity(), session, new GetWalletPrice() {
                    @Override
                    public void onSuccesPrice(Double result) {
                        Constant.WALLET_BALANCE = result;
                        DrawerActivity.tvWallet.setText(activity.getResources().getString(R.string.wallet_balance) +
                                "\t:\t" + Constant.SETTING_CURRENCY_SYMBOL +Constant.formater.format(Constant.WALLET_BALANCE));


                    }
                });

            }else {
                Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
                TextView daysLeftText = toolbar.findViewById(R.id.tbDaysLeftText);
                daysLeftText.setVisibility(View.GONE);

            }
        }


        ProductDetailFragment.backPressedOnPackl = new BackPressedOnPack() {
            @Override
            public void onBack(boolean yes) {


            }
        };



        return root;
    }


    @Override
    public void onStart() {
        super.onStart();

    }



    public void GetOfferImage() {
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constant.GET_OFFER_IMAGE, Constant.GetVal);
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        ArrayList<String> offerList = new ArrayList<>();
                        JSONObject objectbject = new JSONObject(response);
                        if (!objectbject.getBoolean(Constant.ERROR)) {
                            JSONArray jsonArray = objectbject.getJSONArray(Constant.DATA);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                offerList.add(object.getString(Constant.IMAGE));
                            }
                            offerView.setAdapter(new OfferAdapter(offerList, R.layout.offer_lyt));

                            progressBar.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        }, getActivity(), Constant.OFFER_URL, params, false);
    }

    private void GetCategory() {
        progressBar.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<String, String>();
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        JSONObject object = new JSONObject(response);
                        categoryArrayList = new ArrayList<>();
                        categoryArrayList.clear();
                        if (!object.getBoolean(Constant.ERROR)) {
                            JSONArray jsonArray = object.getJSONArray(Constant.DATA);
                            Gson gson = new Gson();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                Category category = gson.fromJson(jsonObject.toString(), Category.class);
                                categoryArrayList.add(category);
                            }
                            categoryRecyclerView.setAdapter(new CategoryAdapter(getContext(), getActivity(), categoryArrayList, R.layout.lyt_category, "home"));

                            progressBar.setVisibility(View.GONE);
                        } else {
                            lytCategory.setVisibility(View.GONE);
                        }
                        progressBar.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, getActivity(), Constant.CategoryUrl, params, false);
    }


    private void GetPacks() {
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id", session.getData(Constant.ID));

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        JSONObject object = new JSONObject(response);
                        packArrayList = new ArrayList<>();
                        packArrayList.clear();
                        if (!object.getBoolean(Constant.ERROR)) {

                            packArrayList = Utils.decryptData(object,
                                    Pack.class);
                            packRecyclerView.setAdapter(
                                    new PackCategoryAdapter(packArrayList,getContext()));


                        } else {
                            lytCategory.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, getActivity(), Constant.GET_PACKS, params, true);
    }




    public void SectionProductRequest() {  //json request for product search
//    progressBar.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<>();
//        params.put(Constant.GET_ALL_SECTIONS, Constant.GetVal);
//        params.put(Constant.USER_ID, session.getData(Constant.ID));
        ApiConfig.RequestToVolley(new VolleyCallback() {

            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {

                        JSONObject object1 = new JSONObject(response);
                        if (object1.getBoolean(Constant.ERROR)) {

                            sectionList=new ArrayList<>();
                             JSONArray jsonArray = object1.getJSONArray(Constant.DATA);
                    //       progressBar.setVisibility(View.GONE);



                            for (int j = 0; j < jsonArray.length(); j++) {
                                Category section = new Category();
                                JSONObject jsonObject = jsonArray.getJSONObject(j);

                                section.setName(jsonObject.getString(Constant.TITLE));
                                section.setStyle(jsonObject.getString(Constant.SECTION_STYLE));
                                section.setSubtitle(jsonObject.getString(Constant.SHORT_DESC));
                                JSONArray productArray = jsonObject.getJSONArray("product");
                                ArrayList<Product> productArrayList = new ArrayList<>();
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    productArrayList.addAll(ApiConfig.GetSectionProductList(productArray).stream().filter(
                                            product -> {
                                                if (!product.getType().equals("fixed")){
                                                    return true;
                                                }
                                                return false;
                                            }
                                    ).collect(Collectors.toList()));


                                }else {

                                    for (Product product:ApiConfig.GetSectionProductList(productArray)){
                                        if (!product.getType().equals("fixed")){
                                          productArrayList.add(product);
                                        }
                                    }
                                }

                                section.setProductList(productArrayList);
                                sectionList.add(section);

                            }


                            sectionView.setVisibility(View.VISIBLE);
                            SectionAdapter sectionAdapter = new SectionAdapter(getContext(),
                                    getActivity(), sectionList);
                            sectionView.setAdapter(sectionAdapter);
                            progressBar.setVisibility(View.GONE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        }, getActivity(), Constant.FeaturedProductUrl, params, true);
    }

    private void GetSlider() {

        Map<String, String> params = new HashMap<>();
        params.put(Constant.GET_SLIDER_IMAGE, Constant.GetVal);
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {

                    sliderArrayList = new ArrayList<>();
                    try {
                        JSONObject object = new JSONObject(response);
                        if (!object.getBoolean(Constant.ERROR)) {
                            JSONArray jsonArray = object.getJSONArray(Constant.DATA);
                            size = jsonArray.length();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                sliderArrayList.add(new Slider(jsonObject.getString(Constant.TYPE), jsonObject.getString(Constant.TYPE_ID), jsonObject.getString(Constant.NAME), jsonObject.getString(Constant.IMAGE)));
                            }
                            mPager.setAdapter(new SliderAdapter(sliderArrayList, getActivity(), R.layout.lyt_slider, "home"));
                            ApiConfig.addMarkers(0, sliderArrayList, mMarkersLayout, getContext());
                            handler = new Handler();
                            Update = new Runnable() {
                                public void run() {
                                    if (currentPage == size) {
                                        currentPage = 0;
                                    }
                                    try {
                                        mPager.setCurrentItem(currentPage++, true);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            };
                            swipeTimer = new Timer();
                            swipeTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    handler.post(Update);
                                }
                            }, timerDelay, timerWaiting);
                        }
//                        System.out.println("TIMING : : : " + timerDelay + " , " + timerWaiting);

                        progressBar.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();

                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        }, getActivity(), Constant.SliderUrl, params, true);

    }

    @Override
    public void onResume() {
        super.onResume();
        Constant.TOOLBAR_TITLE = getString(R.string.app_name);
        getActivity().invalidateOptionsMenu();

        Paper.init(activity);
        Paper.book().delete(Constant.CUSTOM_TRANSACTION_TYPE);

        hideKeyboard();
    }













    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(root.getApplicationWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.toolbar_cart).setVisible(true);
        menu.findItem(R.id.toolbar_sort).setVisible(false);
        menu.findItem(R.id.toolbar_search).setVisible(false);
    }





}
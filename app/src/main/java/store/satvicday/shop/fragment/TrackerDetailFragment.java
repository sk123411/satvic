package store.satvicday.shop.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.BackPressedOnPack;
import store.satvicday.shop.activity.BackPressedOnTrackerDetail;
import store.satvicday.shop.activity.DisbaleCancelButtonInterface;
import store.satvicday.shop.activity.DrawerActivity;
import store.satvicday.shop.activity.GetWalletPrice;
import store.satvicday.shop.activity.MainActivity;
import store.satvicday.shop.adapter.ItemsAdapter;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.OrderTracker;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class TrackerDetailFragment extends Fragment {
    public static ProgressBar pBar;
    public static Button btnCancel, btnReturn;
    public static LinearLayout lyttracker;
    View root;
    OrderTracker order;
    TextView txtorderotp, tvItemTotal, tvTaxPercent, tvTaxAmt, tvDeliveryCharge, tvTotal, tvPromoCode, tvPCAmount, tvWallet, tvFinalTotal, tvDPercent, tvDAmount;
    TextView txtcanceldetail, txtotherdetails, txtorderid, txtorderdate;
    NetworkImageView imgorder;
    RecyclerView recyclerView;
    View l4;
    RelativeLayout relativeLyt;
    LinearLayout returnLyt, lytPromo, lytWallet, lytPriceDetail;
    double totalAfterTax = 0.0;
    Activity activity;
    String id;
    Session session;
    private ItemsAdapter itemAdapter;
    public static BackPressedOnTrackerDetail backPressedOnPack;
    private ProgressDialog packProgressBar;

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_tracker_detail, container, false);
        activity = getActivity();
        session = new Session(activity);

        pBar = root.findViewById(R.id.pBar);
        lytPriceDetail = root.findViewById(R.id.lytPriceDetail);
        lytPromo = root.findViewById(R.id.lytPromo);
        lytWallet = root.findViewById(R.id.lytWallet);
        tvItemTotal = root.findViewById(R.id.tvItemTotal);
        tvTaxPercent = root.findViewById(R.id.tvTaxPercent);
        tvTaxAmt = root.findViewById(R.id.tvTaxAmt);
        tvDeliveryCharge = root.findViewById(R.id.tvDeliveryCharge);
        tvDAmount = root.findViewById(R.id.tvDAmount);
        tvDPercent = root.findViewById(R.id.tvDPercent);
        tvTotal = root.findViewById(R.id.tvTotal);
        tvPromoCode = root.findViewById(R.id.tvPromoCode);
        tvPCAmount = root.findViewById(R.id.tvPCAmount);
        tvWallet = root.findViewById(R.id.tvWallet);
        tvFinalTotal = root.findViewById(R.id.tvFinalTotal);
        txtorderid = root.findViewById(R.id.txtorderid);
        txtorderdate = root.findViewById(R.id.txtorderdate);
        relativeLyt = root.findViewById(R.id.relativeLyt);
        imgorder = root.findViewById(R.id.imgorder);
        txtotherdetails = root.findViewById(R.id.txtotherdetails);
        txtcanceldetail = root.findViewById(R.id.txtcanceldetail);
        lyttracker = root.findViewById(R.id.lyttracker);
        recyclerView = root.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setNestedScrollingEnabled(false);
        btnCancel = root.findViewById(R.id.btncancel);
        l4 = root.findViewById(R.id.l4);
        returnLyt = root.findViewById(R.id.returnLyt);
        txtorderotp = root.findViewById(R.id.txtorderotp);
        packProgressBar = new ProgressDialog(requireContext());

        root.setFocusableInTouchMode(true);
        root.requestFocus();
        root.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {

                    backPressedOnPack.onBack(true);
                }


                return false;
            }
        });


        id = getArguments().getString("id");

        if (id.equals("")) {
            order = (OrderTracker) getArguments().getSerializable("model");


            SetData(order, false);
        } else {
            getOrderDetails(id);

        }




        return root;
    }

    public void getOrderDetails(String id) {
        Map<String, String> params = new HashMap<>();
        params.put(Constant.GET_ORDERS, Constant.GetVal);
        params.put(Constant.USER_ID, session.getData(Constant.ID));
        params.put(Constant.ORDER_ID, id);


        //  System.out.println("=====params " + params.toString());
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {

                try {
                    JSONObject jsonObject1 = new JSONObject(response);
                    if (!jsonObject1.getBoolean(Constant.ERROR)) {
                        JSONObject jsonObject = jsonObject1.getJSONArray(Constant.DATA).getJSONObject(0);

                        String laststatusname = null, laststatusdate = null;
                        JSONArray statusarray = jsonObject.getJSONArray("status");
                        ArrayList<OrderTracker> statusarraylist = new ArrayList<>();
                        String returnRequest = jsonObject.getString("return_request");
                        if (returnRequest.equals("1")) {
                            btnCancel.setText(getResources().getString(R.string.return_request));
                        }

                        int cancel = 0, delivered = 0, process = 0, shipped = 0, returned = 0;
                        for (int k = 0; k < statusarray.length(); k++) {
                            JSONArray sarray = statusarray.getJSONArray(k);
                            String sname = sarray.getString(0);
                            String sdate = sarray.getString(1);


                            statusarraylist.add(new OrderTracker(sname, sdate));
                            laststatusname = sname;
                            laststatusdate = sdate;


                            if (sname.equalsIgnoreCase("cancelled")) {
                                cancel = 1;
                                btnCancel.setText("Cancelled");


                                delivered = 0;
                                process = 0;
                                shipped = 0;
                                returned = 0;
                            } else if (sname.equalsIgnoreCase("delivered")) {
                                btnCancel.setVisibility(View.VISIBLE);
                                delivered = 1;
                                process = 0;
                                shipped = 0;
                                returned = 0;
                            } else if (sname.equalsIgnoreCase("processed")) {
                                process = 1;
                                shipped = 0;
                                returned = 0;
                            } else if (sname.equalsIgnoreCase("shipped")) {
                                shipped = 1;
                                returned = 0;
                            } else if (sname.equalsIgnoreCase("returned")) {

                                returned = 1;
                            }


                        }

                        ArrayList<OrderTracker> itemList = new ArrayList<>();
                        JSONArray itemsarray = jsonObject.getJSONArray("items");

                        for (int j = 0; j < itemsarray.length(); j++) {

                            JSONObject itemobj = itemsarray.getJSONObject(j);
                            double productPrice = 0.0;
                            if (itemobj.getString(Constant.DISCOUNTED_PRICE).equals("0"))
                                productPrice = (Double.parseDouble(itemobj.getString(Constant.PRICE)) * Integer.parseInt(itemobj.getString(Constant.QUANTITY)));
                            else {
                                productPrice = (Double.parseDouble(itemobj.getString(Constant.DISCOUNTED_PRICE)) * Integer.parseInt(itemobj.getString(Constant.QUANTITY)));
                            }
                            JSONArray statusarray1 = itemobj.getJSONArray("status");
                            ArrayList<OrderTracker> statusList = new ArrayList<>();

                            for (int k = 0; k < statusarray1.length(); k++) {
                                JSONArray sarray = statusarray1.getJSONArray(k);
                                String sname = sarray.getString(0);
                                String sdate = sarray.getString(1);
                                statusList.add(new OrderTracker(sname, sdate));
                            }

                            String deliverByName;
                            String deliverBy = itemobj.getString(Constant.DELIVER_BY);
                            if (deliverBy.equals("null")) {

                                deliverByName = "Not assigned";

                            } else {

                                deliverByName = deliverBy;
                            }

                            OrderTracker orderTracker = new OrderTracker(itemobj.getString(Constant.ID),
                                    itemobj.getString(Constant.ORDER_ID),
                                    itemobj.getString(Constant.PRODUCT_VARIANT_ID),
                                    itemobj.getString(Constant.QUANTITY),
                                    String.valueOf(productPrice),
                                    itemobj.getString(Constant.DISCOUNT),
                                    itemobj.getString(Constant.SUB_TOTAL),
                                    deliverByName,
                                    itemobj.getString(Constant.NAME),
                                    itemobj.getString(Constant.IMAGE),
                                    itemobj.getString(Constant.MEASUREMENT),
                                    itemobj.getString(Constant.UNIT),
                                    jsonObject.getString(Constant.PAYMENT_METHOD),
                                    itemobj.getString(Constant.ACTIVE_STATUS),
                                    itemobj.getString(Constant.DATE_ADDED),
                                    statusList,
                                    itemobj.getString(Constant.RETURN_STATUS),
                                    itemobj.getString(Constant.CANCELLABLE_STATUS),
                                    itemobj.getString(Constant.TILL_STATUS),
                                    jsonObject.getString(Constant.SUBSCRIBE),
                                    jsonObject.getString(Constant.SUBSCRIBE_DATE),
                                    jsonObject.getString(Constant.PACK_DATE),
                                    itemobj.getString(Constant.RETURN_REQUEST),
                                    itemobj.getString(Constant.RETURN_METHOD),
                                    jsonObject.getString(Constant.FEEDBACK),
                                    jsonObject.getString(Constant.PACK_ID)
                            );
                            itemList.add(orderTracker);


                        }


                        OrderTracker orderTracker = new OrderTracker(
                                jsonObject.getString(Constant.OTP),
                                jsonObject.getString(Constant.USER_ID),
                                jsonObject.getString(Constant.ID),
                                jsonObject.getString(Constant.DATE_ADDED),
                                laststatusname, laststatusdate,
                                statusarraylist
                                ,
                                jsonObject.getString(Constant.MOBILE),
                                jsonObject.getString(Constant.DELIVERY_CHARGE),
                                jsonObject.getString(Constant.PAYMENT_METHOD),
                                jsonObject.getString(Constant.ADDRESS),
                                jsonObject.getString(Constant.TOTAL),
                                jsonObject.getString(Constant.FINAL_TOTAL),
                                jsonObject.getString(Constant.TAX_AMOUNT),
                                jsonObject.getString(Constant.TAX_PERCENT),
                                jsonObject.getString(Constant.KEY_WALLET_BALANCE),
                                jsonObject.getString(Constant.PROMO_CODE),
                                jsonObject.getString(Constant.PROMO_DISCOUNT),
                                jsonObject.getString(Constant.DISCOUNT),
                                jsonObject.getString(Constant.DISCOUNT_AMT),
                                jsonObject.getString(Constant.USER_NAME), itemList,
                                jsonObject.getString(Constant.SUBSCRIBE),
                                jsonObject.getString(Constant.SUBSCRIBE_DATE),
                                jsonObject.getString(Constant.PACK_DATE),
                                jsonObject.getString(Constant.RETURN_REQUEST),
                                jsonObject.getString(Constant.RETURN_METHOD),
                                jsonObject.getString(Constant.FEEDBACK),
                                jsonObject.getString(Constant.PACK_ID),
                                jsonObject.getString(Constant.ACTIVE_STATUS)

                        );


                        SetData(orderTracker, false);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, activity, Constant.ORDERPROCESS_URL, params, true);
    }


    private void addReturnRequest(String user_id, String order_id) {
        HashMap<String, String> params = new HashMap<>();
        params.put(Constant.USER_ID, user_id);
        params.put(Constant.ORDER_ID, order_id);
        params.put("ordertypes", "1");


        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                try {
                    JSONObject resultData = new JSONObject(message);
                    Toast.makeText(activity, getResources().getString(
                            R.string.return_request), Toast.LENGTH_LONG).show();
                    btnCancel.setText("Order cancellation request sent");

                    if (!resultData.getBoolean(Constant.ERROR)) {

                        btnCancel.setText("Order cancellation request sent");
                        Toast.makeText(activity, getResources().getString(
                                R.string.return_request), Toast.LENGTH_LONG).show();

                        SetData(order, true);


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, activity, "https://store.satvicday.com/api-firebase/add_return_requests.php", params, true);

    }




    private void addReturnRequestSingle(String user_id,String order_id,String order_item_id,String returnMethod
    ){
        HashMap<String,String> params = new HashMap<>();
        params.put(Constant.USER_ID, user_id);
        params.put(Constant.ORDER_ID, order_id);
        params.put(Constant.ORDER_ITEM_ID, order_item_id);

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                try {
                    JSONObject resultData= new JSONObject(message);
                    Toast.makeText(activity, activity.getResources().getString(
                            R.string.return_request),Toast.LENGTH_LONG).show();

                    if (!resultData.getBoolean(Constant.ERROR)){

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        },activity, Constant.SINGLE_RETURN_REQUESTS, params, true );







    }


    @SuppressLint("SetTextI18n")
    public void SetData(OrderTracker order, Boolean disbleButton) {


        String[] date = order.getDate_added().split("\\s+");
        txtorderid.setText(order.getOrder_id());
        txtorderotp.setText(order.getOtp());
        txtorderdate.setText(date[0]);

        txtotherdetails.setText(getString(R.string.name_1) + order.getUsername() + getString(R.string.mobile_no_1) + order.getMobile() + getString(R.string.address_1) + order.getAddress());
        totalAfterTax = (Double.parseDouble(order.getTotal()) + Double.parseDouble(order.getDelivery_charge()) + Double.parseDouble(order.getTax_amt()));

        tvItemTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + order.getTotal());

        tvDeliveryCharge.setText("+ " + Constant.SETTING_CURRENCY_SYMBOL + order.getDelivery_charge());

         if (order.getSubscribe().equalsIgnoreCase("1")){



            txtorderotp.setVisibility(View.GONE);
            double taxPercent = Double.parseDouble(order.getTax_amt()) * Double.parseDouble(order.getItemsList().get(0).getQuantity());
             totalAfterTax = (Double.parseDouble(order.getTotal()) + Double.parseDouble(order.getDelivery_charge()) +
                     taxPercent);

             tvTaxPercent.setText(getString(R.string.tax) + "(" + order.getTax_percent()+"%) :");
            tvTaxAmt.setText("+ " + Constant.SETTING_CURRENCY_SYMBOL + taxPercent);
            tvDPercent.setText(getString(R.string.discount) + "(" + order.getdPercent() + "%) :");
            tvDAmount.setText("- " + Constant.SETTING_CURRENCY_SYMBOL + order.getdAmount());
            tvTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + totalAfterTax);
            tvPCAmount.setText("- " + Constant.SETTING_CURRENCY_SYMBOL + order.getPromoDiscount());
            tvWallet.setText("- " + Constant.SETTING_CURRENCY_SYMBOL + order.getWalletBalance());
             tvFinalTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + totalAfterTax);



        }else if (!order.getPack_id().equalsIgnoreCase("0")){
            tvTaxPercent.setText(getString(R.string.tax) + "(" + "0" + "%) :");
             tvTaxAmt.setText("+ " + Constant.SETTING_CURRENCY_SYMBOL + "0");
            tvDPercent.setText(getString(R.string.discount) + "(" + "0" + "%) :");
            tvDAmount.setText("- " + Constant.SETTING_CURRENCY_SYMBOL + order.getdAmount());
             tvTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + order.getTotal());
            tvPCAmount.setText("- " + Constant.SETTING_CURRENCY_SYMBOL + order.getPromoDiscount());
            tvWallet.setText("- " + Constant.SETTING_CURRENCY_SYMBOL + order.getWalletBalance());
            tvFinalTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + order.getTotal());
        } else  {
            tvTaxPercent.setText(getString(R.string.tax) + "(" + order.getTax_percent() +"%) :");
            tvTaxAmt.setText("+ " + Constant.SETTING_CURRENCY_SYMBOL + order.getTax_amt());
            tvDPercent.setText(getString(R.string.discount) + "(" + order.getdPercent() + "%) :");
            tvDAmount.setText("- " + Constant.SETTING_CURRENCY_SYMBOL + order.getdAmount());
            tvTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + totalAfterTax);
            tvPCAmount.setText("- " + Constant.SETTING_CURRENCY_SYMBOL + order.getPromoDiscount());
            tvWallet.setText("- " + Constant.SETTING_CURRENCY_SYMBOL + order.getWalletBalance());
            tvFinalTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + totalAfterTax);

        }


        Paper.init(activity);
        HashMap<String, Boolean> map = new HashMap<>();
        if (order.getReturn_request().equalsIgnoreCase("1")){
            btnCancel.setText(activity.getResources().getString(R.string.return_request));



            map.put(order.getOrder_id(), true);


        }else {
            map.put(order.getOrder_id(), false);

        }
        Paper.book().write(Constant.RETURNED_ALL, map);




//
//        if (order.getStatus().equalsIgnoreCase("returned")) {
//
//
//            btnCancel.setText(getResources().getString(R.string.return_approved));
//            btnCancel.setEnabled(false);
//        }


        if (!order.getStatus().equalsIgnoreCase("delivered") &&
                !order.getStatus().equalsIgnoreCase("cancelled") && !order.getStatus().equalsIgnoreCase("returned")) {
            btnCancel.setVisibility(View.VISIBLE);
        } else if (order.getStatus().equalsIgnoreCase("returned")){





            btnCancel.setVisibility(View.VISIBLE);
            btnCancel.setEnabled(false);

            btnCancel.setText(activity.getResources().getString(R.string.return_request_approved));

        }else {
            btnCancel.setVisibility(View.GONE);
        }


        if (order.getStatus().equalsIgnoreCase("cancelled")) {
            lyttracker.setVisibility(View.GONE);
            btnCancel.setVisibility(View.GONE);
            txtcanceldetail.setVisibility(View.VISIBLE);
            txtcanceldetail.setText(getString(R.string.canceled_on) + order.getStatusdate());
            lytPriceDetail.setVisibility(View.GONE);
        } else {
            lytPriceDetail.setVisibility(View.VISIBLE);

            if (order.getReturn_request().equalsIgnoreCase("1")) {

                if (order.getActiveStatus().equalsIgnoreCase("returned")) {

                    btnCancel.setText(getResources().getString(R.string.return_approved));
                    btnCancel.setEnabled(false);
                    l4.setVisibility(View.VISIBLE);
                    returnLyt.setVisibility(View.VISIBLE);
                }else {

                    btnCancel.setText(getResources().getString(R.string.return_request));
                    btnCancel.setEnabled(false);

                }
            }
            lyttracker.setVisibility(View.VISIBLE);
            for (int i = 0; i < order.getOrderStatusArrayList().size(); i++) {
                int img = getResources().getIdentifier("img" + i, "id", activity.getPackageName());
                int view = getResources().getIdentifier("l" + i, "id", activity.getPackageName());
                int txt = getResources().getIdentifier("txt" + i, "id", activity.getPackageName());
                int textview = getResources().getIdentifier("txt" + i + "" + i, "id", activity.getPackageName());


                if (img != 0 && root.findViewById(img) != null) {
                    ImageView imageView = root.findViewById(img);
                    imageView.setColorFilter(getResources().getColor(R.color.colorPrimary));
                }

                if (view != 0 && root.findViewById(view) != null) {
                    View view1 = root.findViewById(view);
                    view1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                }

                if (txt != 0 && root.findViewById(txt) != null) {
                    TextView view1 = root.findViewById(txt);
                    view1.setTextColor(getResources().getColor(R.color.black));
                }

                if (textview != 0 && root.findViewById(textview) != null) {
                    TextView view1 = root.findViewById(textview);
                    String str = order.getDate_added();
                    String[] splited = str.split("\\s+");
                    view1.setText(splited[0] + "\n" + splited[1]);
                }
            }


        }








        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.cancel_order))
                        .setMessage(getString(R.string.cancel_msg))
                        .setNegativeButton(getString(R.string.bank_card), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                btnCancel.setEnabled(false);


                                if (order.getPack_id().equalsIgnoreCase("0")) {

                                    Map<String, String> params = new HashMap<String, String>();


                                    params.put(Constant.UPDATE_ORDER_STATUS, Constant.GetVal);
                                    params.put(Constant.ID, order.getOrder_id());
                                    params.put(Constant.RETURN_METHOD, getString(R.string.bank_card));
                                    params.put(Constant.RETURN_REQUEST, Constant.GetVal);


                                    pBar.setVisibility(View.VISIBLE);
                                    ApiConfig.RequestToVolley(new VolleyCallback() {
                                        @Override
                                        public void onSuccess(boolean result, String response) {
                                            // System.out.println("=================*cancelorder- " + response);
                                            try {
                                                JSONObject object = new JSONObject(response);
                                                if (!object.getBoolean(Constant.ERROR)) {
                                                    Constant.isOrderCancelled = true;
                                                    btnCancel.setEnabled(false);
                                                    boolean pack = false;

                                                        addReturnRequest(order.getUser_id(), order.getOrder_id());

                                                }
                                                Toast.makeText(getContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                                pBar.setVisibility(View.GONE);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    }, activity, Constant.ORDERPROCESS_URL, params, false);
                                }else {

                                    packProgressBar.show();
                                    packProgressBar.setCancelable(false);
                                    btnCancel.setEnabled(false);



                                    for (OrderTracker orde:order.getItemsList()) {

                                        Map<String, String> params = new HashMap<String, String>();


                                        params.put(Constant.SINGLE_UPDATE_ORDER_STATUS, "1");
                                        params.put(Constant.USER_ID, session.getData(Constant.ID));
                                        params.put(Constant.ID, orde.getOrder_id());
                                        params.put(Constant.ORDER_ITEM_ID, orde.getId());
                                        params.put(Constant.RETURN_REQUEST, "1");
                                        params.put(Constant.RETURN_METHOD, getString(R.string.bank_card));


                                        pBar.setVisibility(View.VISIBLE);
                                        ApiConfig.RequestToVolley(new VolleyCallback() {
                                            @Override
                                            public void onSuccess(boolean result, String response) {
                                                // System.out.println("=================*cancelorder- " + response);
                                                try {
                                                    JSONObject object = new JSONObject(response);
                                                    if (!object.getBoolean(Constant.ERROR)) {
                                                        Constant.isOrderCancelled = true;

                                                        addReturnRequestSingle(session.getData(Constant.ID), order.getOrder_id(),order.getId(),params.get(Constant.RETURN_METHOD));

                                                    }
                                                    pBar.setVisibility(View.GONE);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        }, activity, Constant.ORDERPROCESS_URL, params, false);

                                    }



                                    packProgressBar.dismiss();
                                    Toast.makeText(getContext(), "Order Cancellation request sent ", Toast.LENGTH_LONG).show();
                                    btnCancel.setText(activity.getResources().getString(R.string.return_request));






                                }
                                dialog.dismiss();


                            }
                        })
                        .setPositiveButton(getString(R.string.wallet),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        btnCancel.setEnabled(false);


                                        if (order.getPack_id().equalsIgnoreCase("0")) {

                                            Map<String, String> params = new HashMap<String, String>();


                                            params.put(Constant.UPDATE_ORDER_STATUS, Constant.GetVal);
                                            params.put(Constant.ID, order.getOrder_id());
                                            params.put(Constant.RETURN_METHOD, getString(R.string.wallet));
                                            params.put(Constant.RETURN_REQUEST, Constant.GetVal);


                                            pBar.setVisibility(View.VISIBLE);
                                            ApiConfig.RequestToVolley(new VolleyCallback() {
                                                @Override
                                                public void onSuccess(boolean result, String response) {
                                                    // System.out.println("=================*cancelorder- " + response);
                                                    try {
                                                        JSONObject object = new JSONObject(response);
                                                        if (!object.getBoolean(Constant.ERROR)) {
                                                            Constant.isOrderCancelled = true;
                                                            boolean pack = false;

                                                                addReturnRequest(order.getUser_id(), order.getOrder_id());


                                                        }
                                                        Toast.makeText(getContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                                                        pBar.setVisibility(View.GONE);
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }

                                                }
                                            }, activity, Constant.ORDERPROCESS_URL, params, false);
                                        }else {
                                            packProgressBar.show();
                                            packProgressBar.setCancelable(false);
                                            btnCancel.setEnabled(false);

                                            for (OrderTracker orde:order.getItemsList()) {

                                                Map<String, String> params = new HashMap<String, String>();


                                                params.put("singleupdate_order_status", "1");
                                                params.put(Constant.USER_ID, session.getData(Constant.ID));
                                                params.put(Constant.ID, orde.getOrder_id());
                                                params.put(Constant.ORDER_ITEM_ID, orde.getId());
                                                params.put(Constant.RETURN_REQUEST, "1");
                                                params.put(Constant.RETURN_METHOD, getString(R.string.wallet));


                                                pBar.setVisibility(View.VISIBLE);
                                                ApiConfig.RequestToVolley(new VolleyCallback() {
                                                    @Override
                                                    public void onSuccess(boolean result, String response) {
                                                        // System.out.println("=================*cancelorder- " + response);
                                                        try {
                                                            JSONObject object = new JSONObject(response);
                                                            if (!object.getBoolean(Constant.ERROR)) {
                                                                Constant.isOrderCancelled = true;

                                                                addReturnRequestSingle(session.getData(Constant.ID), orde.getOrder_id(),orde.getId(),params.get(Constant.RETURN_METHOD));

                                                            }
                                                            pBar.setVisibility(View.GONE);
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                    }
                                                }, activity, Constant.ORDERPROCESS_URL, params, false);

                                            }
                                            btnCancel.setText(activity.getResources().getString(R.string.return_request));
                                            packProgressBar.dismiss();
                                            Toast.makeText(getContext(), "Order Cancellation request sent ", Toast.LENGTH_LONG).show();

                                        }

                                        dialog.dismiss();
                                    }
                                }).show();
            }
        });


        itemAdapter = new ItemsAdapter(activity, order.itemsList, "detail", disbleButton, TrackerDetailFragment.this);
        recyclerView.setAdapter(itemAdapter);
        relativeLyt.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResume() {
        super.onResume();
        Constant.TOOLBAR_TITLE = getString(R.string.order_track_detail);
        activity.invalidateOptionsMenu();
        hideKeyboard();
    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(root.getApplicationWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.toolbar_cart).setVisible(true);
        menu.findItem(R.id.toolbar_sort).setVisible(false);
        menu.findItem(R.id.toolbar_search).setVisible(true);
    }


}
package store.satvicday.shop.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.flutterwave.raveandroid.RavePayActivity;
import com.flutterwave.raveandroid.RaveUiManager;
import com.flutterwave.raveandroid.rave_java_commons.RaveConstants;
import com.google.gson.Gson;
import com.razorpay.Checkout;
import com.shreyaspatil.easyupipayment.EasyUpiPayment;
import com.shreyaspatil.easyupipayment.exception.AppNotFoundException;
import com.shreyaspatil.easyupipayment.listener.PaymentStatusListener;
import com.shreyaspatil.easyupipayment.model.TransactionDetails;
import com.wangsun.upi.payment.UpiPayment;
import com.wangsun.upi.payment.model.PaymentDetail;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.GetWalletPrice;
import store.satvicday.shop.activity.MainActivity;
import store.satvicday.shop.activity.PayPalWebActivity;
import store.satvicday.shop.activity.PayStackActivity;
import store.satvicday.shop.activity.checkout.CheckoutActivity;
import store.satvicday.shop.activity.subscribe.Subscribe;
import store.satvicday.shop.adapter.DateAdapter;
import store.satvicday.shop.adapter.SlotAdapter;
import store.satvicday.shop.fragment.confirmorder.OrderConfirmFragment;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.AppController;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.PaymentModelClass;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.helper.membership.Plan;
import store.satvicday.shop.helper.membership.PlanSuccess;
import store.satvicday.shop.model.BookingDate;
import store.satvicday.shop.model.Slot;
import store.satvicday.shop.model.packs.Pack;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static store.satvicday.shop.helper.ApiConfig.GetTimeSlotConfig;

public class PaymentFragment extends Fragment implements PaymentStatusListener {

    public static String upiPayId;
    private boolean isSlotEnabled = false;


    public static enum TypePurchase {

        SUBSCRIBE,
        MEMBERSHIP,
        PACK,
        REGULAR
    }

    public static String razorPayId, paymentMethod = "", deliveryTime = "", deliveryDay = "", pCode = "", TAG = CheckoutFragment.class.getSimpleName();
    public static RecyclerView recyclerView;
    public static SlotAdapter adapter;
    public TextView tvSelectDeliveryDate, tvPreTotal, tvWltBalance;
    public static Map<String, String> sendparams;
    public TextView tvProceedOrder, tvConfirmOrder, tvPayment, tvDelivery, tvCodText;
    public ArrayList<String> variantIdList, qtyList, dateList;
    public LinearLayout deliveryLyt, paymentLyt;
    RelativeLayout confirmLyt;
    View root;
    double total;

    RelativeLayout lytWallet;
    public LinearLayout deliveryTimeLyt, lytPayOption, lytTax, lytOrderList, lytCLocation, processLyt,
            lytFlutterWave, CODLinearLyt, lytPayU, lytPayPal, lytRazorPay, lytPayStack, lytUpi;
    PaymentModelClass paymentModelClass;
    CheckBox chWallet;
    ImageView imgRefresh;
    ProgressBar pBar;
    Calendar StartDate, EndDate;
    ArrayList<Slot> slotList;
    int mYear, mMonth, mDay;
    Button btnApply;
    Session session;
    String address = null;
    ScrollView scrollPaymentLyt;
    Activity activity;
    DateAdapter dateAdapter;
    ArrayList<BookingDate> bookingDates;
    RecyclerView recyclerViewDates;
    private TextView tvSubTotal;
    RadioButton rbCod, rbPayU, rbPayPal, rbRazorPay, rbPayStack, rbFlutterWave, rbUpiButton;
    private double subtotal = 0.0, usedBalance = 0.0, totalAfterTax = 0.0, taxAmt = 0.0, pCodeDiscount = 0.0, dCharge = 0.0;
    private boolean membershipTransaction = false;
    public static Plan plan;
    private EasyUpiPayment easyUpiPayment;
    TypePurchase transacationType;
    private ProgressBar progressBar;
    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_payment, container, false);
        activity = getActivity();
        paymentModelClass = new PaymentModelClass(activity);
        Constant.selectedDatePosition = 0;
        session = new Session(getActivity());
        getAllWidgets(root);
        setHasOptionsMenu(true);
        Paper.init(getActivity());


        transacationType = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE);

        //Hide days left text
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        TextView daysLeftText = toolbar.findViewById(R.id.tbDaysLeftText);
        daysLeftText.setVisibility(View.GONE);

        if (transacationType == TypePurchase.MEMBERSHIP || transacationType == TypePurchase.SUBSCRIBE
                || transacationType == TypePurchase.PACK) {
            membershipTransaction = true;
            lytWallet.setVisibility(View.GONE);
            CODLinearLyt.setVisibility(View.GONE);

            if (transacationType == TypePurchase.MEMBERSHIP) {
                plan = Paper.book().read(Constant.PLAN_DETAILS);


                CardView planLayout = root.findViewById(R.id.planLayout);
                TextView planMonth = root.findViewById(R.id.planMonth);
                TextView planPrice = root.findViewById(R.id.planPrice);
                TextView planOrignalPrice = root.findViewById(R.id.planOriginalPrice);

                planLayout.setVisibility(View.VISIBLE);
                planMonth.setText(
                        getResources().getString(R.string.plan_month) + plan.getMonth());

                planPrice.setText(
                        getResources().getString(R.string.plan_price) + plan.getPrice());

                planOrignalPrice.setText(
                        getResources().getString(R.string.plan_original_price) + plan.getOriginal_price()
                );
                planOrignalPrice.setPaintFlags(planOrignalPrice.getPaintFlags()
                        | Paint.STRIKE_THRU_TEXT_FLAG);
            }


        } else {
            membershipTransaction = false;
            lytWallet.setVisibility(View.VISIBLE);

        }



        if (!membershipTransaction) {

            total = getArguments().getDouble("total");
            subtotal = getArguments().getDouble("subtotal");
            taxAmt = getArguments().getDouble("taxAmt");

            Constant.SETTING_TAX = getArguments().getDouble("tax");




            pCodeDiscount = getArguments().getDouble("pCodeDiscount");
            pCode = getArguments().getString("pCode");
            dCharge = getArguments().getDouble("dCharge");
            address = getArguments().getString("address");
            variantIdList = getArguments().getStringArrayList("variantIdList");
            qtyList = getArguments().getStringArrayList("qtyList");

            tvSubTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(subtotal));

            if (AppController.isConnected(getActivity())) {

                if (AppController.isConnected(activity)) {
                    GetPaymentConfig();

                }

                chWallet.setTag("false");
                tvWltBalance.setText("Total Balance: " + Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(Constant.WALLET_BALANCE));
                if (Constant.WALLET_BALANCE == 0) {
                    lytWallet.setVisibility(View.GONE);
                } else {
                    lytWallet.setVisibility(View.VISIBLE);
                }


                setParams();

                //Disabling cod layout for orders above 3000
                if (total >= 3000) {

                    tvCodText.setPaintFlags(tvCodText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    rbCod.setEnabled(false);

                } else {
                    rbCod.setEnabled(true);

                }


                chWallet.setOnClickListener(view -> {
                    if (chWallet.getTag().equals("false")) {
                        chWallet.setChecked(true);
                        lytWallet.setVisibility(View.VISIBLE);

                        if (Constant.WALLET_BALANCE >= subtotal) {
                            usedBalance = subtotal;
                            tvWltBalance.setText(getString(R.string.remaining_wallet_balance) + Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format((Constant.WALLET_BALANCE - usedBalance)));
                            paymentMethod = "wallet";
                            lytPayOption.setVisibility(View.GONE);
                        } else {
                            usedBalance = Constant.WALLET_BALANCE;
                            tvWltBalance.setText(getString(R.string.remaining_wallet_balance) + Constant.SETTING_CURRENCY_SYMBOL + "0.00");
                            lytPayOption.setVisibility(View.VISIBLE);
                        }
                        subtotal = (subtotal - usedBalance);
                        tvSubTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(subtotal));
                        chWallet.setTag("true");

                    } else {
                        walletUncheck();
                    }

                });
                confirmLyt.setVisibility(View.VISIBLE);
                scrollPaymentLyt.setVisibility(View.VISIBLE);

            }


        } else {



            if (transacationType != TypePurchase.MEMBERSHIP) {

                total = getArguments().getDouble("total");
                subtotal = getArguments().getDouble("subtotal");
                taxAmt = getArguments().getDouble("taxAmt");
                Constant.SETTING_TAX = getArguments().getDouble("tax");



                if (transacationType==TypePurchase.SUBSCRIBE){
                    Paper.init(activity);

                }

                pCodeDiscount = getArguments().getDouble("pCodeDiscount");
                pCode = getArguments().getString("pCode");
                dCharge = getArguments().getDouble("dCharge");
                address = getArguments().getString("address");
                variantIdList = getArguments().getStringArrayList("variantIdList");
                qtyList = getArguments().getStringArrayList("qtyList");
                CODLinearLyt.setVisibility(View.GONE);


                setParams();

                if (AppController.isConnected(getActivity())) {

                    if (AppController.isConnected(activity)) {
                        GetPaymentConfig();

                    }
                }

                confirmLyt.setVisibility(View.VISIBLE);
                scrollPaymentLyt.setVisibility(View.VISIBLE);

                tvSubTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(subtotal));


                tvProceedOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(isSlotEnabled) {

//                            if (deliveryDay.length() == 0) {
//                                Toast.makeText(getContext(), getString(R.string.select_delivery_day), Toast.LENGTH_SHORT).show();
//                                return;
//                            } else

                                if (deliveryTime.length() == 0) {
                                Toast.makeText(getContext(), getString(R.string.select_delivery_time), Toast.LENGTH_SHORT).show();
                                return;
                            } else if (paymentMethod.isEmpty()) {
                                Toast.makeText(getContext(), getString(R.string.select_payment_method), Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }

                        if (paymentMethod.equals("")){
                            Toast.makeText(getContext(), getString(R.string.select_payment_method), Toast.LENGTH_SHORT).show();

                            return;
                        }


                        purchaseCustom(transacationType);
                    }
                });

                toolbar.setTitle("Make Payment");

            } else {
                sendparams = new HashMap<>();

                if (AppController.isConnected(activity)) {
                    GetPaymentConfig();

                }

                confirmLyt.setVisibility(View.VISIBLE);
                scrollPaymentLyt.setVisibility(View.VISIBLE);

                tvSubTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(Double.parseDouble(plan.getPrice())));
                tvProceedOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        purchaseCustom(transacationType);
                    }
                });
            }
        }

        return root;
    }

    private void setParams() {
        sendparams = new HashMap<>();
        sendparams.put(Constant.PLACE_ORDER, Constant.GetVal);
        sendparams.put(Constant.USER_ID, session.getData(Session.KEY_ID));
        sendparams.put(Constant.TAX_AMOUNT, "" + taxAmt);
        sendparams.put(Constant.TOTAL, "" + total);
        sendparams.put(Constant.TAX_PERCENT, "" + Constant.SETTING_TAX);
        sendparams.put(Constant.FINAL_TOTAL, "" + Constant.formater.format(subtotal));
        sendparams.put(Constant.PRODUCT_VARIANT_ID, String.valueOf(variantIdList));
        sendparams.put(Constant.QUANTITY, String.valueOf(qtyList));
        sendparams.put(Constant.MOBILE, session.getData(Session.KEY_MOBILE));
        sendparams.put(Constant.DELIVERY_CHARGE, "" + dCharge);
        sendparams.put(Constant.DELIVERY_TIME, (deliveryDay + " - " + deliveryTime));
        // sendparams.put(Constant.KEY_WALLET_USED, chWallet.getTag().toString());
        sendparams.put(Constant.KEY_WALLET_BALANCE, String.valueOf(usedBalance));
        sendparams.put(Constant.PAYMENT_METHOD, paymentMethod);
        if (!pCode.isEmpty()) {
            sendparams.put(Constant.PROMO_CODE, pCode);
            sendparams.put(Constant.PROMO_DISCOUNT, Constant.formater.format(pCodeDiscount));
        }

        sendparams.put(Constant.ADDRESS, address);
        sendparams.put(Constant.LONGITUDE, session.getData(Session.KEY_LONGITUDE));
        sendparams.put(Constant.LATITUDE, session.getData(Session.KEY_LATITUDE));
        sendparams.put(Constant.EMAIL, session.getData(Session.KEY_EMAIL));
        sendparams.put(Constant.MEMBER, session.getData(Constant.MEMBER));

    }


    public void getAllWidgets(View root) {
        recyclerView = root.findViewById(R.id.recyclerView);
        pBar = root.findViewById(R.id.pBar);


        lytTax = root.findViewById(R.id.lytTax);


        lytPayStack = root.findViewById(R.id.lytPayStack);
        rbPayStack = root.findViewById(R.id.rbPayStack);
        rbFlutterWave = root.findViewById(R.id.rbFlutterWave);
        rbCod = root.findViewById(R.id.rbcod);
        rbPayU = root.findViewById(R.id.rbPayU);
        rbPayPal = root.findViewById(R.id.rbPayPal);
        rbRazorPay = root.findViewById(R.id.rbRazorPay);
        lytPayPal = root.findViewById(R.id.lytPayPal);
        lytRazorPay = root.findViewById(R.id.lytRazorPay);
        lytPayU = root.findViewById(R.id.lytPayU);
        CODLinearLyt = root.findViewById(R.id.CODLinearLyt);
        lytFlutterWave = root.findViewById(R.id.lytFlutterWave);


        tvDelivery = root.findViewById(R.id.tvDelivery);
        tvPayment = root.findViewById(R.id.tvPayment);
        chWallet = root.findViewById(R.id.chWallet);
        lytPayOption = root.findViewById(R.id.lytPayOption);
        lytOrderList = root.findViewById(R.id.lytOrderList);
        lytCLocation = root.findViewById(R.id.lytCLocation);
        lytWallet = root.findViewById(R.id.lytWallet);
        paymentLyt = root.findViewById(R.id.paymentLyt);
        deliveryLyt = root.findViewById(R.id.deliveryLyt);
        tvProceedOrder = root.findViewById(R.id.tvProceedOrder);
        tvConfirmOrder = root.findViewById(R.id.tvConfirmOrder);
        processLyt = root.findViewById(R.id.processLyt);
        tvSelectDeliveryDate = root.findViewById(R.id.tvSelectDeliveryDate);
        deliveryTimeLyt = root.findViewById(R.id.deliveryTimeLyt);
        imgRefresh = root.findViewById(R.id.imgRefresh);
        recyclerViewDates = root.findViewById(R.id.recyclerViewDates);
        tvSubTotal = root.findViewById(R.id.tvSubTotal);
        confirmLyt = root.findViewById(R.id.confirmLyt);
        scrollPaymentLyt = root.findViewById(R.id.scrollPaymentLyt);
        tvWltBalance = root.findViewById(R.id.tvWltBalance);
        tvPreTotal = root.findViewById(R.id.tvPreTotal);
        btnApply = root.findViewById(R.id.btnApply);
        lytUpi = root.findViewById(R.id.lytUPI);
        rbUpiButton = root.findViewById(R.id.rbUPI);
        tvCodText = root.findViewById(R.id.codText);
        progressBar = root.findViewById(R.id.pprogressBar);
    }


    @Override
    public void onPause() {
        super.onPause();
    }


    public void GetPaymentConfig() {
        Map<String, String> params = new HashMap<>();
        params.put(Constant.SETTINGS, Constant.GetVal);
        params.put(Constant.GET_PAYMENT_METHOD, Constant.GetVal);
        //  System.out.println("=====params " + params.toString());
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {

                if (result) {
                    try {
                        JSONObject objectbject = new JSONObject(response);
                        if (!objectbject.getBoolean(Constant.ERROR)) {


                            JSONObject object = objectbject.getJSONObject(Constant.PAYMENT_METHODS);
                            Constant.MERCHANT_KEY = object.getString(Constant.PAY_M_KEY);
                            Constant.MERCHANT_ID = object.getString(Constant.PAYU_M_ID);
                            Constant.MERCHANT_SALT = object.getString(Constant.PAYU_SALT);
                            Constant.RAZOR_PAY_KEY_VALUE = object.getString(Constant.RAZOR_PAY_KEY);
                            Constant.PAYPAL = object.getString(Constant.paypal_method);
                            Constant.PAYUMONEY = object.getString(Constant.payu_method);
                            Constant.RAZORPAY = object.getString(Constant.razor_pay_method);
                            Constant.COD = object.getString(Constant.cod_payment_method);
                            Constant.PAYSTACK = object.getString(Constant.paystack_method);
                            Constant.PAYSTACK_KEY = object.getString(Constant.paystack_public_key);
                            Constant.FLUTTERWAVE = object.getString(Constant.flutterwave_payment_method);
                            Constant.FLUTTERWAVE_ENCRYPTION_KEY_VAL = object.getString(Constant.flutterwave_encryption_key);
                            Constant.FLUTTERWAVE_PUBLIC_KEY_VAL = object.getString(Constant.flutterwave_public_key);
                            Constant.FLUTTERWAVE_SECRET_KEY_VAL = object.getString(Constant.flutterwave_secret_key);
                            Constant.VPA_PAYEE_ADDRESS = object.getString(Constant.VPA_PAYEE_ADDRESS_KEY);
                            Constant.VPA_PAYEE_NAME = object.getString(Constant.VPA_PAYEE_NAME_KEY);
                            Constant.VPA_MERCHANT_CODE = object.getString(Constant.VPA_MERCHANT_CODE_KEY);
                            setPaymentMethod();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, activity, Constant.SETTING_URL, params, true);
    }

    @SuppressLint("SetTextI18n")
    public void walletUncheck() {
        paymentMethod = "";

        rbCod.setChecked(false);
        rbPayU.setChecked(false);
        rbPayPal.setChecked(false);
        rbRazorPay.setChecked(false);
        rbPayStack.setChecked(false);
        rbFlutterWave.setChecked(false);

        lytPayOption.setVisibility(View.VISIBLE);
        tvWltBalance.setText(getString(R.string.total) + Constant.SETTING_CURRENCY_SYMBOL + Constant.WALLET_BALANCE);
        subtotal = (subtotal + usedBalance);
        tvSubTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(subtotal));
        chWallet.setChecked(false);
        chWallet.setTag("false");


    }

    public void setPaymentMethod() {


        if (Constant.FLUTTERWAVE.equals("0") &&
                Constant.PAYPAL.equals("0") && Constant.PAYUMONEY.equals("0") && Constant.COD.equals("0") && Constant.RAZORPAY.equals("0") && Constant.PAYSTACK.equals("0")) {
            lytPayOption.setVisibility(View.VISIBLE);
        } else {
            lytPayOption.setVisibility(View.VISIBLE);


            if (Constant.PAYUMONEY.equals("1")) {
                lytPayU.setVisibility(View.VISIBLE);
            }
            if (Constant.RAZORPAY.equals("1")) {
                lytRazorPay.setVisibility(View.VISIBLE);
            }
            if (Constant.PAYSTACK.equals("1")) {
                lytPayStack.setVisibility(View.VISIBLE);
            }
            if (Constant.FLUTTERWAVE.equals("1")) {
                lytFlutterWave.setVisibility(View.VISIBLE);
            }
            if (Constant.PAYPAL.equals("1")) {
                lytPayPal.setVisibility(View.VISIBLE);
            }
            if (Constant.PAYPAL.equals("1")) {
                lytPayPal.setVisibility(View.VISIBLE);
            }
        }

        lytUpi.setVisibility(View.VISIBLE);

        rbCod.setOnClickListener(v -> {
            rbCod.setChecked(true);
            rbPayU.setChecked(false);
            rbPayPal.setChecked(false);
            rbRazorPay.setChecked(false);
            rbPayStack.setChecked(false);
            rbFlutterWave.setChecked(false);
            rbUpiButton.setChecked(false);

            paymentMethod = rbCod.getTag().toString();

        });
        rbPayU.setOnClickListener(v -> {
            rbCod.setChecked(false);
            rbPayU.setChecked(true);
            rbPayPal.setChecked(false);
            rbRazorPay.setChecked(false);
            rbPayStack.setChecked(false);
            rbFlutterWave.setChecked(false);
            rbUpiButton.setChecked(false);
            paymentMethod = rbPayU.getTag().toString();
        });

        rbPayPal.setOnClickListener(v -> {
            rbCod.setChecked(false);
            rbPayU.setChecked(false);
            rbPayPal.setChecked(true);
            rbRazorPay.setChecked(false);
            rbPayStack.setChecked(false);
            rbFlutterWave.setChecked(false);
            rbUpiButton.setChecked(false);

            paymentMethod = rbPayPal.getTag().toString();

        });

        rbRazorPay.setOnClickListener(v -> {
            rbCod.setChecked(false);
            rbPayU.setChecked(false);
            rbPayPal.setChecked(false);
            rbRazorPay.setChecked(true);
            rbPayStack.setChecked(false);
            rbFlutterWave.setChecked(false);
            rbUpiButton.setChecked(false);

            paymentMethod = rbRazorPay.getTag().toString();
            Checkout.preload(getContext());
        });

        rbPayStack.setOnClickListener(v -> {
            rbCod.setChecked(false);
            rbPayU.setChecked(false);
            rbPayPal.setChecked(false);
            rbRazorPay.setChecked(false);
            rbPayStack.setChecked(true);
            rbFlutterWave.setChecked(false);
            rbUpiButton.setChecked(false);

            paymentMethod = rbPayStack.getTag().toString();

        });

        rbFlutterWave.setOnClickListener(v -> {
            rbCod.setChecked(false);
            rbPayU.setChecked(false);
            rbPayPal.setChecked(false);
            rbRazorPay.setChecked(false);
            rbPayStack.setChecked(false);
            rbFlutterWave.setChecked(true);
            rbUpiButton.setChecked(false);

            paymentMethod = rbFlutterWave.getTag().toString();

        });

        rbUpiButton.setOnClickListener(v -> {
            rbCod.setChecked(false);
            rbPayU.setChecked(false);
            rbPayPal.setChecked(false);
            rbRazorPay.setChecked(false);
            rbPayStack.setChecked(false);
            rbFlutterWave.setChecked(false);
            rbUpiButton.setChecked(true);

            paymentMethod = rbUpiButton.getTag().toString();


        });




        if (!membershipTransaction) {

            getTimeSlots();
            tvProceedOrder.setOnClickListener(v -> PlaceOrderProcess());
        }else if (membershipTransaction&&transacationType==TypePurchase.SUBSCRIBE||
                    membershipTransaction&&transacationType==TypePurchase.PACK){
            getTimeSlots();
        }

    }

    public void getTimeSlots() {
        GetTimeSlotConfig(session, getActivity());


        if (transacationType==TypePurchase.PACK
        ){

            if (session.getData(Constant.SUBSCRIBED_TIMESLOT).equals("1")){
                isSlotEnabled = true;

                deliveryTimeLyt.setVisibility(View.VISIBLE);
                GetTimeSlots();
                deliveryDay = deliveryTime;
            }else {
                isSlotEnabled = false;

            }


        }else if (transacationType==TypePurchase.SUBSCRIBE){

            if (session.getData(Constant.PACK_TIMESLOT).equals("1")){
                isSlotEnabled = true;

                deliveryTimeLyt.setVisibility(View.VISIBLE);
                GetTimeSlots();
                deliveryDay = deliveryTime;

            }else {
                isSlotEnabled = false;

            }




        }else {
            if (session.getData(Constant.IS_TIME_SLOTS_ENABLE).equals(Constant.GetVal)) {

                isSlotEnabled = true;

                deliveryTimeLyt.setVisibility(View.VISIBLE);

                StartDate = Calendar.getInstance();
                EndDate = Calendar.getInstance();
                mYear = StartDate.get(Calendar.YEAR);
                mMonth = StartDate.get(Calendar.MONTH);
                mDay = StartDate.get(Calendar.DAY_OF_MONTH);

                int DeliveryStartFrom = Integer.parseInt(session.getData(Constant.DELIVERY_STARTS_FROM)) - 1;
                int DeliveryAllowFrom = Integer.parseInt(session.getData(Constant.ALLOWED_DAYS));

                StartDate.add(Calendar.DATE, DeliveryStartFrom);

                EndDate.add(Calendar.DATE, (DeliveryStartFrom + DeliveryAllowFrom));

                dateList = ApiConfig.getDates(StartDate.get(Calendar.DATE) + "-" +
                        (StartDate.get(Calendar.MONTH) + 1) + "-" + StartDate.get(Calendar.YEAR), EndDate.get(Calendar.DATE) +
                        "-" + (EndDate.get(Calendar.MONTH) + 1) + "-" + EndDate.get(Calendar.YEAR));
                setDateList(dateList);

                GetTimeSlots();

            } else {
                isSlotEnabled  =false;
                deliveryTimeLyt.setVisibility(View.GONE);
                deliveryDay = "Date : N/A";
                deliveryTime = "Time : N/A";
            }
        }
    }

    public void setDateList(ArrayList<String> datesList) {
        bookingDates = new ArrayList<>();
        for (int i = 0; i < datesList.size(); i++) {
            String[] date = datesList.get(i).split("-");

            BookingDate bookingDate1 = new BookingDate();
            bookingDate1.setDate(date[0]);
            bookingDate1.setMonth(date[1]);
            bookingDate1.setYear(date[2]);
            bookingDate1.setDay(date[3]);

            bookingDates.add(bookingDate1);
        }
        dateAdapter = new DateAdapter(getActivity(), bookingDates);

        recyclerViewDates.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewDates.setAdapter(dateAdapter);

    }


    private void purchaseCustom(TypePurchase typePurchase) {


        Paper.book().write(Constant.CUSTOM_TRANSACTION, true);

        AlertDialog.Builder confirmDialog = new AlertDialog.Builder(activity);
        confirmDialog.setTitle(getResources().getString(R.string.confirm_purchase));
        confirmDialog.setMessage(getResources().getString(R.string.proceed_order));


        confirmDialog.setPositiveButton(getResources().getString(R.string.proceed), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                switch (typePurchase) {

                    case MEMBERSHIP:
                        //Get Membership params in paper pd
                        //user_id, plan_id
                        Paper.book().write(Constant.CUSTOM_TRANSACTION_TYPE, TypePurchase.MEMBERSHIP);
                        plan = Paper.book().read(Constant.PLAN_DETAILS);

                        if (paymentMethod.equals(rbRazorPay.getTag())) {
                            dialog.dismiss();
                            CreateOrderId(Double.parseDouble(plan.getPrice()));

                        } else if (paymentMethod.equals(rbPayU.getTag())) {
                            dialog.dismiss();
                            sendparams.put(Constant.AMOUNT, plan.getPrice());
                            sendparams.put(Constant.TOTAL, plan.getPrice());
                            sendparams.put(Constant.FINAL_TOTAL, plan.getPrice());
                            sendparams.put(Constant.USER_NAME, session.getData(Session.KEY_NAME));
                            sendparams.put(Constant.FIRST_NAME, session.getData(Session.KEY_NAME));
                            paymentModelClass.OnPayClick(getActivity(), sendparams, TypePurchase.MEMBERSHIP.name(), String.valueOf(sendparams.get(Constant.FINAL_TOTAL)));

                        } else if (paymentMethod.equals(rbUpiButton.getTag())) {
                            dialog.dismiss();
                            startUPIPayment(Double.parseDouble(plan.getPrice()), "membership");
                        } else if (paymentMethod.equals(getString(R.string.flutterwave))) {
                            dialog.dismiss();
                            subtotal = Double.parseDouble(plan.getPrice());
                            StartFlutterWavePayment(subtotal);
                        }

                        break;

                    case SUBSCRIBE:
                        //Get Membership params in paper pd
                        //user_id, plan_id

                        Subscribe subscribe = Paper.book().read(Constant.SUBSCRIBE_DETAILS);


                        if (paymentMethod.equals(rbRazorPay.getTag())) {
                            dialog.dismiss();
                            CreateOrderId(Double.parseDouble(subscribe.getFinalTotal()));

                        } else if (paymentMethod.equals(rbPayU.getTag())) {
                            dialog.dismiss();
                            sendparams.put(Constant.AMOUNT, subscribe.getPriceVariation().getDiscounted_price());
                            sendparams.put(Constant.TOTAL, subscribe.getPriceVariation().getDiscounted_price());
                            sendparams.put(Constant.FINAL_TOTAL, subscribe.getFinalTotal());
                            sendparams.put(Constant.USER_NAME, session.getData(Session.KEY_NAME));
                            sendparams.put(Constant.FIRST_NAME, session.getData(Session.KEY_NAME));

                            paymentModelClass.OnPayClick(getActivity(), sendparams, TypePurchase.SUBSCRIBE.toString(), String.valueOf(sendparams.get(Constant.FINAL_TOTAL)));

                        } else if (paymentMethod.equals(getString(R.string.flutterwave))) {
                            dialog.dismiss();
                            subtotal = Double.parseDouble(subscribe.getFinalTotal());
                            StartFlutterWavePayment(subtotal);
                        } else if (paymentMethod.equals(rbUpiButton.getTag())) {
                            dialog.dismiss();
                            startUPIPayment(Double.parseDouble(subscribe.getFinalTotal()), "subscribe");
                        }
                        break;

                    case PACK:
                        //Get Membership params in paper pd
                        //user_id, plan_id

                        Pack pack = Paper.book().read(Constant.PACK_DETAILS);

                        if (paymentMethod.equals(rbRazorPay.getTag())) {
                            dialog.dismiss();
                            int total = pack.getPack_date_count() * Integer.parseInt(
                                    pack.getPackValue()
                            );



                            CreateOrderId(Double.parseDouble(String.valueOf(total)));

                        } else if (paymentMethod.equals(rbPayU.getTag())) {
                            dialog.dismiss();
                            sendparams.put(Constant.AMOUNT, pack.getPackValue());
                            sendparams.put(Constant.TOTAL, pack.getPackValue());
                            sendparams.put(Constant.FINAL_TOTAL, String.valueOf(total));
                            sendparams.put(Constant.USER_NAME, session.getData(Session.KEY_NAME));
                            sendparams.put(Constant.FIRST_NAME, session.getData(Session.KEY_NAME));


                            paymentModelClass.OnPayClick(getActivity(), sendparams, TypePurchase.PACK.toString(), String.valueOf(sendparams.get(Constant.FINAL_TOTAL)));

                        } else if (paymentMethod.equals(getString(R.string.flutterwave))) {
                            dialog.dismiss();
                            subtotal = Double.parseDouble(String.valueOf(total));
                            StartFlutterWavePayment(subtotal);
                        } else if (paymentMethod.equals(rbUpiButton.getTag())) {
                            dialog.dismiss();
                            int total = pack.getPack_date_count() * Integer.parseInt(
                                    pack.getPackValue()
                            );
                            startUPIPayment(
                                    Double.parseDouble(String.valueOf(total)), "pack");
                        }
                        break;

                }


            }
        });

        confirmDialog.setNegativeButton(activity.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        confirmDialog.show();

    }


    public void purchaseMembership(Activity activity, String paymentMethod, String razorPayId) {

        Plan plan = Paper.book().read(Constant.PLAN_DETAILS);
        sendparams.put(Constant.USER_ID, plan.getUser_id());
        sendparams.put(Constant.PLAN_ID, plan.getId());
        sendparams.put(Constant.PAYMENT_METHOD, paymentMethod);
        sendparams.put(Constant.TXN_ID, razorPayId);
        sendparams.put(Constant.ORDER_TYPE, TypePurchase.MEMBERSHIP.name());

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                try {
                    JSONObject data = new JSONObject(message);


                    if (!data.getBoolean(Constant.ERROR)) {
                        Gson gson = new Gson();

                        PlanSuccess planSuccess = gson.fromJson(data.toString(), PlanSuccess.class);

                        CheckoutActivity.fm.beginTransaction().replace(R.id.container,
                                new OrderConfirmFragment()).addToBackStack(null).commit();



                        // Forward to order successfull activity
                        Toast.makeText(activity, "success " + planSuccess.getMessage(), Toast.LENGTH_LONG).show();
                        AddTransaction(
                                activity,
                                data.getString(Constant.ORDER_ID),
                                paymentMethod,
                                razorPayId,
                                "Success",
                                "Purchased membeship",
                                sendparams);

                        MainActivity.fm.beginTransaction().replace(R.id.container,
                                new OrderConfirmFragment()).commit();



                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, activity, Constant.GET_PLAN, sendparams, true);

    }

    public void purchaseSubscription(Activity activity, String paymentMethod, String razorPayId) {

        Subscribe subscribe = Paper.book().read(Constant.SUBSCRIBE_DETAILS);
        double price = Double.parseDouble(subscribe.getPriceVariation().getDiscounted_price())
                + taxAmt;

        sendparams.put(Constant.PLACE_ORDER, "1");
        sendparams.put(Constant.PRICE, Constant.formater.format(price));
        sendparams.put(Constant.PAYMENT_METHOD, paymentMethod);
        sendparams.put(Constant.TXN_ID, razorPayId);
        sendparams.put(Constant.SUBSCRIBE_DATE, subscribe.getSelectedDays());
        sendparams.put(Constant.PRODUCT_VARIANT_ID, subscribe.getPriceVariation().getId());
        sendparams.put(Constant.QUANTITY, subscribe.getUnit());
        sendparams.put(Constant.FINAL_TOTAL, Constant.formater.format(price));
        sendparams.put(Constant.ORDER_TYPE, TypePurchase.SUBSCRIBE.name());
        sendparams.put(Constant.DELIVERY_TIME, (deliveryDay + " - " + deliveryTime));

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                try {
                    JSONObject data = new JSONObject(message);

                    if (!data.getBoolean(Constant.ERROR)) {
                        Gson gson = new Gson();
                        // Forward to order successfull activity
                        Toast.makeText(activity, "success", Toast.LENGTH_LONG).show();
                        AddTransaction(activity,
                                data.getString(Constant.ORDER_ID),
                                paymentMethod, razorPayId, "Success", "Purchased subscription", sendparams);

                        CheckoutActivity.fm.beginTransaction().replace(R.id.container,
                                new OrderConfirmFragment()).commit();




                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, activity, Constant.GET_SUBSCRIPTION, sendparams, true);


    }


    public void purchasePack(Activity activity, String paymentMethod, String transactionID) {

        Pack pack = Paper.book().read(Constant.PACK_DETAILS);

        HashMap<String, String> map = new HashMap<>(sendparams);
        map.put(Constant.PLACE_ORDER, "1");
        map.put(Constant.TOTAL, pack.getPackValue());
        map.put(Constant.PAYMENT_METHOD, paymentMethod);
        map.put(Constant.QUANTITY, pack.getQtyList().toString());
        map.put(Constant.TAX_AMOUNT,"0");
        map.put(Constant.TAX,"0");
        map.put(Constant.TAX_PERCENT,"0");
        map.put("pack_date", pack.getPack_date());
        map.put(Constant.PRODUCT_VARIANT_ID, pack.getVariantIdList().toString());
        map.put(Constant.PACK_ID, pack.getId());
        map.put(Constant.FINAL_TOTAL, pack.getPackValue());
        map.put(Constant.ORDER_TYPE, TypePurchase.PACK.name());
        map.put(Constant.DELIVERY_TIME, (deliveryDay + " - " + deliveryTime));


        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                try {
                    JSONObject data = new JSONObject(message);

                    if (!data.getBoolean(Constant.ERROR)) {
                        Gson gson = new Gson();

                        // Forward to order successfull activity
                        Toast.makeText(activity, "success", Toast.LENGTH_LONG).show();
                        AddTransaction(activity,data.getString(Constant.ORDER_ID)
                                , paymentMethod, transactionID,
                                "Success", "Purchased pack", sendparams);

                        CheckoutActivity.fm.beginTransaction().replace(R.id.container,
                                new OrderConfirmFragment()).addToBackStack(null).commit();



                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, activity, Constant.PLACE_PACK_ORDER, map, true);


    }


    @SuppressLint("SetTextI18n")
    public void PlaceOrderProcess() {
        if (deliveryDay.length() == 0) {
            Toast.makeText(getContext(), getString(R.string.select_delivery_day), Toast.LENGTH_SHORT).show();
            return;
        } else if (deliveryTime.length() == 0) {
            Toast.makeText(getContext(), getString(R.string.select_delivery_time), Toast.LENGTH_SHORT).show();
            return;
        } else if (paymentMethod.isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.select_payment_method), Toast.LENGTH_SHORT).show();
            return;
        }
        sendparams = new HashMap<>();
        sendparams.put(Constant.PLACE_ORDER, Constant.GetVal);
        sendparams.put(Constant.USER_ID, session.getData(Session.KEY_ID));
        sendparams.put(Constant.TAX_AMOUNT, "" + taxAmt);
        sendparams.put(Constant.TOTAL, "" + total);
        sendparams.put(Constant.TAX_PERCENT, "" + Constant.SETTING_TAX);
        sendparams.put(Constant.FINAL_TOTAL, "" + Constant.formater.format(subtotal));
        sendparams.put(Constant.PRODUCT_VARIANT_ID, String.valueOf(variantIdList));
        sendparams.put(Constant.QUANTITY, String.valueOf(qtyList));
        sendparams.put(Constant.MOBILE, session.getData(Session.KEY_MOBILE));
        sendparams.put(Constant.DELIVERY_CHARGE, "" + dCharge);
        sendparams.put(Constant.DELIVERY_TIME, (deliveryDay + " - " + deliveryTime));
        sendparams.put(Constant.KEY_WALLET_USED, chWallet.getTag().toString());
        sendparams.put(Constant.KEY_WALLET_BALANCE, String.valueOf(usedBalance));
        sendparams.put(Constant.PAYMENT_METHOD, paymentMethod);
        sendparams.put(Constant.MEMBER, session.getData(Constant.MEMBER));
        sendparams.put(Constant.ORDER_TYPE, TypePurchase.REGULAR.name());

        if (!pCode.isEmpty()) {
            sendparams.put(Constant.PROMO_CODE, pCode);
            sendparams.put(Constant.PROMO_DISCOUNT, Constant.formater.format(pCodeDiscount));
        }
        sendparams.put(Constant.ADDRESS, address);
        sendparams.put(Constant.LONGITUDE, session.getCoordinates(Session.KEY_LONGITUDE));
        sendparams.put(Constant.LATITUDE, session.getCoordinates(Session.KEY_LATITUDE));
        sendparams.put(Constant.EMAIL, session.getData(Session.KEY_EMAIL));



        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.dialog_order_confirm, null);
        alertDialog.setView(dialogView);
        alertDialog.setCancelable(true);
        final AlertDialog dialog = alertDialog.create();
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView tvDialogCancel, tvDialogConfirm, tvDialogItemTotal, tvDialogTaxPercent, tvDialogTaxAmt, tvDialogDeliveryCharge, tvDialogTotal, tvDialogPromoCode, tvDialogPCAmount, tvDialogWallet, tvDialogFinalTotal;
        LinearLayout lytDialogPromo, lytDialogWallet;


        lytDialogPromo = dialogView.findViewById(R.id.lytDialogPromo);
        lytDialogWallet = dialogView.findViewById(R.id.lytDialogWallet);
        tvDialogItemTotal = dialogView.findViewById(R.id.tvDialogItemTotal);
        tvDialogTaxPercent = dialogView.findViewById(R.id.tvDialogTaxPercent);
        tvDialogTaxAmt = dialogView.findViewById(R.id.tvDialogTaxAmt);
        tvDialogDeliveryCharge = dialogView.findViewById(R.id.tvDialogDeliveryCharge);
        tvDialogTotal = dialogView.findViewById(R.id.tvDialogTotal);
        tvDialogPCAmount = dialogView.findViewById(R.id.tvDialogPCAmount);
        tvDialogWallet = dialogView.findViewById(R.id.tvDialogWallet);
        tvDialogFinalTotal = dialogView.findViewById(R.id.tvDialogFinalTotal);
        tvDialogCancel = dialogView.findViewById(R.id.tvDialogCancel);
        tvDialogConfirm = dialogView.findViewById(R.id.tvDialogConfirm);


        if (pCodeDiscount > 0) {
            lytDialogPromo.setVisibility(View.VISIBLE);
            tvDialogPCAmount.setText("- " + Constant.SETTING_CURRENCY_SYMBOL + pCodeDiscount);
        } else {
            lytDialogPromo.setVisibility(View.GONE);
        }

        if (chWallet.getTag().toString().equals("true") && !membershipTransaction) {
            lytDialogWallet.setVisibility(View.VISIBLE);
            tvDialogWallet.setText("- " + Constant.SETTING_CURRENCY_SYMBOL + usedBalance);
        } else {
            lytDialogWallet.setVisibility(View.GONE);
        }


        totalAfterTax = (total + dCharge + taxAmt);

        //CHECKING MEMBERSHIP TRANSACTION
        tvDialogItemTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(total));
        tvDialogDeliveryCharge.setText(dCharge > 0 ? Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(dCharge) : getString(R.string.free));
        tvDialogTaxPercent.setText(getString(R.string.tax) + "(" + Constant.SETTING_TAX + "%) :");
        tvDialogTaxAmt.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(taxAmt));
        tvDialogTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(subtotal));
        tvDialogFinalTotal.setText(Constant.SETTING_CURRENCY_SYMBOL + Constant.formater.format(subtotal));


        tvDialogConfirm.setOnClickListener(v -> {


            if (paymentMethod.equals(getResources().getString(R.string.codpaytype)) || paymentMethod.equals("wallet")) {
                ApiConfig.RequestToVolley((result, response) -> {
                    if (result) {
                        try {
                            JSONObject object = new JSONObject(response);
                            if (!object.getBoolean(Constant.ERROR)) {
                                if (chWallet.getTag().toString().equals("true"))
//                                    ApiConfig.getWalletBalance(getActivity(), session, new GetWalletPrice() {
//                                        @Override
//                                        public void onSuccesPrice(Double result) {
//
//                                        }
//                                    });
                                    AddTransaction(activity, object.getString(Constant.ORDER_ID),
                                           paymentMethod, object.getString(Constant.ORDER_ID)
                                                    , "success",
                                            activity.getString(R.string.order_success), sendparams);



//                                    ApiConfig.AddMultipleProductInCart(
//                                            session, activity, new HashMap<>());

                                dialog.dismiss();

                                clearBackStackAndNavigate(MainActivity.fm);

                            } else {
                                Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, getActivity(), Constant.ORDERPROCESS_URL, sendparams, true);

                dialog.dismiss();




            } else {
                sendparams.put(Constant.USER_NAME, session.getData(Session.KEY_NAME));
                if (paymentMethod.equals(getString(R.string.pay_u))) {
                    dialog.dismiss();
                    paymentModelClass.OnPayClick(getActivity(), sendparams, "Cart Order", String.valueOf(sendparams.get(Constant.FINAL_TOTAL)));
                } else if (paymentMethod.equals(getString(R.string.paypal))) {
                    dialog.dismiss();
                    StartPayPalPayment(sendparams);
                } else if (paymentMethod.equals(getString(R.string.razor_pay))) {
                    dialog.dismiss();
                    CreateOrderId(subtotal);

                } else if (paymentMethod.equals(getString(R.string.paystack))) {
                    dialog.dismiss();
                    sendparams.put("from", "payment");
                    callPayStack(sendparams);
                } else if (paymentMethod.equals(getString(R.string.flutterwave))) {
                    dialog.dismiss();
                    StartFlutterWavePayment(subtotal);
                } else if (paymentMethod.equals(rbUpiButton.getTag())) {
                    dialog.dismiss();
                    startUPIPayment(Double.parseDouble(sendparams
                            .get(Constant.FINAL_TOTAL)), TypePurchase.REGULAR.name());

                }
            }
        });

        tvDialogCancel.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    public void callPayStack(final Map<String, String> sendParams) {
        Intent intent = new Intent(activity, PayStackActivity.class);
        intent.putExtra("params", (Serializable) sendParams);
        startActivity(intent);
    }

    public void CreateOrderId(double payble) {

        String[] amount = String.valueOf(payble * 100).split("\\.");
        Map<String, String> params = new HashMap<>();
        params.put("amount", amount[0]);
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        JSONObject object = new JSONObject(response);
                        if (!object.getBoolean(Constant.ERROR)) {
                            startPayment(object.getString("id"), object.getString("amount"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, getActivity(), Constant.Get_RazorPay_OrderId, params, true);

    }

    public void startPayment(String orderId, String payAmount) {
        Checkout checkout = new Checkout();
        checkout.setKeyID(Constant.RAZOR_PAY_KEY_VALUE);
        checkout.setImage(R.drawable.ic_launcher);

        try {
            JSONObject options = new JSONObject();
            options.put(Constant.NAME, session.getData(Session.KEY_NAME));
            options.put(Constant.ORDER_ID, orderId);
            options.put(Constant.CURRENCY, "INR");
            options.put(Constant.AMOUNT, payAmount);

            JSONObject preFill = new JSONObject();
            preFill.put(Constant.EMAIL, session.getData(Session.KEY_EMAIL));
            preFill.put(Constant.CONTACT, session.getData(Session.KEY_MOBILE));
            options.put("prefill", preFill);

            checkout.open(getActivity(), options);
        } catch (Exception e) {
            Log.d(TAG, "Error in starting Razorpay Checkout", e);
        }
    }

    public void PlaceOrder(final Activity activity, final String paymentType, final String txnid, boolean issuccess, final Map<String, String> sendparams, final String status) {
        if (issuccess) {
            ApiConfig.RequestToVolley(new VolleyCallback() {
                @Override
                public void onSuccess(boolean result, String response) {

                        try {
                            JSONObject object = new JSONObject(response);
                            if (!object.getBoolean(Constant.ERROR)) {
                                AddTransaction(activity, object.getString(Constant.ORDER_ID),
                                        paymentType, txnid, status, activity.getString(R.string.order_success), sendparams);
                                clearBackStackAndNavigate(MainActivity.fm);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

            }, activity, Constant.ORDERPROCESS_URL, sendparams, true);
        } else {

            AddTransaction(activity, "", getString(R.string.razor_pay), txnid, status, getString(R.string.order_failed), sendparams);
        }
    }


    private void clearBackStackAndNavigate(FragmentManager manager) {
        OrderConfirmFragment orderConfirmFragment = new OrderConfirmFragment();
        manager.beginTransaction().replace(R.id.container, orderConfirmFragment).commit();


    }


    public void AddTransaction(Activity activity, String orderId, String paymentType, String txnid, final String status, String message, Map<String, String> sendparams) {
        Map<String, String> transparams = new HashMap<>();
        transparams.put(Constant.Add_TRANSACTION, Constant.GetVal);
        transparams.put(Constant.USER_ID, sendparams.get(Constant.USER_ID));
        transparams.put(Constant.ORDER_ID, orderId);
        transparams.put(Constant.TYPE, paymentType);
        transparams.put(Constant.TAX_PERCENT, "" + Constant.SETTING_TAX);
        transparams.put(Constant.TRANS_ID, txnid);
        transparams.put(Constant.AMOUNT, sendparams.get(Constant.FINAL_TOTAL));
        transparams.put(Constant.STATUS, status);
        transparams.put(Constant.MESSAGE, message);
        Date c = Calendar.getInstance().getTime();



        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        transparams.put("transaction_date", df.format(c));

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {

                if (result) {
                    try {
                        JSONObject objectbject = new JSONObject(response);
                        if (!objectbject.getBoolean(Constant.ERROR)) {
                            if (status.equals("Failed")) {

                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, activity, Constant.ORDERPROCESS_URL, transparams, true);
    }

    public void StartPayPalPayment(final Map<String, String> sendParams) {

        final Map<String, String> params = new HashMap<>();
        params.put(Constant.FIRST_NAME, sendParams.get(Constant.USER_NAME));
        params.put(Constant.LAST_NAME, sendParams.get(Constant.USER_NAME));
        params.put(Constant.PAYER_EMAIL, sendParams.get(Constant.EMAIL));
        params.put(Constant.ITEM_NAME, "Card Order");
        params.put(Constant.ITEM_NUMBER, System.currentTimeMillis() + Constant.randomNumeric(3));
        params.put(Constant.AMOUNT, sendParams.get(Constant.FINAL_TOTAL));

        // System.out.println("======params paypal "+params.toString());
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                //System.out.println("=====url paypal == "+response );
                Intent intent = new Intent(getContext(), PayPalWebActivity.class);
                intent.putExtra("url", response);
                intent.putExtra("item_no", params.get(Constant.ITEM_NUMBER));
                intent.putExtra("params", (Serializable) sendParams);
                startActivity(intent);
            }
        }, getActivity(), Constant.PAPAL_URL, params, true);
    }

    private enum TIMEDAYS{
        MORNING,
        AFTERNOON,
        EVENING,
        NIGHT
    }


    public void GetTimeSlots() {
        slotList = new ArrayList<>();
        Map<String, String> params = new HashMap<>();
        params.put("get_time_slots", Constant.GetVal);
        final TIMEDAYS[] timedays = new TIMEDAYS[1];
        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        JSONObject object = new JSONObject(response);

                        if (!object.getBoolean(Constant.ERROR)) {
                            JSONArray jsonArray = object.getJSONArray("time_slots");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object1 = jsonArray.getJSONObject(i);

                                String timeTitle = object1.getString("title");

                                String langSelected = session.getLangData("lang");
                                String hindiTag = null;

                                if (timeTitle.contains("Morning"))

                                    timedays[0] = TIMEDAYS.MORNING;
                                else if (timeTitle.contains("Afternoon")){
                                    timedays[0] = TIMEDAYS.AFTERNOON;

                                }else if (timeTitle.contains("Evening")){
                                    timedays[0] = TIMEDAYS.EVENING;

                                }else if (timeTitle.contains("Night")){
                                    timedays[0] = TIMEDAYS.NIGHT;

                                }

                                if (langSelected.equals("en")){

                                    hindiTag = timeTitle;
                                }else {


                                    switch (timedays[0]) {

                                        case MORNING:
                                            hindiTag =  timeTitle.replace("Morning","सुबह");
                                            break;
                                        case AFTERNOON:
                                            hindiTag=  timeTitle.replace("Afternoon","दोपहर");

                                            break;
                                        case EVENING:
                                            hindiTag =  timeTitle.replace("Evening","शाम");
                                            break;
                                        case NIGHT:
                                            hindiTag =  timeTitle.replace("Night","रात");
                                            break;


                                    }
                                }



                                slotList.add(new Slot(object1.getString("id"), hindiTag, object1.getString("last_order_time")));
                            }

                            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

                            adapter = new SlotAdapter(deliveryTime, getActivity(), slotList);
                            recyclerView.setAdapter(adapter);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, getActivity(), Constant.SETTING_URL, params, true);
    }


    private void StartFlutterWavePayment(double amount) {
//        new RavePayManager(requireActivity())
//                .setAmount(amount)
//                .setEmail(session.getData(Constant.EMAIL))
//                .setfName(session.getData(Constant.FIRST_NAME))
//                .setlName(session.getData(Constant.LAST_NAME))
//                .setNarration(getString(R.string.app_name) + " Shopping")
//                .setPublicKey(Constant.FLUTTERWAVE_PUBLIC_KEY_VAL)
//                .setEncryptionKey(Constant.FLUTTERWAVE_ENCRYPTION_KEY_VAL)
//                .setTxRef(System.currentTimeMillis() + "Ref")
//                .acceptAccountPayments(true)
//                .acceptCardPayments(true)
//                .acceptMpesaPayments(true)
//                .onStagingEnv(false)
//                .showStagingLabel(true)
//                .isPreAuth(true)
//                .initialize();

        new RaveUiManager(activity).setAmount(amount)
                .setCurrency("USD")
                .setEmail(session.getData(Constant.EMAIL))
                .setfName(session.getData(Constant.FIRST_NAME))
                .setlName(session.getData(Constant.FIRST_NAME))
                .setPublicKey(Constant.FLUTTERWAVE_PUBLIC_KEY_VAL)
                .setEncryptionKey(Constant.FLUTTERWAVE_ENCRYPTION_KEY_VAL)
                .setTxRef(System.currentTimeMillis() + "Ref")
                    .acceptAccountPayments(true)
                    .acceptCardPayments(true)
                    .acceptMpesaPayments(true)
                    .acceptAchPayments(true)
                    .acceptGHMobileMoneyPayments(true)
                    .acceptUgMobileMoneyPayments(true)
                    .acceptZmMobileMoneyPayments(true)
                    .acceptRwfMobileMoneyPayments(true)
                    .acceptSaBankPayments(true)
                    .acceptUkPayments(true)
                    .acceptBankTransferPayments(true)
                    .acceptUssdPayments(true)
                    .acceptBarterPayments(true)
                    .acceptFrancMobileMoneyPayments(true)
                    .allowSaveCardFeature(true)
                    .onStagingEnv(true)
                .isPreAuth(true)
                .shouldDisplayFee(true)
                    .showStagingLabel(true)
                    .initialize();



    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode != RaveConstants.RAVE_REQUEST_CODE && data != null) {
            paymentModelClass.TrasactionMethod(data, getActivity(), "payment");
        } else if (requestCode == RaveConstants.RAVE_REQUEST_CODE && data != null && data.getStringExtra("response") != null) {



            try {
                JSONObject details = new JSONObject(data.getStringExtra("response"));
                JSONObject jsonObject = details.getJSONObject(Constant.DATA);

                if (resultCode == RavePayActivity.RESULT_SUCCESS) {
                    if (transacationType == TypePurchase.SUBSCRIBE) {
                        purchaseSubscription(getActivity(), jsonObject.getString("paymentType"),
                                jsonObject.getString("txRef")
                        );
                    } else if (transacationType == TypePurchase.PACK) {
                        purchasePack(getActivity(), jsonObject.getString("paymentType"),
                                jsonObject.getString("txRef")
                        );
                    } else if (transacationType == TypePurchase.MEMBERSHIP) {
                        purchaseMembership(getActivity(), jsonObject.getString("paymentType"),
                                jsonObject.getString("txRef")
                        );
                    } else {
                        Toast.makeText(getContext(), getString(R.string.order_placed1), Toast.LENGTH_LONG).show();
                        new PaymentModelClass(getActivity()).PlaceOrder(getActivity(), jsonObject.getString("paymentType"), jsonObject.getString("txRef"), true, sendparams, "Success");
                    }
                } else if (resultCode == RavePayActivity.RESULT_ERROR) {
                    new PaymentModelClass(getActivity()).PlaceOrder(getActivity(), "", "", false, sendparams, "Failed");
                    Toast.makeText(getContext(), getString(R.string.order_error), Toast.LENGTH_LONG).show();
                } else if (resultCode == RavePayActivity.RESULT_CANCELLED) {
                    new PaymentModelClass(getActivity()).PlaceOrder(getActivity(), "", "", false, sendparams, "Failed");
                    Toast.makeText(getContext(), getString(R.string.order_cancel), Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Constant.TOOLBAR_TITLE = getString(R.string.payment);
        activity.invalidateOptionsMenu();

        hideKeyboard();
    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(root.getApplicationWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.toolbar_cart).setVisible(false);
        menu.findItem(R.id.toolbar_sort).setVisible(false);
        menu.findItem(R.id.toolbar_search).setVisible(false);
    }


    private void startUPIPayment(double amount, String orderDes) {


        final String vpa_address = Constant.VPA_PAYEE_ADDRESS.toLowerCase();
        final String vpa_name = Constant.VPA_PAYEE_NAME.toLowerCase();
        final String vpa_code = Constant.VPA_MERCHANT_CODE.toLowerCase();



//        EasyUpiPayment.Builder builder = new EasyUpiPayment.Builder(activity)
//                .setPayeeVpa(vpa_address)
//                .setPayeeName(vpa_name)
//                .setPayeeMerchantCode("")
//                .setTransactionId("regular")
//                .setTransactionRefId("regular")
//                .setDescription("regular")
//                .setAmount(Constant.formater.format(amount));
//
//
//
//        try {
//            easyUpiPayment = builder.build();
//            easyUpiPayment.setPaymentStatusListener(new PaymentStatusListener() {
//                @Override
//                public void onTransactionCompleted(@NotNull TransactionDetails transactionDetails) {
//
//                    Log.d("SSSSSSSSSSSSSSSSS" , "::::");
//                }
//
//                @Override
//                public void onTransactionCancelled() {
//
//                }
//            });
//            easyUpiPayment.startPayment();
//
//        } catch (AppNotFoundException e) {
//            e.printStackTrace();
//        }



        PaymentDetail payment = new PaymentDetail(
                vpa_address,  //vpa/upi = your vpa/upi
                vpa_name,       //name = your name
                "",//payeeMerchantCode = only if you have merchantCode else pass empty string
                "",                     //txnRefId =  if you pass empty string we will generate txnRefId for you
                orderDes,          //description =
                String.valueOf(Constant.formater.format(amount)));


        Activity activity = requireActivity();
        boolean isMainActivity = false;
        if (activity instanceof MainActivity){
               isMainActivity = true;
        }else if (activity instanceof CheckoutActivity) {

            isMainActivity = false;
        }


       if (isMainActivity) {
        MainActivity.initializeUPI((MainActivity) requireActivity(), payment);
       }else {
        CheckoutActivity.initializeUPI((CheckoutActivity) requireActivity(), payment);
       }



    }

    private String generateTransactionID(String amount) {

        Random random = new Random();
        int number = random.nextInt(999);
        String tid = "ID" + session.getData(Constant.ID) + session.getData(Constant.CATEGORY_ID)
                + String.valueOf(amount) + String.valueOf(number);


        if (tid != null) {

        }
        return String.valueOf(tid);

    }


    @Override
    public void onTransactionCancelled() {

        Toast.makeText(activity, "Transaction cancelled", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onTransactionCompleted(@NotNull TransactionDetails transactionDetails) {

        Paper.init(activity);
        boolean customTransaction = Paper.book().read(Constant.CUSTOM_TRANSACTION, false);

        try {

            PaymentFragment.upiPayId = transactionDetails.getTransactionRefId();

                if (!customTransaction) {

                    if (WalletTransactionFragment.payFromWallet) {
                        WalletTransactionFragment.payFromWallet = false;
                        new WalletTransactionFragment().AddWalletBalance(activity,
                                new Session(activity), WalletTransactionFragment.amount, WalletTransactionFragment.msg, transactionDetails.getTransactionRefId());
                    } else {

                        new PaymentFragment().PlaceOrder(requireActivity(),
                                PaymentFragment.paymentMethod,
                                PaymentFragment.razorPayId, true,
                                PaymentFragment.sendparams, "Success");

                    }



                } else {
                    PaymentFragment.TypePurchase typePurchase = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE, PaymentFragment.TypePurchase.MEMBERSHIP);

                    if (typePurchase == PaymentFragment.TypePurchase.MEMBERSHIP) {
                        new PaymentFragment().purchaseMembership(requireActivity(),
                                PaymentFragment.paymentMethod, PaymentFragment.upiPayId);
                    } else if (typePurchase == PaymentFragment.TypePurchase.SUBSCRIBE) {
                        new PaymentFragment().purchaseSubscription(requireActivity(),
                                PaymentFragment.paymentMethod, PaymentFragment.upiPayId);
                    } else if (typePurchase == PaymentFragment.TypePurchase.PACK) {
                        new PaymentFragment().purchasePack(requireActivity(),
                                PaymentFragment.paymentMethod, PaymentFragment.upiPayId);
                    }



                }


        } catch (Exception e) {
            Log.d(TAG, "onPaymentSuccess  ", e);
        }


    }



}
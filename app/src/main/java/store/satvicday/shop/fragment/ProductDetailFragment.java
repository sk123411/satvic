package store.satvicday.shop.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.BackPressedOnPack;
import store.satvicday.shop.activity.BackPressedOnPackSecond;
import store.satvicday.shop.activity.LoginActivity;
import store.satvicday.shop.activity.MainActivity;
import store.satvicday.shop.activity.checkout.CheckoutActivity;
import store.satvicday.shop.activity.packs.AddCustomProductActivity;
import store.satvicday.shop.activity.packs.GetLeftPrice;
import store.satvicday.shop.activity.packs.PackProductActivity;
import store.satvicday.shop.activity.packs.adapter.CallGetLeftPrice;
import store.satvicday.shop.activity.subscribe.SubscribeActivity;
import store.satvicday.shop.adapter.AdapterStyle3;
import store.satvicday.shop.adapter.ProductLoadMoreAdapter;
import store.satvicday.shop.adapter.SliderAdapter;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.AppController;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.DatabaseHelper;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.Utils;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.Favorite;
import store.satvicday.shop.model.PriceVariation;
import store.satvicday.shop.model.Product;
import store.satvicday.shop.model.Slider;
import store.satvicday.shop.model.packs.Pack;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static store.satvicday.shop.helper.ApiConfig.AddMultipleProductInCart;
import static store.satvicday.shop.helper.ApiConfig.AddOrRemoveFavorite;
import static store.satvicday.shop.helper.ApiConfig.GetSettings;


public class ProductDetailFragment extends Fragment {
    static ArrayList<Slider> sliderArrayList;
    TextView tvMfg, tvMadeIn, txtProductName, txtqty, txtPrice, txtOriginalPrice, txtDiscountedPrice, txtMeasurement, txtstatus, tvReturnable, tvCancellable, tvTitleMadeIn, tvTitleMfg, tvTaxPercent;
    WebView webDescription;
    ViewPager viewPager;
    Spinner spinner;
    ImageView imgIndicator;
    SpannableString spannableString;
    LinearLayout mMarkersLayout, lytMfg, lytMadeIn;
    RelativeLayout lytqty, lytmainprice;
    ScrollView scrollView;
    Session session;
    boolean favorite;
    ImageView imgFav;
    ImageButton imgAdd, imgMinus;
    LinearLayout lytshare, lytsave, lytSimilar;
    int size, count;
    View root;
    int vpos;
    String from, id, qty;
    boolean isLogin;
    Product product;
    PriceVariation priceVariation;
    ArrayList<PriceVariation> priceVariationslist;
    DatabaseHelper databaseHelper;
    int position = 0;
    Button btnCart;
    Activity activity;
    RecyclerView recyclerView;
    RelativeLayout relativeLayout;
    TextView tvMore, pdSubscribeButton;
    private PaymentFragment.TypePurchase typePurchase;
    private Pack pack;
    private TextView leftText;

    public static BackPressedOnPack backPressedOnPackl;
    public static BackPressedOnPackSecond backPressedOnPackSecond;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_product_detail, container, false);

        setHasOptionsMenu(true);
        activity = getActivity();
        Paper.init(activity);
        Constant.CartValues = new HashMap<>();
//        try {
            session = new Session(activity);
            isLogin = session.isUserLoggedIn();
            databaseHelper = new DatabaseHelper(activity);
            product = Paper.book().read(Constant.PR);

            from = getArguments().getString(Constant.FROM);
            ;
            vpos = getArguments().getInt("vpos", 0);
            id = getArguments().getString("id");

            typePurchase = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE);
            pack = Paper.book().read(Constant.PACK_DETAILS);


            GetProductDetail(id);
            GetSettings(activity);


            try {


                if (from.equals("fragment") || from.equals("favorite") || from.equals("search")) {
                    position = getArguments().getInt("position");
                }


            } catch (NullPointerException e) {


            }

            lytqty = root.findViewById(R.id.lytqty);
            scrollView = root.findViewById(R.id.scrollView);
            mMarkersLayout = root.findViewById(R.id.layout_markers);
            sliderArrayList = new ArrayList<>();
            viewPager = root.findViewById(R.id.viewPager);
            txtProductName = root.findViewById(R.id.txtproductname);
            txtOriginalPrice = root.findViewById(R.id.txtoriginalprice);
            txtDiscountedPrice = root.findViewById(R.id.txtdiscountPrice);
            webDescription = root.findViewById(R.id.txtDescription);
            txtPrice = root.findViewById(R.id.txtprice);
            txtMeasurement = root.findViewById(R.id.txtmeasurement);
            imgFav = root.findViewById(R.id.imgFav);
            lytmainprice = root.findViewById(R.id.lytmainprice);
            txtqty = root.findViewById(R.id.txtqty);
            txtstatus = root.findViewById(R.id.txtstatus);
            imgAdd = root.findViewById(R.id.btnaddqty);
            imgMinus = root.findViewById(R.id.btnminusqty);
            spinner = root.findViewById(R.id.spinner);
            imgIndicator = root.findViewById(R.id.imgIndicator);
            lytshare = root.findViewById(R.id.lytshare);
            lytsave = root.findViewById(R.id.lytsave);
            lytSimilar = root.findViewById(R.id.lytSimilar);
            tvReturnable = root.findViewById(R.id.tvReturnable);
            tvCancellable = root.findViewById(R.id.tvCancellable);
            tvMadeIn = root.findViewById(R.id.tvMadeIn);
            tvTitleMadeIn = root.findViewById(R.id.tvTitleMadeIn);
            tvMfg = root.findViewById(R.id.tvMfg);
            tvTitleMfg = root.findViewById(R.id.tvTitleMfg);
            lytMfg = root.findViewById(R.id.lytMfg);
            lytMadeIn = root.findViewById(R.id.lytMadeIn);
            btnCart = root.findViewById(R.id.btnCart);
            recyclerView = root.findViewById(R.id.recyclerView);
            relativeLayout = root.findViewById(R.id.relativeLayout);
            tvMore = root.findViewById(R.id.tvMore);
            tvTaxPercent = root.findViewById(R.id.tvTaxPercent);
            pdSubscribeButton = root.findViewById(R.id.pdSubscribeButton);



            root.setFocusableInTouchMode(true);
            root.requestFocus();
            root.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {

                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {

                        backPressedOnPackl.onBack(true);

                    }

                    return false;
                }
            });










            if (typePurchase == PaymentFragment.TypePurchase.PACK) {

                pdSubscribeButton.setText(activity.getResources().getString(
                        R.string.add));
                pdSubscribeButton.setVisibility(View.GONE);
                btnCart.setVisibility(View.GONE);

//            String qty = Paper.book().read(Constant.QTY, "0");
//
//            txtqty.setText(qty);


            }


            lytmainprice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    spinner.performClick();
                }
            });

            tvMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ShowSimilar();
                }
            });

            lytSimilar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShowSimilar();
                }
            });

            btnCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainActivity.fm.beginTransaction().add(R.id.container, new CartFragment()).addToBackStack(null).commit();
                }
            });

            lytshare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new ShareProduct().execute();
                }
            });

            lytsave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isLogin) {
                        favorite = product.isIs_favorite();
                        if (AppController.isConnected(activity)) {
                            if (favorite) {
                                favorite = false;
                                product.setIs_favorite(false);
                                imgFav.setImageResource(R.drawable.ic_is_not_favorite);
                            } else {
                                favorite = true;
                                product.setIs_favorite(true);
                                imgFav.setImageResource(R.drawable.ic_is_favorite);
                            }
                            AddOrRemoveFavorite(activity, session, product.getId(), favorite);
                        }
                    } else {
                        favorite = databaseHelper.getFavouriteById(product.getId());
                        if (favorite) {
                            favorite = false;
                            imgFav.setImageResource(R.drawable.ic_is_not_favorite);
                        } else {
                            favorite = true;
                            imgFav.setImageResource(R.drawable.ic_is_favorite);
                        }
                        databaseHelper.AddOrRemoveFavorite(product.getId(), favorite);
                    }
                    if (from.equals("fragment")) {
                        ProductListFragment.productArrayList.get(position).setIs_favorite(favorite);
                        ProductListFragment.mAdapter.notifyDataSetChanged();
                    } else if (from.equals("favorite")) {
                        if (session.isUserLoggedIn()) {
                            Favorite favProduct = new Favorite();
                            favProduct.setId(product.getId());
                            favProduct.setProduct_id(product.getId());
                            favProduct.setName(product.getName());
                            favProduct.setSlug(product.getSlug());
                            favProduct.setSubcategory_id(product.getSubcategory_id());
                            favProduct.setImage(product.getImage());
                            favProduct.setStatus(product.getStatus());
                            favProduct.setDate_added(product.getDate_added());
                            favProduct.setCategory_id(product.getCategory_id());
                            favProduct.setIndicator(product.getIndicator());
                            favProduct.setManufacturer(product.getManufacturer());
                            favProduct.setMade_in(product.getMade_in());
                            favProduct.setReturn_status(product.getReturn_status());
                            favProduct.setCancelable_status(product.getCancelable_status());
                            favProduct.setTill_status(product.getTill_status());
                            favProduct.setPriceVariations(product.getPriceVariations());
//                        favProduct.setOther_images(product.getOther_images());
                            favProduct.setIs_favorite(true);
                            if (favorite) {
                                FavoriteFragment.favoriteArrayList.add(favProduct);
                            } else {
                                FavoriteFragment.favoriteArrayList.remove(position);
                            }
                            FavoriteFragment.favoriteLoadMoreAdapter.notifyDataSetChanged();


                        } else {
                            if (favorite) {
                                FavoriteFragment.productArrayList.add(product);
                            } else {
                                FavoriteFragment.productArrayList.remove(position);
                            }
                            FavoriteFragment.offlineFavoriteAdapter.notifyDataSetChanged();
                        }
                    }
                }
            });

            imgMinus.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View view) {
                    if (AppController.isConnected(activity)) {
                        Constant.CLICK = true;
                        count = Integer.parseInt(txtqty.getText().toString().isEmpty() ? "1" : txtqty.getText().toString());
                        if (!(count <= 0)) {
                            if (count != 0) {
                                count--;
                                txtqty.setText("" + count);
                                if (isLogin) {


                                    if (typePurchase == PaymentFragment.TypePurchase.PACK) {
                                        imgMinus.setEnabled(false);

                                        Pack pack = Paper.book().read(Constant.PACK_DETAILS);

                                        getLeftPrice(pack, new GetLeftPrice() {
                                            @Override
                                            public void onSuccess(String leftPrice) {

                                                removeProductFromPack(
                                                        pack, priceVariation,
                                                        session.getData(Constant.ID), imgMinus);
                                                //Add product in pack here

                                            }
                                        });

                                        if (Constant.PackCartValues.containsKey(priceVariation.getId())) {

                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                Constant.PackCartValues.replace(priceVariation.getId(), "" + count);
                                            }


                                        } else {
                                            Constant.PackCartValues.put(priceVariation.getId(), "" + count);
                                        }


                                    } else {


                                        if (Constant.CartValues.containsKey(priceVariationslist.get(vpos).getId())) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                Constant.CartValues.replace(priceVariationslist.get(vpos).getId(), "" + count);
                                            } else {
                                                Constant.CartValues.remove(priceVariationslist.get(vpos).getId());
                                                Constant.CartValues.put(priceVariationslist.get(vpos).getId(), "" + count);
                                            }
                                        } else {
                                            Constant.CartValues.put(priceVariationslist.get(vpos).getId(), "" + count);
                                        }




                                        if (Constant.CheckOutCartValues.containsKey(priceVariationslist.get(vpos).getId())) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                Constant.CheckOutCartValues.replace(priceVariationslist.get(vpos).getId(), "" + count);
                                            } else {
                                                Constant.CheckOutCartValues.remove(priceVariationslist.get(vpos).getId());
                                                Constant.CheckOutCartValues.put(priceVariationslist.get(vpos).getId(), "" + count);
                                            }
                                        } else {
                                            Constant.CheckOutCartValues.put(priceVariationslist.get(vpos).getId(), "" + count);
                                        }






                                        AddMultipleProductInCart(session, activity, Constant.CartValues);

                                    }
                                } else {
                                    databaseHelper.AddOrderData(priceVariationslist.get(vpos).getId(), priceVariation.getProduct_id(), "" + count);

                                    NotifyData(count);

                                }
                            }

                        }
                    }

                }
            });

            imgAdd.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View view) {
                    if (AppController.isConnected(activity)) {


                   //     try {
                            if (!(count > Float.parseFloat(priceVariationslist.get(vpos).getStock()))) {
                                if (count < Constant.MAX_PRODUCT_LIMIT) {
                                    Constant.CLICK = true;
                                    count = Integer.parseInt(txtqty.getText().toString());
                                    count++;
                                    txtqty.setText("" + count);
                                    if (isLogin) {

                                        if (typePurchase == PaymentFragment.TypePurchase.PACK) {

                                            imgAdd.setEnabled(false);

                                            Pack pack = Paper.book().read(Constant.PACK_DETAILS);

                                            getLeftPrice(pack, new GetLeftPrice() {
                                                @Override
                                                public void onSuccess(String leftPrice) {

                                                    addProductInPack(pack, priceVariation,
                                                            leftPrice, session.getData(Constant.ID), imgAdd, count, txtqty);

                                                }
                                            });


                                            if (Constant.PackCartValues.containsKey(priceVariation.getId())) {

                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                    Constant.PackCartValues.replace(priceVariation.getId(), "" + count);
                                                }


                                            } else {
                                                Constant.PackCartValues.put(priceVariation.getId(), "" + count);
                                            }


                                        } else {


                                            if (Constant.CartValues.containsKey(priceVariationslist.get(vpos).getId())) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                    Constant.CartValues.replace(priceVariationslist.get(vpos).getId(), "" + count);
                                                } else {
                                                    Constant.CartValues.remove(priceVariationslist.get(vpos).getId());
                                                    Constant.CartValues.put(priceVariationslist.get(vpos).getId(), "" + count);
                                                }
                                            } else {
                                                Constant.CartValues.put(priceVariationslist.get(vpos).getId(), "" + count);
                                            }


                                            if (Constant.CheckOutCartValues.containsKey(priceVariationslist.get(vpos).getId())) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                    Constant.CheckOutCartValues.replace(priceVariationslist.get(vpos).getId(), "" + count);
                                                } else {
                                                    Constant.CheckOutCartValues.remove(priceVariationslist.get(vpos).getId());
                                                    Constant.CheckOutCartValues.put(priceVariationslist.get(vpos).getId(), "" + count);
                                                }
                                            } else {
                                                Constant.CheckOutCartValues.put(priceVariationslist.get(vpos).getId(), "" + count);
                                            }






                                            AddMultipleProductInCart(session, activity, Constant.CartValues);


                                        }


                                    } else {
                                        databaseHelper.AddOrderData(priceVariationslist.get(vpos).getId(), priceVariation.getProduct_id(), "" + count);
                                        NotifyData(count);

                                    }
                                } else {
                                    Toast.makeText(getContext(), getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
                            }






//                        } catch (Exception e) {
//
//                            Log.d("XXXXXXXXXXXX", "::"+e.getCause());
//                            Log.d("XXXXXXXXXXXX", "::"+e.getMessage());
//
//                            startActivity(new Intent(view.getContext(), MainActivity.class).putExtra("from", ""));
//
//
//                        }


                    }


                }
            });


            String fromCartCheckout = getArguments().getString("coming");

            if (fromCartCheckout != null) {

                if (fromCartCheckout.equals("checkout")) {

                    root.setFocusableInTouchMode(true);
                    root.requestFocus();
                    root.setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {

                            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {

                                backPressedOnPackSecond.onBack(true);

                            }

                            return false;
                        }
                    });


                }

            }

//        } catch (Exception e) {
//
//            startActivity(new Intent(getActivity(),
//                    MainActivity.class).putExtra("from", ""));
//
//        }


        return root;
    }


    public void ShowSimilar() {
        Fragment fragment = new ProductListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("id", product.getId());
        bundle.putString("cat_id", product.getCategory_id());
        bundle.putString("from", Constant.PRODUCT_DETAIL);
        bundle.putString("name", "Similar Products");
        bundle.putString("from_pack", "no");
        fragment.setArguments(bundle);


        if (typePurchase == PaymentFragment.TypePurchase.PACK) {
            AddCustomProductActivity.fm.beginTransaction().add(R.id.container, fragment)
                    .addToBackStack(null).commit();


        } else {
            MainActivity.fm.beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();

        }

    }


    private void GetSimilarData(Product product) {
        Map<String, String> params = new HashMap<>();
        params.put(Constant.GET_SIMILAR_PRODUCT, Constant.GetVal);
        params.put(Constant.PRODUCT_ID, product.getId());
        params.put(Constant.CATEGORY_ID, product.getCategory_id());
        params.put(Constant.USER_ID, session.getData(Constant.ID));
        params.put(Constant.LIMIT, "" + Constant.LOAD_ITEM_LIMIT);

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        JSONObject objectbject = new JSONObject(response);
                        if (!objectbject.getBoolean(Constant.ERROR)) {

                            recyclerView.setLayoutManager(new LinearLayoutManager(
                                    activity, LinearLayoutManager.HORIZONTAL,
                                    false));


                            List<Pack.PackProduct> packProducts = Utils.decryptData(objectbject,
                                    Pack.PackProduct.class);


                            if (typePurchase == PaymentFragment.TypePurchase.PACK) {

                                List<Pack.PackProduct> filterProducts = new ArrayList<>();

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    filterProducts.addAll(packProducts.stream().filter(
                                            product -> {

                                                if (product.getVariants().size() > 0) {
                                                    if (Double.parseDouble(
                                                            product.getVariants().get(0).
                                                                    getDiscounted_price()) % 5 == 0) {
                                                        return true;
                                                    }

                                                }
                                                return false;
                                            }
                                    ).collect(Collectors.toList()));


                                } else {

                                    for (Pack.PackProduct packProduct : packProducts) {

                                        if (packProduct.getVariants().size() > 0) {


                                            if (Double.parseDouble(
                                                    product.getPriceVariations().get(0).getDiscounted_price()) % 5 == 0) {
                                                filterProducts.add(packProduct);
                                            }
                                        }
                                    }

                                }
                                AdapterStyle3 adapter =
                                        new AdapterStyle3(getContext(),
                                                activity,
                                                filterProducts,
                                                R.layout.offer_layout);
                                recyclerView.setAdapter(adapter);
                                relativeLayout.setVisibility(View.VISIBLE);


                            } else {

                                AdapterStyle3 adapter =
                                        new AdapterStyle3(getContext(),
                                                activity,
                                                packProducts,
                                                R.layout.offer_layout);
                                recyclerView.setAdapter(adapter);
                                relativeLayout.setVisibility(View.VISIBLE);


                            }


                        } else {
                            relativeLayout.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, activity, Constant.GET_SIMILAR_PRODUCT_URL, params, false);
    }


    public void NotifyData(int count) {
        switch (from) {
            case "fragment":
                ProductListFragment.productArrayList.get(position).getPriceVariations().get(vpos).setQty(count);
                ProductListFragment.mAdapter.notifyItemChanged(position, ProductListFragment.productArrayList.get(position));
                if (session.isUserLoggedIn()) {
                    ApiConfig.getCartItemCount(activity, session);
                } else {
                    databaseHelper.getTotalItemOfCart(activity);
                }
                activity.invalidateOptionsMenu();
                break;
            case "favorite":
                if (session.isUserLoggedIn()) {
                    FavoriteFragment.favoriteArrayList.get(position).getPriceVariations().get(vpos).setQty(count);
                    FavoriteFragment.favoriteLoadMoreAdapter.notifyItemChanged(position, FavoriteFragment.favoriteArrayList.get(position));
                } else {
                    FavoriteFragment.productArrayList.get(position).getPriceVariations().get(vpos).setQty(count);
                    FavoriteFragment.offlineFavoriteAdapter.notifyItemChanged(position, FavoriteFragment.productArrayList.get(position));
                    databaseHelper.getTotalItemOfCart(activity);
                }
                activity.invalidateOptionsMenu();
                break;
            case "search":
                SearchFragment.productArrayList.get(position).getPriceVariations().get(vpos).setQty(count);
                SearchFragment.productAdapter.notifyItemChanged(position, SearchFragment.productArrayList.get(position));
                if (!session.isUserLoggedIn()) {
                    databaseHelper.getTotalItemOfCart(activity);
                }
                activity.invalidateOptionsMenu();
                break;
            case "section":
            case "share":
                if (!session.isUserLoggedIn()) {
                    databaseHelper.getTotalItemOfCart(activity);
                } else {
                    ApiConfig.getCartItemCount(activity, session);
                }
                activity.invalidateOptionsMenu();
                break;
        }
    }

    private void GetProductDetail(final String productid) {
        root.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constant.PRODUCT_ID, productid);
        if (session.isUserLoggedIn()) {
            params.put(Constant.USER_ID, session.getData(Constant.ID));
        }

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String response) {
                if (result) {
                    try {
                        JSONObject objectbject = new JSONObject(response);
                        if (!objectbject.getBoolean(Constant.ERROR)) {

                            JSONObject object = new JSONObject(response);
                            JSONArray jsonArray = object.getJSONArray(Constant.DATA);
                            product = ApiConfig.GetProductList(jsonArray).get(0);
                            priceVariationslist = product.getPriceVariations();

                            SetProductDetails(product);
                            GetSimilarData(product);

                            root.findViewById(R.id.progressBar).setVisibility(View.GONE);
                        } else {
                            root.findViewById(R.id.progressBar).setVisibility(View.GONE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        root.findViewById(R.id.progressBar).setVisibility(View.GONE);
                    }
                }
            }
        }, activity, Constant.GET_PRODUCT_DETAIL_URL, params, false);
    }


    @SuppressLint("SetTextI18n")
    private void SetProductDetails(final Product product) {

        if (product.getEnabled_status().equals("1")) {

            pdSubscribeButton.setVisibility(View.VISIBLE);
        } else {

            pdSubscribeButton.setVisibility(View.GONE);

        }


        try {
            sliderArrayList = new ArrayList<>();

            Object array = product.getOther_images();
            if (array != null) {
                JSONArray jsonArray = product.getOther_images();
                size = jsonArray.length();


                for (int i = 0; i < jsonArray.length(); i++) {
                    sliderArrayList.add(new Slider(jsonArray.getString(i)));
                }

            } else {

            }


            sliderArrayList.add(new Slider(product.getImage()));

            if (product.getMade_in().length() > 0) {
                lytMadeIn.setVisibility(View.VISIBLE);
                tvMadeIn.setText(product.getMade_in());
            }

            if (product.getManufacturer().length() > 0) {
                lytMfg.setVisibility(View.VISIBLE);
                tvMfg.setText(product.getManufacturer());
            }

            if (isLogin) {
                if (product.isIs_favorite()) {
                    favorite = true;
                    imgFav.setImageResource(R.drawable.ic_is_favorite);
                } else {
                    favorite = false;
                    imgFav.setImageResource(R.drawable.ic_is_not_favorite);
                }
            } else {
                if (databaseHelper.getFavouriteById(product.getId())) {
                    imgFav.setImageResource(R.drawable.ic_is_favorite);
                } else {
                    imgFav.setImageResource(R.drawable.ic_is_not_favorite);
                }
            }

            if (isLogin) {

                if (typePurchase == PaymentFragment.TypePurchase.PACK) {
//                    String qty = Paper.book().read(Constant.QTY, "0");
//                    txtqty.setText(qty);


//                    if (Constant.PackCartValues.containsKey(priceVariation.getId())) {
//                        txtqty.setText(Constant.PackCartValues.get(priceVariation.getId()));
//                    } else {
//                        txtqty.setText("0");
//                    }


                } else {
                    if (Constant.CartValues.containsKey(product.getPriceVariations().get(0).getId())) {

                        txtqty.setText("" + Constant.CartValues.get(product.getPriceVariations().get(0).getId()));


                    } else {
                        txtqty.setText(product.getPriceVariations().get(0).getCart_count());
                    }
                }
            } else {
                txtqty.setText(databaseHelper.CheckOrderExists(product.getPriceVariations().get(0).getId(), product.getPriceVariations().get(0).getProduct_id()));
            }

            if (product.getReturn_status().equalsIgnoreCase("1")) {
                tvReturnable.setText(Constant.ORDER_DAY_LIMIT + " Days Returnable.");
            } else {
                tvReturnable.setText("Not Returnable.");
            }

            if (product.getCancelable_status().equalsIgnoreCase("1")) {
                tvCancellable.setText("Order Can Cancel Till Order " + ApiConfig.toTitleCase(product.getTill_status()) + ".");
            } else {
                tvCancellable.setText("Non Cancellable.");
            }


            viewPager.setAdapter(new SliderAdapter(sliderArrayList, activity, R.layout.lyt_detail_slider, "detail"));
            ApiConfig.addMarkers(0, sliderArrayList, mMarkersLayout, getContext());


            if (priceVariationslist.size() == 1) {
                spinner.setVisibility(View.GONE);
                lytmainprice.setEnabled(false);
                priceVariation = priceVariationslist.get(0);
                session.setData(Constant.PRODUCT_VARIANT_ID, "" + 0);

                SetSelectedData(priceVariation);
            }

            if (!product.getIndicator().equals("0")) {
                imgIndicator.setVisibility(View.VISIBLE);
                if (product.getIndicator().equals("1"))
                    imgIndicator.setImageResource(R.drawable.veg_icon);
                else if (product.getIndicator().equals("2"))
                    imgIndicator.setImageResource(R.drawable.non_veg_icon);
            }
            CustomAdapter customAdapter = new CustomAdapter();
            spinner.setAdapter(customAdapter);

            webDescription.setVerticalScrollBarEnabled(true);
            webDescription.loadDataWithBaseURL("", product.getDescription(), "text/html", "UTF-8", "");
            webDescription.setBackgroundColor(getResources().getColor(R.color.white));
            txtProductName.setText(product.getName());

            if (product.getTax_percentage().length() > 0 && !product.getTax_percentage().isEmpty()) {
                tvTaxPercent.setText(product.getTax_percentage() + "% Tax Applicable on Product Price");
            } else {
                tvTaxPercent.setVisibility(View.GONE);
            }


            spinner.setSelection(vpos);

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {
                }

                @Override
                public void onPageSelected(int position) {
                    ApiConfig.addMarkers(position, sliderArrayList, mMarkersLayout, getContext());
                }

                @Override
                public void onPageScrollStateChanged(int i) {
                }
            });

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    priceVariation = product.getPriceVariations().get(i);
                    vpos = i;
                    session.setData(Constant.PRODUCT_VARIANT_ID, "" + i);
                    SetSelectedData(priceVariation);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });

            scrollView.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (typePurchase == PaymentFragment.TypePurchase.PACK) {
            pdSubscribeButton.setVisibility(View.GONE);
            btnCart.setVisibility(View.GONE);

            pdSubscribeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (session.isUserLoggedIn()) {
                        Paper.init(activity);
                        Paper.book().write(Constant.PRODUCT, product);


                        startActivity(new Intent(v.getContext(), PackProductActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));

                    } else {
                        activity.startActivity(new Intent(v.getContext(), LoginActivity.class).putExtra("fromto", "subscribe"));

                    }

                }
            });

        } else {
            pdSubscribeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (session.isUserLoggedIn()) {
                        Paper.init(activity);
                        Paper.book().write(Constant.PRODUCT, product);
                        startActivity(new Intent(v.getContext(), SubscribeActivity.class));
                    } else {
                        activity.startActivity(new Intent(v.getContext(), LoginActivity.class).putExtra("fromto", "subscribe"));

                    }

                }
            });

        }

    }


    @Override
    public void onResume() {
        super.onResume();

        if (typePurchase == PaymentFragment.TypePurchase.PACK) {

            Pack pack = Paper.book().read(Constant.PACK_DETAILS);
            Constant.TOOLBAR_TITLE = getResources().getString(R.string.rupee_symbol)
                    + " " + pack.getPackValue() + " " + getResources().getString(R.string.pack);

        } else {
            Constant.TOOLBAR_TITLE = getString(R.string.app_name);

        }
        activity.invalidateOptionsMenu();
        hideKeyboard();
    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(root.getApplicationWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("SetTextI18n")
    public void SetSelectedData(PriceVariation priceVariation) {


        Paper.init(activity);
        if (priceVariation != null) {

            if (Double.parseDouble(priceVariation.getDiscounted_price())==Double.parseDouble("1")) {

                Paper.book().write(Constant.PRODUCT_VARIATION, product.getPriceVariations().get(1));
                spannableString = new SpannableString(getString(R.string.mrp) + Constant.SETTING_CURRENCY_SYMBOL + product.getPriceVariations().get(1).getPrice());
                spannableString.setSpan(new StrikethroughSpan(), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                txtOriginalPrice.setText(spannableString);
                double diff = Double.parseDouble(product.getPriceVariations().get(1).getPrice()) - Double.parseDouble(product.getPriceVariations().get(1).getProductPrice());
                txtDiscountedPrice.setText(getString(R.string.you_save) + Constant.SETTING_CURRENCY_SYMBOL + diff + product.getPriceVariations().get(1).getDiscountpercent());
                txtMeasurement.setText(" ( " + priceVariation.getMeasurement() + priceVariation.getMeasurement_unit_name() + " ) ");
                txtPrice.setText(getString(R.string.offer_price) + Constant.SETTING_CURRENCY_SYMBOL + priceVariation.getProductPrice());
                txtstatus.setText(priceVariation.getServe_for());


            } else {

                Paper.book().write(Constant.PRODUCT_VARIATION, priceVariation);

                spannableString = new SpannableString(getString(R.string.mrp) + Constant.SETTING_CURRENCY_SYMBOL + priceVariation.getPrice());
                spannableString.setSpan(new StrikethroughSpan(), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                txtOriginalPrice.setText(spannableString);
                double diff = Double.parseDouble(priceVariation.getPrice()) - Double.parseDouble(priceVariation.getProductPrice());
                txtDiscountedPrice.setText(getString(R.string.you_save) + Constant.SETTING_CURRENCY_SYMBOL + diff + priceVariation.getDiscountpercent());
                txtMeasurement.setText(" ( " + priceVariation.getMeasurement() + priceVariation.getMeasurement_unit_name() + " ) ");
                txtPrice.setText(getString(R.string.offer_price) + Constant.SETTING_CURRENCY_SYMBOL + priceVariation.getProductPrice());
                txtstatus.setText(priceVariation.getServe_for());


            }
        }






        if (isLogin) {
//            System.out.println("priceVariation.getId()) : " + Constant.CartValues);

            if (typePurchase == PaymentFragment.TypePurchase.PACK) {
//                    String qty = Paper.book().read(Constant.QTY, "0");
//                    txtqty.setText(qty);


                if (Constant.PackCartValues.containsKey(priceVariation.getId())) {
                    txtqty.setText(Constant.PackCartValues.get(priceVariation.getId()));
                } else {
                    txtqty.setText("0");
                }


            } else {

                if (Constant.CartValues.containsKey(priceVariation.getId())) {
                    txtqty.setText(Constant.CartValues.get(priceVariation.getId()));
                } else {
                    txtqty.setText(priceVariation.getCart_count());
                }
            }
        } else {
            txtqty.setText(databaseHelper.CheckOrderExists(priceVariation.getId(), priceVariation.getProduct_id()));
        }

        if (priceVariation.getServe_for().equalsIgnoreCase(Constant.SOLDOUT_TEXT)) {
            txtstatus.setVisibility(View.VISIBLE);
            lytqty.setVisibility(View.GONE);
        } else {
            txtstatus.setVisibility(View.GONE);
            lytqty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);


        if (typePurchase == PaymentFragment.TypePurchase.PACK) {
            menu.findItem(R.id.toolbar_cart).setVisible(false);
            menu.findItem(R.id.toolbar_sort).setVisible(false);
            menu.findItem(R.id.toolbar_search).setVisible(false);
        } else {
            menu.findItem(R.id.toolbar_cart).setVisible(true);
            menu.findItem(R.id.toolbar_sort).setVisible(false);
            menu.findItem(R.id.toolbar_search).setVisible(true);
            menu.findItem(R.id.toolbar_cart).setIcon(ApiConfig.buildCounterDrawable(Constant.TOTAL_CART_ITEM, R.drawable.ic_cart, activity));


        }

    }

    @Override
    public void onPause() {
        super.onPause();
        AddMultipleProductInCart(session, activity, Constant.CartValues);
    }

    public class ShareProduct extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                Bitmap bitmap = null;
                URL url = null;
                url = new URL(product.getImage());
                bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.US);
                Date now = new Date();
                File file = new File(activity.getExternalCacheDir(), formatter.format(now) + ".png");
                FileOutputStream fos = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.flush();
                fos.close();

                Uri uri = FileProvider.getUriForFile(activity, activity.getPackageName() + ".provider", file);

                String message = product.getName() + "\n";
                message = message + Constant.share_url + "itemdetail/" + product.getId() + "/" + product.getSlug();
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                intent.setDataAndType(uri, activity.getContentResolver().getType(uri));
                intent.putExtra(Intent.EXTRA_STREAM, uri);
                intent.putExtra(android.content.Intent.EXTRA_TEXT, message);
                startActivity(Intent.createChooser(intent, getString(R.string.share_via)));

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }
    }

    public class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return product.getPriceVariations().size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @SuppressLint({"ViewHolder", "SetTextI18n"})
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.lyt_spinner_item, null);
            TextView measurement = view.findViewById(R.id.txtmeasurement);
            TextView price = view.findViewById(R.id.txtprice);

            PriceVariation extra = product.getPriceVariations().get(i);
            measurement.setText(extra.getMeasurement() + " " + extra.getMeasurement_unit_name());
            price.setText(Constant.SETTING_CURRENCY_SYMBOL + extra.getProductPrice());

            if (extra.getServe_for().equalsIgnoreCase(Constant.SOLDOUT_TEXT)) {
                measurement.setTextColor(getResources().getColor(R.color.red));
                price.setTextColor(getResources().getColor(R.color.red));
            } else {
                measurement.setTextColor(getResources().getColor(R.color.black));
                price.setTextColor(getResources().getColor(R.color.black));
            }

            return view;
        }


    }


    private void getLeftPrice(Pack pack, GetLeftPrice getLeftPrice) {

        final int[] price = {0};
        ArrayList<Pack> packs = new ArrayList<>();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", session.getData(Constant.ID));


        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                try {
                    packs.addAll(Utils.decryptData(new JSONObject(message), Pack.class));

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        packs.stream().filter(
                                pack1 -> {

                                    if (pack1.getId().equals(pack.getId())) {

                                        getLeftPrice.onSuccess(String.valueOf(pack1.getLeftPrice()));

//
//                                        binding.toolbar.tbDaysLeftText.setText(
//                                                "Left Amount\n"+leftPrice
//                                        );
                                        return true;
                                    }
                                    return false;
                                }
                        ).collect(Collectors.toList());
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, activity, Constant.GET_PACKS, params, true);

    }

    private void addProductInPack(Pack pack, PriceVariation extra, String leftPrice, String userid, ImageButton imgAdd, int count, TextView countText) {
        HashMap<String, String> map = new HashMap<>();
        map.put(Constant.PACK_ID, pack.getId());
        map.put(Constant.PRODUCT_ID, extra.getProduct_id());
        map.put(Constant.USER_ID, userid);
        map.put(Constant.USER_ID, userid);
        map.put("leftprice", leftPrice);

        if (Double.parseDouble(leftPrice) >= Double.parseDouble(extra.getDiscounted_price())) {

            ApiConfig.RequestToVolley(new VolleyCallback() {
                @Override
                public void onSuccess(boolean result, String message) {


                    Toast.makeText(activity, "Product added", Toast.LENGTH_SHORT).show();
                    getLeftPrice(pack, new GetLeftPrice() {
                        @Override
                        public void onSuccess(String price) {
                            imgAdd.setEnabled(true);


                            if (Integer.parseInt(price) >= 0) {

                                AddCustomProductActivity.binding.toolbar.tbDaysLeftText.setText(
                                        activity.getResources().getString(R.string.rupee_symbol) + " " + price + "\n"
                                                + activity.getResources().getString(R.string.left));


                            }

                        }
                    });


                }
            }, activity, Constant.ADD_VARIABLE_PRODUCT, map, true);

        } else {
            imgAdd.setEnabled(true);
            count--;
            countText.setText(String.valueOf(count));
            Toast.makeText(activity, activity.getResources().getString(R.string.product_amount_more), Toast.LENGTH_SHORT).show();
        }
    }

    private void removeProductFromPack(Pack pack, PriceVariation extra, String user_ID, ImageButton minusButton) {

        HashMap<String, String> map = new HashMap<>();
        map.put(Constant.PACK_ID, pack.getId());
        map.put(Constant.USER_ID, user_ID);

        ArrayList<Pack> packs = new ArrayList<>();
        ArrayList<Pack.PackProduct> packProducts = new ArrayList<>();


        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {


                try {

                    packs.addAll(Utils.decryptData(new JSONObject(message), Pack.class));

                    for (Pack pack1 : packs) {

                        packProducts.addAll(pack1.getProduct());

                    }

                    for (Pack.PackProduct packProduct : packProducts) {
                        if (extra.getProduct_id().equals(packProduct.getId())) {

                            deleteProduct(packProduct.getVariable_id(), new CallGetLeftPrice() {
                                @Override
                                public void onSuccess(String result) {

                                    getLeftPrice(pack, new GetLeftPrice() {
                                        @Override
                                        public void onSuccess(String price) {
                                            minusButton.setEnabled(true);


                                            try {

                                                if (Integer.parseInt(price) >= 0) {

                                                    AddCustomProductActivity.binding.toolbar.tbDaysLeftText.setText(
                                                            getResources().getString(R.string.rupee_symbol) + " " + price + "\n"
                                                                    + getResources().getString(R.string.left));
                                                }
                                            } catch (Exception e) {


                                            }


                                        }
                                    });
                                }


                            });
                            break;
                        } else {
                            //    Toast.makeText(getContext(), "Product not added in pack", Toast.LENGTH_SHORT).show();

                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, activity, Constant.GET_VARIABLE_PRODUCTS, map, true);

    }

    private void deleteProduct(String variable_id, CallGetLeftPrice callGetLeftPrice) {

        HashMap<String, String> params = new HashMap<>();
        params.put("delete_variable_product", "1");
        params.put(Constant.ID, variable_id);


        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                Toast.makeText(getContext(), "Product Cleared", Toast.LENGTH_SHORT).show();
                callGetLeftPrice.onSuccess("done");

            }
        }, activity, Constant.DELETE_PRODUCT_VARIABLE, params, true);


    }


}

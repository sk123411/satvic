package store.satvicday.shop.activity.checkout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.razorpay.PaymentResultListener;
import com.shreyaspatil.easyupipayment.listener.PaymentStatusListener;
import com.shreyaspatil.easyupipayment.model.TransactionDetails;
import com.wangsun.upi.payment.UpiPayment;
import com.wangsun.upi.payment.model.PaymentDetail;

import org.jetbrains.annotations.NotNull;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.BackPressedOnPackSecond;
import store.satvicday.shop.adapter.AllPacksFragment;
import store.satvicday.shop.fragment.CheckoutFragment;
import store.satvicday.shop.fragment.PaymentFragment;
import store.satvicday.shop.fragment.WalletTransactionFragment;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;

public class CheckoutActivity extends LocalizationActivity implements PaymentResultListener, PaymentStatusListener {

    public static FragmentManager fm;
    private Toolbar toolbar;
    private Session session;
    private LinearLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getSharedPreferences("language", MODE_PRIVATE);
        setLanguage(sharedPreferences.getString("LANG", "en"));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Checkout");


        Paper.init(this);
        session = new Session(this);
        TextView daysLeftText = findViewById(R.id.tbDaysLeftText);
        daysLeftText.setVisibility(View.GONE);
        rootLayout = findViewById(R.id.rootLayout);

        fm = getSupportFragmentManager();



        if (getIntent() != null) {
            if (getIntent().getStringExtra("from").equals("home")) {

                AllPacksFragment allPacksFragment = new AllPacksFragment();
                fm.beginTransaction().add(
                        R.id.container, allPacksFragment
                ).addToBackStack(null).commit();


            } else if (getIntent().getStringExtra("from").equals("member")) {

                PaymentFragment paymentFragment = new PaymentFragment();
                Bundle bundle = new Bundle();
                paymentFragment.setArguments(bundle);
                fm.beginTransaction().add(R.id.container,
                        paymentFragment, null).addToBackStack(null).commit();


            } else {

                CheckoutFragment paymentFragment = new CheckoutFragment();
                Bundle bundle = new Bundle();
                paymentFragment.setArguments(bundle);
                fm.beginTransaction().add(R.id.container,
                        paymentFragment, null).addToBackStack(null).commit();
            }


        }



    }








    @Override
    public void onPaymentSuccess(String s) {

        try {
            Paper.init(this);
            PaymentFragment.razorPayId = s;

            boolean customTransaction = Paper.book().read(Constant.CUSTOM_TRANSACTION, false);
            if (!customTransaction) {

            } else {
                PaymentFragment.TypePurchase typePurchase = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE, PaymentFragment.TypePurchase.MEMBERSHIP);

                if (typePurchase == PaymentFragment.TypePurchase.MEMBERSHIP) {
                    new PaymentFragment().purchaseMembership(CheckoutActivity.this,
                            PaymentFragment.paymentMethod, PaymentFragment.razorPayId);
                } else if (typePurchase == PaymentFragment.TypePurchase.SUBSCRIBE) {
                    new PaymentFragment().purchaseSubscription(CheckoutActivity.this,
                            PaymentFragment.paymentMethod, PaymentFragment.razorPayId);
                } else if (typePurchase == PaymentFragment.TypePurchase.PACK) {
                    new PaymentFragment().purchasePack(CheckoutActivity.this,
                            PaymentFragment.paymentMethod, PaymentFragment.razorPayId);
                }


            }
        } catch (Exception e) {
            Log.d("onPaymentSuccess", ":::" + s);

        }


    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(getApplicationContext(), "Payment error occurred", Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onTransactionCancelled() {


        Toast.makeText(getApplicationContext(), "Transaction cancelled", Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onTransactionCompleted(@NotNull TransactionDetails transactionDetails) {


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        fragment.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.toolbar_cart).setVisible(false);
        menu.findItem(R.id.toolbar_search).setVisible(false);
        menu.findItem(R.id.toolbar_cart).setIcon(ApiConfig.buildCounterDrawable(Constant.TOTAL_CART_ITEM, R.drawable.ic_cart, this));


        invalidateOptionsMenu();

        if (fm.getBackStackEntryCount() > 0) {

           toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setTitle(Constant.TOOLBAR_TITLE);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

          toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                    fm.popBackStack();

                }
            });

        } else {

            toolbar.setNavigationIcon(R.drawable.ic_back);
           toolbar.setTitle(getString(R.string.app_name));
            onBackPressed();


        }



        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        menu.findItem(R.id.toolbar_cart).setVisible(false);
        menu.findItem(R.id.toolbar_search).setVisible(false);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                // onBackPressed();
                if (fm.getBackStackEntryCount() == 0) {
                    onBackPressed();
                }
        }

        return false;
    }


    public static void initializeUPI(AppCompatActivity activity, PaymentDetail payment) {

        new UpiPayment(activity)
                .setPaymentDetail(payment)
                .setUpiApps(UpiPayment.getExistingUpiApps(activity))
                .setCallBackListener(new UpiPayment.OnUpiPaymentListener() {
                    @Override
                    public void onSuccess(@NotNull com.wangsun.upi.payment.model.TransactionDetails transactionDetails) {

                        Paper.init(activity);
                        PaymentFragment.TypePurchase customTransaction = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE,
                                PaymentFragment.TypePurchase.MEMBERSHIP);

                        try {
                            PaymentFragment.upiPayId = transactionDetails.getTransactionRefId();
                            if (customTransaction == PaymentFragment.TypePurchase.REGULAR) {

                                if (WalletTransactionFragment.payFromWallet) {
                                    WalletTransactionFragment.payFromWallet = false;
                                    new WalletTransactionFragment().
                                            AddWalletBalance(activity,
                                                    new Session(activity),
                                                    WalletTransactionFragment.amount,
                                                    WalletTransactionFragment.msg,
                                                    transactionDetails.getTransactionRefId());
                                } else {

                                    new PaymentFragment().PlaceOrder(activity,
                                            PaymentFragment.paymentMethod,
                                            PaymentFragment.razorPayId, true,
                                            PaymentFragment.sendparams, "Success");
                                }

                            } else {
                                PaymentFragment.TypePurchase typePurchase = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE, PaymentFragment.TypePurchase.MEMBERSHIP);

                                if (typePurchase == PaymentFragment.TypePurchase.MEMBERSHIP) {
                                    new PaymentFragment().purchaseMembership(activity,
                                            PaymentFragment.paymentMethod, PaymentFragment.upiPayId);
                                } else if (typePurchase == PaymentFragment.TypePurchase.SUBSCRIBE) {
                                    new PaymentFragment().purchaseSubscription(activity,
                                            PaymentFragment.paymentMethod, PaymentFragment.upiPayId);
                                } else if (typePurchase == PaymentFragment.TypePurchase.PACK) {
                                    new PaymentFragment().purchasePack(activity,
                                            PaymentFragment.paymentMethod, PaymentFragment.upiPayId);
                                }


                            }


                        } catch (Exception e) {

                        }

                    }

                    @Override
                    public void onSubmitted(@NotNull com.wangsun.upi.payment.model.TransactionDetails transactionDetails) {
                        Toast.makeText(activity, "transaction submitted", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onError(@NotNull String s) {

                        Toast.makeText(activity, "transaction cancelled", Toast.LENGTH_SHORT).show();
                    }
                }).pay();
    }

}
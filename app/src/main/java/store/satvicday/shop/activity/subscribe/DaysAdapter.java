package store.satvicday.shop.activity.subscribe;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import store.satvicday.shop.R;
import store.satvicday.shop.databinding.DateItemBinding;

public class DaysAdapter extends RecyclerView.Adapter<DaysAdapter.ViewHolder> {

    private List<Calendar> calendarList = new ArrayList<>();
    private Context context;


    public DaysAdapter(List<Calendar> calendarList, Context context) {
        this.calendarList = calendarList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(context).inflate(R.layout.date_item,parent,false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        holder.bindData(calendarList.get(position));
    }

    @Override
    public int getItemCount() {
        return calendarList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        DateItemBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            binding = DateItemBinding.bind(itemView);
        }

        public void bindData(Calendar calendar) {

            binding.dtDays.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
            int correctMonth = calendar.get(Calendar.MONTH)+1;
            binding.dtMonth.setText(String.valueOf(correctMonth));
            binding.dtYear.setText(String.valueOf(calendar.get(Calendar.YEAR)));
        }
    }
}

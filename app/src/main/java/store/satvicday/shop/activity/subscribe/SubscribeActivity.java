package store.satvicday.shop.activity.subscribe;

import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.aminography.primecalendar.PrimeCalendar;
import com.aminography.primecalendar.civil.CivilCalendar;
import com.aminography.primecalendar.japanese.JapaneseCalendar;
import com.aminography.primecalendar.persian.PersianCalendar;
import com.aminography.primedatepicker.picker.PrimeDatePicker;
import com.aminography.primedatepicker.picker.callback.MultipleDaysPickCallback;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.LoginActivity;
import store.satvicday.shop.activity.checkout.CheckoutActivity;
import store.satvicday.shop.databinding.ActivitySubscribeBinding;
import store.satvicday.shop.fragment.PaymentFragment;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.model.Cart;
import store.satvicday.shop.model.CartItems;
import store.satvicday.shop.model.PriceVariation;
import store.satvicday.shop.model.Product;

public class SubscribeActivity extends LocalizationActivity {

    ActivitySubscribeBinding binding;
    PriceVariation priceVariation;
    private static int totalSusbcribedDays = 0;
    private static int count=1;
    private static double final_total;
    private Subscribe subscribe;
    private static String subscribeDays;
    private Session session;
    private Product product;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getSharedPreferences("language", MODE_PRIVATE);
        setLanguage(sharedPreferences.getString("LANG", "en"));
        super.onCreate(savedInstanceState);
        Paper.init(this);

        binding = ActivitySubscribeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.toolbar.tbDaysLeftText.setVisibility(View.GONE);

        binding.toolbar.toolbar.setTitle(getResources().getString(R.string.subscribe));
        setSupportActionBar(binding.toolbar.toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        totalSusbcribedDays = 0;

        session = new Session(this);
        subscribe = Paper.book().read(Constant.SUBSCRIBE_DETAILS);
        product = Paper.book().read(Constant.PRODUCT);

        priceVariation = Paper.book().read(Constant.PRODUCT_VARIATION);


        binding.sbDaysText.setText(String.valueOf(totalSusbcribedDays));

        binding.sbSubscriptionButton.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
        binding.sbSubscriptionButton.setEnabled(false);
        setProductData();






            binding.sbSubscriptionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    proceedWithSubscribe();
                }
            });



    }

    private void setProductData() {

        binding.sbproductImage.setImageUrl(
                product.getImage(),Constant.imageLoader);

        binding.sbproductName.setText(product.getName());
        binding.sbproductPrice.setText("Rs "+priceVariation.getDiscounted_price());
        binding.sbproductVariaition.setText(priceVariation.getMeasurement_unit_name());


    }


    public void selectDays(View view) {


        List<Calendar> calendars = new ArrayList<>();
        PrimeCalendar calendar = new CivilCalendar();
        calendar.add(Calendar.DAY_OF_MONTH,1);
        PrimeCalendar addtionalDay =calendar.toCivil();
        PrimeCalendar calendar31 = new CivilCalendar();
        calendar31.add(Calendar.DAY_OF_MONTH,31);
        PrimeCalendar addtional31Days =calendar31.toCivil();


        PrimeDatePicker datePicker = PrimeDatePicker.Companion.dialogWith(calendar)
                .pickMultipleDays(new MultipleDaysPickCallback() {
                    @Override
                    public void onMultipleDaysPicked(List<PrimeCalendar> multipleDays) {

                        binding.sbDateslist.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                                LinearLayoutManager.HORIZONTAL, false));


                        StringBuilder subDays=new StringBuilder();

                        for (PrimeCalendar primeCalendar:multipleDays){
                            Calendar cal = Calendar.getInstance();
                            cal.set(primeCalendar.get(Calendar.YEAR),primeCalendar.get(Calendar.MONDAY),
                                    primeCalendar.get(Calendar.DAY_OF_MONTH));
                            calendars.add(cal);


                        }


                        for (Calendar calendar1:calendars){
                            int month = calendar1.get(Calendar.MONTH)+1;
                            subDays.append(calendar1.get(Calendar.DAY_OF_MONTH)+"/"+String.valueOf(month)+"/"+
                                    calendar1.get(Calendar.YEAR)+",");
                        }


                        subscribeDays = subDays.toString();
                        subDays.deleteCharAt(subscribeDays.length()-1);
                        subscribeDays = subDays.toString();



                        binding.afterStartedDaySelected.setVisibility(View.VISIBLE);
                        binding.afterStartedDaySelected.setText(calendars.get(0).get(Calendar.DAY_OF_MONTH)+"/"+String.valueOf(
                                calendars.get(0).get(Calendar.MONTH))+"/"+ calendars.get(0).get(Calendar.YEAR));




                        binding.afterEndDaySelected.setVisibility(View.VISIBLE);
                        binding.afterEndDaySelected.setText(calendars.get(calendars.size()-1).get(Calendar.DAY_OF_MONTH)+"/"+String.valueOf(
                                calendars.get(0).get(Calendar.MONTH))+"/"+ calendars.get(0).get(Calendar.YEAR));

                        binding.sbStartButton.setVisibility(View.GONE);
                        binding.sbEndButton.setVisibility(View.GONE);




                        binding.daysDelivery.setVisibility(View.VISIBLE);

                        binding.sbDaysTotalText.setText(String.valueOf(calendars.size()));


                        binding.sbSubscriptionButton.setBackgroundColor(getResources().getColor(R.color.blue));
                        binding.sbSubscriptionButton.setEnabled(true);

                        totalSusbcribedDays = calendars.size();
                        setFinalTotal(totalSusbcribedDays,count);



                        binding.sbDaysText.setText(String.valueOf(totalSusbcribedDays));

                         Collections.sort(calendars);


                        binding.sbDateslist.setAdapter(new DaysAdapter(calendars,getApplicationContext()));



                    }
                }).minPossibleDate(addtionalDay)
                .maxPossibleDate(addtional31Days)
                .build();
        datePicker.show(getSupportFragmentManager(), "SOME_TAG");


    }

    private void setFinalTotal(final int totalSusbcribedDays, final int count) {


        if (totalSusbcribedDays<7){

            binding.sbSubscriptionButton.setEnabled(false);
            binding.sbSubscriptionButton.setBackgroundColor(getResources().getColor(
                    R.color.gray));
            binding.sbSubscriptionButton.setText(getResources().getString(
                    R.string.select_minimum_days
            ));

        }else {


            binding.sbSubscriptionButton.setEnabled(true);
            final_total = (Double.parseDouble(priceVariation.getDiscounted_price()) * count) * totalSusbcribedDays;
            binding.sbSubscriptionButton.setText(String.format(getResources().getText(R.string.buy_subscription) + " @Rs " + Constant.formater.format(final_total)));
            binding.sbSubscriptionButton.setBackgroundColor(
                    getResources().getColor(R.color.blue));



        }

    }

    public void increaseQuantity(View view) {

        if (count!=0){

            if (count>=1){

                count++;
                binding.sbUnitText.setText(String.valueOf(count));

                setFinalTotal(totalSusbcribedDays,count);

            }

        }


    }

    public void decreaseQuantity(View view) {

        if (count!=0){

            if (count>=1){
                if (count==1)
                    return;
                else
                count--;
                binding.sbUnitText.setText(String.valueOf(count));
                setFinalTotal(totalSusbcribedDays,count);

            }
        }


    }




    public void proceedWithSubscribe() {

        subscribe = new Subscribe(subscribeDays,priceVariation,String.valueOf(count),String.valueOf(final_total)
                , totalSusbcribedDays,session.getData(Constant.ID), product.getName());
        Paper.init(this);
        Paper.book().write(Constant.SUBSCRIBE_DETAILS, subscribe);
        Paper.book().write(Constant.CUSTOM_TRANSACTION_TYPE, PaymentFragment.TypePurchase.SUBSCRIBE);


        ArrayList<Cart> carts = new ArrayList<>();
        ArrayList<String> variantids = new ArrayList<>();
        ArrayList<String> qtylist = new ArrayList<>();

        int countHere = 0;

            countHere++;
            ArrayList<CartItems> cartItemsArrayList = new ArrayList<>();
            Cart cart = new Cart();
            cart.setId(String.valueOf(countHere));
            cart.setUser_id(session.getData(Constant.ID));
            cart.setProduct_id(priceVariation.getProduct_id());
            cart.setProduct_variant_id(priceVariation.getId());
            cart.setQty(String.valueOf(count));
            CartItems cartItems = new CartItems();
            cartItems.setId(priceVariation.getId());
            cartItems.setProduct_id(priceVariation.getProduct_id());
            cartItems.setPrice(priceVariation.getDiscounted_price());
            cartItems.setDiscounted_price(priceVariation.getDiscounted_price());
            cartItems.setName(product.getName());
            cartItems.setUnit(priceVariation.getStock_unit_name());
            if (product.getTax_percentage().isEmpty()){

                cartItems.setTax_percentage("0");
            }else {
                cartItems.setTax_percentage(product.getTax_percentage());
            }
            cartItems.setMeasurement(priceVariation.getMeasurement());
            cartItems.setImage(product.getImage());
            cartItemsArrayList.add(cartItems);
            cart.setItems(cartItemsArrayList);
            carts.add(cart);

            variantids.add(priceVariation.getId());
            qtylist.add(subscribe.getUnit());
            Paper.book().write(Constant.CART_ITEMS, carts);

        Paper.book().write(Constant.CUSTOM_TRANSACTION_TYPE, PaymentFragment.TypePurchase.SUBSCRIBE);
        startActivity(new Intent(getApplicationContext(), CheckoutActivity.class).putExtra("from","pack"));



    }

    public void clickedDaysButton(View view) {

        Toast.makeText(this, getResources().getString(R.string.select_date_calendar), Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        binding.sbDaysText.setText(String.valueOf(totalSusbcribedDays));

    }
}
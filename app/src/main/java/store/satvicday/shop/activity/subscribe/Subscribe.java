package store.satvicday.shop.activity.subscribe;

import store.satvicday.shop.model.PriceVariation;

public class Subscribe {

    private String selectedDays;
    private PriceVariation priceVariation;
    private String unit;
    private String userID;
    private String finalTotal;
    private int daysCount;
    private String productName;
    private String taxAmt;


    public Subscribe(String selectedDays, PriceVariation priceVariation, String unit,
                     String finalTotal,int daysCount, String userID, String productName) {
        this.selectedDays = selectedDays;
        this.priceVariation = priceVariation;
        this.unit = unit;
        this.finalTotal = finalTotal;
        this.userID = userID;
        this.daysCount = daysCount;
        this.productName = productName;

    }


    public String getTaxAmt() {
        return taxAmt;
    }

    public void setTaxAmt(String taxAmt) {
        this.taxAmt = taxAmt;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getDaysCount() {
        return daysCount;
    }

    public void setDaysCount(int daysCount) {
        this.daysCount = daysCount;
    }

    public String getFinalTotal() {
        return finalTotal;
    }

    public void setFinalTotal(String finalTotal) {
        this.finalTotal = finalTotal;
    }

    public String getSelectedDays() {
        return selectedDays;
    }

    public void setSelectedDays(String selectedDays) {
        this.selectedDays = selectedDays;
    }

    public PriceVariation getPriceVariation() {
        return priceVariation;
    }

    public void setPriceVariation(PriceVariation priceVariation) {
        this.priceVariation = priceVariation;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}

package store.satvicday.shop.activity.packs.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import store.satvicday.shop.R;
import store.satvicday.shop.activity.packs.GetLeftPrice;
import store.satvicday.shop.activity.packs.PackProductActivity;
import store.satvicday.shop.databinding.LytPackProductItemBinding;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.packs.Pack;

public class PackProductAdapter extends RecyclerView.Adapter<PackProductAdapter.MyViewHolder> {

    private List<Pack.PackProduct> packProducts = new ArrayList<>();
    private String fixedProductIds;
    private Context context;
    private Activity activity;


    public PackProductAdapter(Context context) {
        this.context = context;

    }

    public void setProducts(List<Pack.PackProduct> packProducts){
        this.packProducts = packProducts;

    }
    public void setActivity(Activity activity){
        this.activity = activity;
    }
    public void setFixedProductIds(String fixedProductIds) {
        this.fixedProductIds = fixedProductIds;
    }





    public void setVariabledProducts(List<Pack.PackProduct> packProducts){

        this.packProducts.addAll(packProducts);
        notifyDataSetChanged();

    }



    public List<Pack.PackProduct> getProducts(){
        return packProducts;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.lyt_pack_product_item,
                parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        String[] ids = fixedProductIds.split(",");

        final Pack.PackProduct product = packProducts.get(position);


        if (product.getVariable_id()!=null){
            holder.bindVariableData(product);


        }else {
            holder.bindFixedData(product);

        }


    }

    @Override
    public int getItemCount() {
        return packProducts.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LytPackProductItemBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            binding = LytPackProductItemBinding.bind(itemView);



             }




        public void bindFixedData(Pack.PackProduct packProduct) {
            binding.ppImage.setImageUrl(Constant.PACKIMAGE_PATH+packProduct.getImage(),Constant.imageLoader);
            binding.ppProductType.setText(binding.getRoot().getResources().getString(
                    R.string.fixed));
            binding.ppProductName.setText(packProduct.getName());
            binding.ppRemoveButton.setVisibility(View.GONE);

        }

        public void bindVariableData(Pack.PackProduct packProduct) {

            binding.ppImage.setImageUrl(Constant.PACKIMAGE_PATH+packProduct.getImage(),Constant.imageLoader);
            binding.ppProductType.setText(binding.getRoot().getResources().getString(
                    R.string.variable));
            binding.ppRemoveButton.setVisibility(View.VISIBLE);
            binding.ppProductName.setText(packProduct.getName());


            binding.ppRemoveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    PackProductActivity data = (PackProductActivity)activity;

                    deleteProduct(packProduct.getVariable_id(), new CallGetLeftPrice() {
                        @Override
                        public void onSuccess(String result) {


                            packProducts.remove(packProduct);
                            notifyItemRemoved(getAdapterPosition());
                            notifyItemChanged(getAdapterPosition(),packProducts.size());


                            data.getLeftPrice(new GetLeftPrice() {
                                @Override
                                public void onSuccess(String result) {
                                    if (Integer.parseInt(result)>=0) {

                                        PackProductActivity.binding.toolbar.tbDaysLeftText
                                                .setText(
                                                        context.getResources().getString(R.string.rupee_symbol) +" "+  result+"\n"
                                                                + context.getResources().getString(R.string.left));

                                    }
                                    Constant.PACK_LEFT_PRICE = result;

                                    PackProductActivity.binding.txttotal.setText(
                                            context.getResources().getString(R.string.total)
                                            + packProducts.size() + " items");


                                }
                            });

                        }
                    });


                }
            });


        }


    }

    private void deleteProduct(String variable_id, CallGetLeftPrice callGetLeftPrice) {

        HashMap<String,String> params = new HashMap<>();
        params.put("delete_variable_product","1");
        params.put(Constant.ID,variable_id);


        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                Toast.makeText(context,"Product Cleared", Toast.LENGTH_SHORT).show();
                callGetLeftPrice.onSuccess("call");

            }
        },activity,Constant.DELETE_PRODUCT_VARIABLE,params,true);


    }

}

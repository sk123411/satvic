package store.satvicday.shop.activity.member;

import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import store.satvicday.shop.R;
import store.satvicday.shop.activity.MainActivity;
import store.satvicday.shop.activity.packs.adapter.GetPlanDetails;
import store.satvicday.shop.databinding.FragmentMembershipDerailsBinding;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.membership.Plan;


public class MembershipDetailsFragment extends Fragment {

    private FragmentMembershipDerailsBinding binding;
    private Activity activity;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentMembershipDerailsBinding.inflate(getLayoutInflater());


        Session session = new Session(getContext());
        activity = getActivity();

        MainActivity.getMembershipDetails(getActivity(), session.getData(Constant.ID),
                new GetPlanDetails() {
                    @Override
                    public void onSuccess(JSONObject result) {

                        try {
                            binding.mdPlanPrice.setVisibility(View.VISIBLE);
                            binding.mdStartDay.setVisibility(View.VISIBLE);
                            binding.mdPlanDuration.setVisibility(View.VISIBLE);
                            binding.mdEndDate.setVisibility(View.VISIBLE);

                            binding.mdEndDate.setText(getResources().getString(
                                    R.string.ending_in_zero_days
                            ));




                            if (!result.getBoolean(Constant.ERROR)) {





                                JSONObject data = result.getJSONObject(Constant.DATA);


                                Gson gson = new Gson();

                                Plan plan = gson.fromJson(data.toString(), Plan.class);
                                binding.mdPlanDuration.setText(plan.getMonth() +" " +getResources().getString(R.string.month_));
                                binding.mdPlanPrice.setText(getResources().getString(R.string.value_) + " "+plan.getPrice());
                                binding.mdStartDay.setText(getResources().getString(R.string.started_on_) + " "+result.getString("start_date"));

                                int days = 0;
                                try {
                                    String remainingDay = result.getString("remaining_day");



                                    try {
//                                        remainingDay = remainingDay.replaceAll("[^0-9]", "");

                                        days = Integer.parseInt(remainingDay.substring(0, 3));
                                    } catch (NumberFormatException e) {
                                        try {

                                            days = Integer.parseInt(remainingDay.substring(0, 2));
                                        } catch (NumberFormatException e2) {

                                            try{
                                                days =   Integer.parseInt(remainingDay.substring(0, 1));
                                            }catch (NumberFormatException e3){
                                                days = Integer.parseInt(remainingDay);

                                            }
                                        }
                                    }

                                    binding.mdEndDate.setText(activity.getResources().getString(R.string.ending_in) +" " +remainingDay);


                                    if (days > 0) {
                                        binding.mdMembershipButton.setText(getResources()
                                                .getString(R.string.membership_activated));
                                    } else {

                                        binding.mdPlanPrice.setVisibility(View.GONE);
                                        binding.mdStartDay.setVisibility(View.GONE);
                                        binding.mdPlanDuration.setVisibility(View.GONE);

                                        binding.mdEndDate.setText(getResources().getString(
                                                R.string.ending_in_zero_days
                                        ));

                                        binding.mdMembershipButton.setText(getResources()
                                                .getString(R.string.membership_expired));
                                        TextView daysLeftText = activity.findViewById(R.id.tbDaysLeftText);
                                        daysLeftText.setVisibility(View.GONE);

                                    }


                                } catch (Exception e) {
                                    e.printStackTrace();

                                    binding.mdPlanPrice.setVisibility(View.GONE);
                                    binding.mdStartDay.setVisibility(View.GONE);
                                    binding.mdEndDate.setVisibility(View.GONE);

                                    binding.mdPlanDuration.setText(getResources().getString(R.string.not_purchased_membership));
                                    binding.mdMembershipButton.setText(getResources().getString(R.string.membership_not_activated));
                                    TextView daysLeftText = activity.findViewById(R.id.tbDaysLeftText);
                                    daysLeftText.setVisibility(View.GONE);
                                }


                            } else {

                                binding.mdPlanPrice.setVisibility(View.GONE);
                                binding.mdStartDay.setVisibility(View.GONE);
                                binding.mdEndDate.setVisibility(View.GONE);

                                binding.mdPlanDuration.setText(getResources().getString(R.string.not_purchased_membership));
                                binding.mdMembershipButton.setText(getResources().getString(R.string.membership_not_activated));

                                TextView daysLeftText = activity.findViewById(R.id.tbDaysLeftText);
                                daysLeftText.setVisibility(View.GONE);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                            binding.mdPlanPrice.setVisibility(View.GONE);
                            binding.mdStartDay.setVisibility(View.GONE);
                            binding.mdEndDate.setVisibility(View.GONE);

                            binding.mdPlanDuration.setText(getResources().getString(R.string.not_purchased_membership));
                            binding.mdMembershipButton.setText(getResources().getString(R.string.membership_not_activated));

                        }


                    }
                });


        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        Constant.TOOLBAR_TITLE = getString(R.string.plan_details);
    }
}
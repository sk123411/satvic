package store.satvicday.shop.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.razorpay.PaymentResultListener;
import com.wangsun.upi.payment.UpiPayment;
import com.wangsun.upi.payment.model.PaymentDetail;
import com.wangsun.upi.payment.model.TransactionDetails;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.packs.adapter.GetPlanDetails;
import store.satvicday.shop.fragment.CartFragment;
import store.satvicday.shop.fragment.CategoryFragment;
import store.satvicday.shop.fragment.CheckoutFragment;
import store.satvicday.shop.fragment.FavoriteFragment;
import store.satvicday.shop.fragment.HomeFragment;
import store.satvicday.shop.fragment.OrderPlacedFragment;
import store.satvicday.shop.fragment.PaymentFragment;
import store.satvicday.shop.fragment.ProductDetailFragment;
import store.satvicday.shop.fragment.ProductListFragment;
import store.satvicday.shop.fragment.SearchFragment;
import store.satvicday.shop.fragment.SubCategoryFragment;
import store.satvicday.shop.fragment.TrackOrderFragment;
import store.satvicday.shop.fragment.TrackerDetailFragment;
import store.satvicday.shop.fragment.WalletTransactionFragment;
import store.satvicday.shop.fragment.confirmorder.OrderConfirmFragment;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.DatabaseHelper;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.VolleyCallback;

import static store.satvicday.shop.helper.ApiConfig.GetSettings;


public class MainActivity extends DrawerActivity implements
        OnMapReadyCallback, PaymentResultListener {

    private static final String TAG = "MAINACTIVITY";
    public static Toolbar toolbar;
    public static BottomNavigationView bottomNavigationView;
    public static Fragment active;
    public static FragmentManager fm = null;
    public static Fragment homeFragment, categoryFragment, favoriteFragment, trackOrderFragment;
    public static boolean homeClicked = false, categoryClicked = false, favoriteClicked = false, trackingClicked = false;
    public static Activity activity;
    boolean doubleBackToExitPressedOnce = false;
    Menu menu;
    public static Session session;
    DatabaseHelper databaseHelper;
    String from;
    TextView toolbarDays;
    public static UpiPayment upiPayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_main, frameLayout);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        activity = MainActivity.this;
        session = new Session(activity);
        from = getIntent().getStringExtra("from");

        databaseHelper = new DatabaseHelper(activity);
        toolbarDays = toolbar.findViewById(R.id.tbDaysLeftText);
        Paper.init(activity);
        Paper.book().write(Constant.FROM_CHECKOUT, false);
        toolbarDays.setVisibility(View.GONE);


        //Hide days left text
        if (session.isUserLoggedIn()) {

            ApiConfig.getCartItemCount(activity, session);
            Paper.book().write(Constant.CUSTOM_TRANSACTION_TYPE, PaymentFragment.TypePurchase.REGULAR);

            getMembershipDetails(activity, session.getData(Constant.ID), new GetPlanDetails() {
                @Override
                public void onSuccess(JSONObject result) {

                    try {

                        if (!result.getBoolean(Constant.ERROR)) {

                            String remainingDay = result.getString("remaining_day");
                            int days = 0;
                            try {
                                days = Integer.parseInt(remainingDay.substring(0, 3));
                            } catch (NumberFormatException e) {
                                try {
                                    days = Integer.parseInt(remainingDay.substring(0, 2));
                                } catch (NumberFormatException e2) {

                                    try {
                                        days = Integer.parseInt(remainingDay.substring(0, 1));

                                    } catch (Exception e4) {

                                        days = Integer.parseInt(remainingDay);
                                    }
                                }
                            }

                            toolbarDays.setText(getResources().getString(R.string.left_days) + days);

                            if (days > 0) {
                                session.setMemberData(Constant.MEMBER, "1");
                                toolbarDays.setVisibility(View.VISIBLE);

                            } else {
                                toolbarDays.setVisibility(View.GONE);
                                session.setMemberData(Constant.MEMBER, "0");
                            }
                        } else {
                            toolbarDays.setVisibility(View.GONE);
                            session.setMemberData(Constant.MEMBER, "0");

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            });


        } else {
            session.setMemberData(Constant.MEMBER, "0");

            toolbarDays.setVisibility(View.GONE);
            databaseHelper.getTotalItemOfCart(activity);
        }
        fm = getSupportFragmentManager();
        Bundle total = new Bundle();
        total.putString("from", "");
        homeFragment = new HomeFragment();
        categoryFragment = new CategoryFragment();
        categoryFragment.setArguments(total);

        favoriteFragment = new FavoriteFragment();
        trackOrderFragment = new TrackOrderFragment();

        if (from.equals("track_order")) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_track_order);
            active = trackOrderFragment;
            trackingClicked = true;
            homeClicked = false;
            favoriteClicked = false;
            categoryClicked = false;
            fm.beginTransaction().add(R.id.container, trackOrderFragment, "track_order").commit();
        } else if (!from.equals("splash")) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_home);
            active = homeFragment;
            homeClicked = true;
            trackingClicked = false;
            favoriteClicked = false;
            categoryClicked = false;
            fm.beginTransaction().add(R.id.container, homeFragment).commit();


        }


        switch (from) {
            case "checkout":
                bottomNavigationView.setVisibility(View.GONE);
                ApiConfig.getCartItemCount(activity, session);
                fm.beginTransaction().add(R.id.container, new CheckoutFragment()).addToBackStack(null).commit();

                break;
            case "share":
            case "product": {
                Fragment fragment = new ProductDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("vpos", getIntent().getIntExtra("vpos", 0));
                bundle.putString("id", getIntent().getStringExtra("id"));
                bundle.putString("from", "share");
                fragment.setArguments(bundle);
                fm.beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();
                break;
            }
            case "category": {
                Fragment fragment = new SubCategoryFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id", getIntent().getStringExtra("id"));
                bundle.putString("name", getIntent().getStringExtra("name"));
                fragment.setArguments(bundle);
                fm.beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();
                break;
            }
            case "order": {
                Fragment fragment = new TrackerDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("model", "");
                bundle.putString("id", getIntent().getStringExtra("id"));
                fragment.setArguments(bundle);
                fm.beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();
                break;
            }
            case "payment_success": {
                fm.beginTransaction().replace(R.id.container, new OrderConfirmFragment()).commit();
                break;
            }
            case "wallet": {

                fm.beginTransaction().add(R.id.container, new WalletTransactionFragment()).addToBackStack(null).commit();
                break;
            }
        }


        DrawerActivity.imgProfile.setImageUrl(session.getData(Constant.PROFILE), Constant.imageLoader);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:

                        try {

                            if (homeClicked) {
                                fm.beginTransaction().show(homeFragment).hide(active).commit();
                            } else {
                                fm.beginTransaction().add(R.id.container, homeFragment).
                                        show(homeFragment).hide(active).commit();
                                homeClicked = true;

                            }


                            active = homeFragment;

                            return true;

                        } catch (Exception e) {

                            startActivity(new Intent(getApplicationContext(), MainActivity.class).putExtra("from", ""));
                        }


                    case R.id.navigation_category:


                        try {


                            if (!categoryClicked) {
                                fm.beginTransaction().add(R.id.container, categoryFragment).show(categoryFragment).hide(active).commit();
                                categoryClicked = true;
                            } else {
                                fm.beginTransaction().show(categoryFragment).hide(active).commit();
                            }
                            active = categoryFragment;
                            return true;


                        } catch (Exception e) {

                            startActivity(new Intent(getApplicationContext(), MainActivity.class).putExtra("from", ""));
                        }


                    case R.id.navigation_favorite:


                        try {


                            if (!favoriteClicked) {
                                fm.beginTransaction().add(R.id.container, favoriteFragment).show(favoriteFragment).hide(active).commit();
                                favoriteClicked = true;
                            } else {
                                fm.beginTransaction().show(favoriteFragment).hide(active).commit();
                            }
                            active = favoriteFragment;
                            return true;


                        } catch (Exception e) {

                            startActivity(new Intent(getApplicationContext(), MainActivity.class).putExtra("from", ""));
                        }


                    case R.id.navigation_track_order:


                        try {


                            if (session.isUserLoggedIn()) {

                                if (!trackingClicked) {

                                    fm.beginTransaction().add(R.id.container, trackOrderFragment, "track_order").show(trackOrderFragment).hide(active).commit();
                                    trackingClicked = true;
                                } else {


                                    fm.beginTransaction().show(trackOrderFragment).hide(active).commit();
                                }
                                active = trackOrderFragment;
                            } else {
                                Toast.makeText(activity, getString(R.string.track_login_msg), Toast.LENGTH_SHORT).show();
                            }


                            return true;


                        } catch (Exception e) {

                            startActivity(new Intent(getApplicationContext(), MainActivity.class).putExtra("from", ""));
                        }


                }
                return false;
            }
        });


        bottomNavigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem item) {
                int id = item.getItemId();

                switch (id) {
                    case R.id.navigation_home:
                    case R.id.navigation_track_order:
                    case R.id.navigation_favorite:
                    case R.id.navigation_category:
                        break;
                }
            }
        });

        drawerToggle = new ActionBarDrawerToggle
                (
                        this,
                        drawer_layout, toolbar,
                        R.string.drawer_open,
                        R.string.drawer_close
                ) {
        };

        fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                ApiConfig.updateNavItemCounter(DrawerActivity.navigationView, R.id.menu_transaction_history, Session.getCount(Constant.UNREAD_TRANSACTION_COUNT, getApplicationContext()));
                ApiConfig.updateNavItemCounter(DrawerActivity.navigationView, R.id.menu_wallet_history, Session.getCount(Constant.UNREAD_WALLET_COUNT, getApplicationContext()));
                ApiConfig.updateNavItemCounter(DrawerActivity.navigationView, R.id.menu_notifications, Session.getCount(Constant.UNREAD_NOTIFICATION_COUNT, getApplicationContext()));

            }
        });

        GetSettings(activity);

    }


    @Override
    public void onBackPressed() {
        if (drawer_layout.isDrawerOpen(navigationView))
            drawer_layout.closeDrawers();
        else
            doubleBack();
    }

    public void doubleBack() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        if (fm.getBackStackEntryCount() == 0) {
            if (active != homeFragment) {

                this.doubleBackToExitPressedOnce = false;
                bottomNavigationView.setSelectedItemId(R.id.navigation_home);
                homeClicked = true;
                fm.beginTransaction().hide(active).show(homeFragment).commit();
                active = homeFragment;
            } else {
                Toast.makeText(this, getString(R.string.exit_msg), Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }

        }
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbar_cart:
                Paper.init(activity);
                Paper.book().write(Constant.CUSTOM_TRANSACTION_TYPE, PaymentFragment.TypePurchase.REGULAR);
                MainActivity.fm.beginTransaction().add(R.id.container, new CartFragment(), "cart").addToBackStack(null).commit();
                break;
            case R.id.toolbar_search:
                MainActivity.fm.beginTransaction().add(R.id.container, new SearchFragment()).addToBackStack(null).commit();
                break;
            case R.id.toolbar_logout:
                session.logoutUser(activity);
                ApiConfig.clearFCM(activity);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.main_menu, menu);


        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.toolbar_cart).setVisible(true);
        menu.findItem(R.id.toolbar_search).setVisible(true);
        menu.findItem(R.id.toolbar_cart).setIcon(ApiConfig.buildCounterDrawable(Constant.TOTAL_CART_ITEM, R.drawable.ic_cart, activity));
        invalidateOptionsMenu();


        if (fm.getBackStackEntryCount() > 0) {


            bottomNavigationView.setVisibility(View.GONE);

            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setTitle(Constant.TOOLBAR_TITLE);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);


            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Log.d("popo", "called");
                    try {
                        CheckoutFragment myFragment = (CheckoutFragment) fm.findFragmentByTag("checkout");
                        ProductListFragment productListFragment = (ProductListFragment) fm.
                                findFragmentByTag("pr");
                        TrackOrderFragment trackOrderFragment = (TrackOrderFragment) fm.
                                findFragmentByTag("track_order");


                        CartFragment cartFragment = (CartFragment) fm.
                                findFragmentByTag("cart");

                        if (productListFragment != null) {
                            productListFragment.GetData();
                        } else if (myFragment != null) {
                            myFragment.getCartData();

                        } else if (trackOrderFragment != null) {
                            trackOrderFragment.GetOrderDetails();

                        } else if (cartFragment != null) {
                            cartFragment.getCartData();
                            cartFragment.SearchRequestAll(session, activity);
                        }

                        fm.popBackStack();


                    } catch (Exception e) {
                        fm.popBackStack();


                    }
                }
            });

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    DrawerActivity.drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                }
            }, 500);
        } else {

            DrawerActivity.drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

            if (from.equals("payment_success")) {
                bottomNavigationView.setVisibility(View.GONE);

            } else {
                bottomNavigationView.setVisibility(View.VISIBLE);

            }
            toolbar.setNavigationIcon(R.drawable.ic_menu);
            toolbar.setTitle(getString(R.string.app_name));
            drawerToggle = new ActionBarDrawerToggle
                    (
                            this,
                            drawer_layout, toolbar,
                            R.string.drawer_open,
                            R.string.drawer_close
                    ) {
            };
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        LatLng latLng = new LatLng(Double.parseDouble(session.getCoordinates(Session.KEY_LATITUDE)), Double.parseDouble(session.getCoordinates(Session.KEY_LONGITUDE)));
        googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true)
                .title("Current Location"));

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(19));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode==1){
//            from = getIntent().getStringExtra("from");
//            return;
//        }

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        fragment.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        Paper.init(activity);
        try {
            if (WalletTransactionFragment.payFromWallet) {
                WalletTransactionFragment.payFromWallet = false;
                new WalletTransactionFragment().AddWalletBalance(activity,
                        new Session(activity), WalletTransactionFragment.amount,
                        WalletTransactionFragment.msg, razorpayPaymentID);
            } else {

                PaymentFragment.razorPayId = razorpayPaymentID;
                PaymentFragment.paymentMethod = "razorpay;";
                new PaymentFragment().PlaceOrder(MainActivity.this,
                        PaymentFragment.paymentMethod, PaymentFragment.razorPayId,
                        true, PaymentFragment.sendparams, "Success");


            }
        } catch (Exception e) {
            Log.d(TAG, "onPaymentSuccess  ", e);
        }
    }

    @Override
    public void onPaymentError(int code, String response) {
        try {
            if (!WalletTransactionFragment.payFromWallet) {
                new PaymentFragment().PlaceOrder(MainActivity.this, PaymentFragment.paymentMethod, "", false, PaymentFragment.sendparams, "Failed");
            }
            Toast.makeText(activity, getString(R.string.order_cancel), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.d(TAG, "onPaymentError  ", e);
        }
    }

    @Override
    protected void onResume() {
        ApiConfig.updateNavItemCounter(DrawerActivity.navigationView, R.id.menu_transaction_history, Session.getCount(Constant.UNREAD_TRANSACTION_COUNT, getApplicationContext()));
        ApiConfig.updateNavItemCounter(DrawerActivity.navigationView, R.id.menu_wallet_history, Session.getCount(Constant.UNREAD_WALLET_COUNT, getApplicationContext()));
        ApiConfig.updateNavItemCounter(DrawerActivity.navigationView, R.id.menu_notifications, Session.getCount(Constant.UNREAD_NOTIFICATION_COUNT, getApplicationContext()));
        super.onResume();


    }

    public static void getMembershipDetails(Activity activity, String id,
                                            GetPlanDetails getPlanDetails) {

        HashMap<String, String> params = new HashMap<>();
        params.put(Constant.USER_ID, id);


        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {


                try {
                    JSONObject object = new JSONObject(message);
                    getPlanDetails.onSuccess(object);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, activity, Constant.GET_MEMBERSHIP_DETAILS, params, false);

    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    public static void initializeUPI(AppCompatActivity activity, PaymentDetail payment) {

        new UpiPayment(activity)
                .setPaymentDetail(payment)
                .setUpiApps(UpiPayment.getExistingUpiApps(activity))
                .setCallBackListener(new UpiPayment.OnUpiPaymentListener() {
                    @Override
                    public void onSuccess(@NotNull TransactionDetails transactionDetails) {
                        Paper.init(activity);
                        PaymentFragment.TypePurchase customTransaction = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE,
                                PaymentFragment.TypePurchase.MEMBERSHIP);
                        try {
                            PaymentFragment.upiPayId = transactionDetails.getTransactionRefId();
                            if (customTransaction == PaymentFragment.TypePurchase.REGULAR) {

                                if (WalletTransactionFragment.payFromWallet) {
                                    WalletTransactionFragment.payFromWallet = false;
                                    new WalletTransactionFragment().
                                            AddWalletBalance(activity,
                                                    new Session(activity),
                                                    WalletTransactionFragment.amount,
                                                    WalletTransactionFragment.msg,
                                                    transactionDetails.getTransactionRefId());
                                } else {

                                    new PaymentFragment().PlaceOrder(activity,
                                            PaymentFragment.paymentMethod,
                                            PaymentFragment.upiPayId, true,
                                            PaymentFragment.sendparams, "Success");

                                }


                            } else {
                                PaymentFragment.TypePurchase typePurchase = Paper.book().read(Constant.CUSTOM_TRANSACTION_TYPE, PaymentFragment.TypePurchase.MEMBERSHIP);

                                if (typePurchase == PaymentFragment.TypePurchase.MEMBERSHIP) {
                                    new PaymentFragment().purchaseMembership(activity,
                                            PaymentFragment.paymentMethod, PaymentFragment.upiPayId);
                                } else if (typePurchase == PaymentFragment.TypePurchase.SUBSCRIBE) {
                                    new PaymentFragment().purchaseSubscription(activity,
                                            PaymentFragment.paymentMethod, PaymentFragment.upiPayId);
                                } else if (typePurchase == PaymentFragment.TypePurchase.PACK) {
                                    new PaymentFragment().purchasePack(activity,
                                            PaymentFragment.paymentMethod, PaymentFragment.upiPayId);
                                }


                            }


                        } catch (Exception e) {
                            Log.d(TAG, "onPaymentSuccess  ", e);
                        }


                    }

                    @Override
                    public void onSubmitted(@NotNull TransactionDetails transactionDetails) {
                        Toast.makeText(activity, "transaction submitted", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onError(@NotNull String s) {
                        Log.d(TAG, "onPaymentSuccess" + s);

                        Toast.makeText(activity, "transaction cancelled", Toast.LENGTH_SHORT).show();
                    }
                }).pay();
    }

    public static void initializeWalletRechargeUPI(AppCompatActivity activity, PaymentDetail payment) {

        new UpiPayment(activity)
                .setPaymentDetail(payment)
                .setUpiApps(UpiPayment.getExistingUpiApps(activity))
                .setCallBackListener(new UpiPayment.OnUpiPaymentListener() {
                    @Override
                    public void onSuccess(@NotNull TransactionDetails transactionDetails) {

                        try {

                            new WalletTransactionFragment().AddWalletBalance(activity, session, payment.getAmount(),
                                    "Wallet Recharge", payment.getTxnRefId());


                        } catch (Exception e) {


                        }


                    }

                    @Override
                    public void onSubmitted(@NotNull TransactionDetails transactionDetails) {
                        Toast.makeText(activity, "transaction submitted", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onError(@NotNull String s) {

                        Toast.makeText(activity, "transaction cancelled", Toast.LENGTH_SHORT).show();
                    }
                }).pay();
    }


}
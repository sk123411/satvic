package store.satvicday.shop.activity.packs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.aminography.primecalendar.PrimeCalendar;
import com.aminography.primecalendar.civil.CivilCalendar;
import com.aminography.primedatepicker.picker.PrimeDatePicker;
import com.aminography.primedatepicker.picker.callback.SingleDayPickCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.subscribe.DaysAdapter;
import store.satvicday.shop.databinding.ActivitySelectPackBinding;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.packs.Pack;

public class SelectPackActivity extends LocalizationActivity {

    private static int weekInput = 1;
    private ActivitySelectPackBinding binding;
    private Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getSharedPreferences("language", MODE_PRIVATE);
        setLanguage(sharedPreferences.getString("LANG", "en"));

        super.onCreate(savedInstanceState);
        binding = ActivitySelectPackBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Paper.init(this);
        Pack pack = Paper.book().read(Constant.PACK_DETAILS);

        binding.toolbar.toolbar.setTitle(getResources().getString(R.string.rupee_symbol)
                +" "+ pack.getPackValue()+" "+getResources().getString(R.string.pack));
        setSupportActionBar(binding.toolbar.toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        binding.toolbar.tbDaysLeftText.setVisibility(View.GONE);



        binding.spWeekInput.setHint(String.valueOf(weekInput));
        binding.sbpackImage.setImageUrl(pack.getImage(),Constant.imageLoader);
        binding.spWeekInput.addTextChangedListener(weekWatcher());


    }

    private TextWatcher weekWatcher(){
        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().length()>0) {
                    if (Integer.parseInt(s.toString()) > 4 || Integer.parseInt(s.toString()) < 1) {
                        binding.spWeekInput.setError(getResources().getString(R.string.minimum_days_pack));
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        return watcher;
    }



    public void selectDays(View view) {

        if (!binding.spWeekInput.getText().toString().isEmpty()){
            weekInput =  Integer.parseInt(binding.spWeekInput.getText().toString());

            if (weekInput>=1&weekInput<=4){

                openCustomCalendar();
            }else {
                binding.spWeekInput.setError(getResources().getString(R.string.minimum_days_pack));
            }

        }



    }

    private void openCustomCalendar() {

        List<Calendar> calendars = new ArrayList<>();

        PrimeCalendar primeCalendar = new CivilCalendar();
        primeCalendar.add(Calendar.DAY_OF_MONTH, 1);

        PrimeDatePicker datePicker = PrimeDatePicker.Companion.dialogWith(primeCalendar)
                .pickSingleDay(new SingleDayPickCallback() {
                    @Override
                    public void onSingleDayPicked(PrimeCalendar singleDay) {
                        calendars.clear();


                        Calendar currentDay = Calendar.getInstance();
                        currentDay.set(singleDay.get(Calendar.YEAR),singleDay.get(Calendar.MONTH),
                                singleDay.get(Calendar.DAY_OF_MONTH));
                        calendars.add(currentDay);


                        binding.proceedButton.setEnabled(true);
                        binding.proceedButton.setBackgroundColor(getResources()
                        .getColor(R.color.colorPrimary));




                        weekInput = Integer.parseInt(binding.spWeekInput.getText().toString());
                        for (int i = 1; i < weekInput ; i++) {

                            Calendar currentDay2 = Calendar.getInstance();
                            singleDay.add(Calendar.DAY_OF_MONTH, 7);
                            currentDay2.set(singleDay.get(Calendar.YEAR),singleDay.get(Calendar.MONTH),
                                    singleDay.get(Calendar.DAY_OF_MONTH));
                            calendars.add(currentDay2);
                        }

                        StringBuilder packDays=new StringBuilder();
                        String days="";

                        for (Calendar calendar1:calendars){
                            int month = calendar1.get(Calendar.MONTH)+1;
                            packDays.append(calendar1.get(Calendar.DAY_OF_MONTH)+"/"+String.valueOf(month)+"/"+
                                    calendar1.get(Calendar.YEAR)+",");
                        }


                        days = packDays.toString();
                        packDays.deleteCharAt(days.length()-1);
                        days = packDays.toString();

                        Pack pack = Paper.book().read(Constant.PACK_DETAILS);
                        pack.setPack_date(days);
                        pack.setPack_date_count(calendars.size());
                        Paper.book().write(Constant.PACK_DETAILS, pack);


                        binding.spDatesList.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                                LinearLayoutManager.HORIZONTAL,false));
                        binding.spDatesList.setAdapter(new DaysAdapter(calendars,getApplicationContext()));

                    }
                }).minPossibleDate(primeCalendar)
                .build();


        datePicker.show(getSupportFragmentManager(), "SOME_TAG");



    }

    public void proceed(View view) {


        startActivity(new Intent(view.getContext(), PackProductActivity.class));

    }

    @Override
    protected void onResume() {
        super.onResume();

        Paper.init(SelectPackActivity.this);
        session = new Session(SelectPackActivity.this);
        Pack pack = Paper.book().read(Constant.PACK_DETAILS);

        Constant.PackCartValues.clear();

        HashMap<String,String> map = new HashMap<>();
        map.put("user_id", session.getData(Constant.ID));
        map.put("delete_variable_product","1");
        map.put("pack_id",pack.getId());

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                try {
                    JSONObject jsonObject = new JSONObject(message);

                    if (!jsonObject.getBoolean(Constant.ERROR)){

//                        Toast.makeText(SelectPackActivity.this,getResources().getString(R.string.pack_product_cleared),Toast.LENGTH_SHORT).show();


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        },SelectPackActivity.this,Constant.CLEAR_VARIABLE_PRODUCTS,map,true);


    }
}
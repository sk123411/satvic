package store.satvicday.shop.activity.member;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.checkout.CheckoutActivity;
import store.satvicday.shop.databinding.ActivityMembershipBinding;
import store.satvicday.shop.fragment.PaymentFragment;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.Utils;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.helper.membership.Image;
import store.satvicday.shop.helper.membership.Plan;

public class MembershipActivity extends LocalizationActivity {

    ActivityMembershipBinding binding;
    private List<Image> images;
    private List<Plan> planList;
    private static Plan selectedPlan;
    private Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getSharedPreferences("language", MODE_PRIVATE);
        setLanguage(sharedPreferences.getString("LANG", "en"));
        super.onCreate(savedInstanceState);

        binding = ActivityMembershipBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.toolbar.toolbar.setTitle(getResources().getString(R.string.select_plan));
        setSupportActionBar(binding.toolbar.toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        binding.toolbar.tbDaysLeftText.setVisibility(View.GONE);

        Paper.init(this);


        getPlanImages();
        getPlanDetails();
        session = new Session(this);
        checkMember();

        List<Image> imagesList = Paper.book().read(Constant.MEMBERSHIP_IMAGES);

        if (imagesList!=null&&imagesList.size()>0){
            setImages();
        }


    }

    private void checkMember() {
        boolean isMember = false;
        if (session.getData(Constant.MEMBER).equals("1")){
            isMember= true;
            binding.mMembershipButton.setEnabled(false);
            binding.mMembershipButton.setBackgroundColor(getResources().getColor(
                    R.color.gray));
            binding.mMembershipButton.setText(getResources()
                    .getString(R.string.already_member));

        }else {


            isMember = false;
        }

    }


    private void getPlanDetails() {
        planList = new ArrayList<>();
        HashMap<String,String> params = new HashMap<>();

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {


                try {
                    planList = Utils.decryptData(new JSONObject(message),
                            Plan.class);
                    setPlanDetails(planList);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        },this,Constant.MEMBERSHIP_PLANS,params,true);

    }

    private void setPlanDetails(List<Plan> planList) {

        binding.mlOneMonth.setText(planList.get(0).getMonth());
        binding.mlMonthPrice.setText(getResources().getString(R.string.rupee_symbol) + " "+planList.get(0).getPrice());
        binding.mlOneMonthOriginalPrice.setText(getResources().getString(R.string.rupee_symbol) + " "+planList.get(0).getOriginal_price());

        binding.mlOneMonthOriginalPrice.setPaintFlags(
                binding.mlOneMonthOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG
        );
        binding.mlOneMonthDiscountPercent.setText(
                String.valueOf(getDiscountPercent(planList.get(0).getOriginal_price(),
                        planList.get(0).getPrice()) + "%")
        );





        binding.mlSecondMonth.setText(planList.get(1).getMonth());
        binding.mlSecondMonthPrice.setText(getResources().getString(R.string.rupee_symbol) + " "+planList.get(1).getPrice());
        binding.mlSecondMonthOriginalPrice.setText(getResources().getString(R.string.rupee_symbol) + " "+planList.get(1).getOriginal_price());
        binding.mlSecondMonthDiscountPercent.setText(
                String.valueOf(getDiscountPercent(planList.get(1).getOriginal_price(),
                        planList.get(1).getPrice()) + "%")
        );

        binding.mlSecondMonthOriginalPrice.setPaintFlags(
                binding.mlOneMonthOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG
        );
        binding.mlThreeMonth.setText(planList.get(2).getMonth());
        binding.mlThreeMonthPrice.setText(getResources().getString(R.string.rupee_symbol) + " "+planList.get(2).getPrice());
        binding.mlThreeMonthOriginalPrice.setText(getResources().getString(R.string.rupee_symbol) + " "+planList.get(2).getOriginal_price());

        binding.mlThreeMonthDiscountPercent.setText(
                String.valueOf(getDiscountPercent(planList.get(2).getOriginal_price(),
                        planList.get(2).getPrice()) + "%")
        );
        binding.mlThreeMonthOriginalPrice.setPaintFlags(
                binding.mlOneMonthOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG
        );

    }


    private int getDiscountPercent(String originalPrice, String discountPrice){
        float originalPriceMonthOne = Float.parseFloat(originalPrice);
        float discountPriceMonthOne = Float.parseFloat(discountPrice);
        float difference = (originalPriceMonthOne - discountPriceMonthOne) / originalPriceMonthOne;

        float percent = (float) difference* 100;

        return (int) percent;

    }

    private void getPlanImages() {
        images = new ArrayList<>();

        HashMap<String,String> params = new HashMap<>();

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {


                try {
                    JSONObject data = new JSONObject(message);


                    if (!data.getBoolean(Constant.ERROR)){
                        JSONArray dataArray = data.getJSONArray(Constant.DATA);

                        Gson gson = new Gson();

                        for (int i = 0; i <dataArray.length() ; i++) {


                            JSONObject image = dataArray.getJSONObject(i);
                            Image planImage = gson.fromJson(image.toString(), Image.class);
                            images.add(planImage);

                        }

                        Paper.init(MembershipActivity.this);
                        Paper.book().write(Constant.MEMBERSHIP_IMAGES, images);

                        setImages();




                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, this, Constant.PLAN_IMAGES,params,false);






    }

    private void setImages(){

        Paper.init(MembershipActivity.this);
        List<Image> savedImages = Paper.book().read(Constant.MEMBERSHIP_IMAGES);

        if (savedImages!=null&&savedImages.size()>0){

            Picasso.get().load(Constant.MAINBASEUrl+savedImages.get(0).getImage())
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(binding.mImageOne);

            Picasso.get().load(Constant.MAINBASEUrl+savedImages.get(1).getImage())
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(binding.mImageTwo);


            Picasso.get().load(Constant.MAINBASEUrl+savedImages.get(2).getImage())
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(binding.mImageThree);


        }




    }






    @SuppressLint("ResourceType")
    public void planOneSelected(View v){

        binding.mlPlanOne.setBackgroundResource(R.drawable.back_round_deep_blue);
        binding.mlPlanTwo.setBackgroundResource(R.drawable.back_round);
        binding.mlPlanThree.setBackgroundResource(R.drawable.back_round);
        selectedPlan = planList.get(0);

    }



    @SuppressLint("ResourceAsColor")
    public void planTwoSelected(View v){
        binding.mlPlanTwo.setBackgroundResource(R.drawable.back_round_deep_blue);
        binding.mlPlanOne.setBackgroundResource(R.drawable.back_round);
        binding.mlPlanThree.setBackgroundResource(R.drawable.back_round);
        selectedPlan = planList.get(1);


    }


    @SuppressLint("ResourceAsColor")
    public void planThreeSelected(View v){
        binding.mlPlanThree.setBackgroundResource(R.drawable.back_round_deep_blue);
        binding.mlPlanTwo.setBackgroundResource(R.drawable.back_round);
        binding.mlPlanOne.setBackgroundResource(R.drawable.back_round);
        selectedPlan = planList.get(2);


    }





    public void proceedWithPlan(View view) {

        if (selectedPlan!=null){
            selectedPlan.setUser_id(session.getData(Constant.ID));
            Paper.book().write(Constant.PLAN_DETAILS, selectedPlan);
        }

        Paper.init(this);
        Paper.book().write(Constant.CUSTOM_TRANSACTION_TYPE, PaymentFragment.TypePurchase.MEMBERSHIP);
        startActivity(new Intent(view.getContext(), CheckoutActivity.class).putExtra("from","member"));


    }


}
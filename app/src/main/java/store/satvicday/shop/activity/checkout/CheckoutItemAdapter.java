package store.satvicday.shop.activity.checkout;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.MainActivity;
import store.satvicday.shop.activity.packs.AddCustomProductActivity;
import store.satvicday.shop.databinding.LytCheckoutItemsBinding;
import store.satvicday.shop.fragment.CartFragment;
import store.satvicday.shop.fragment.CheckoutFragment;
import store.satvicday.shop.fragment.PaymentFragment;
import store.satvicday.shop.fragment.ProductDetailFragment;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.model.PriceVariation;
import store.satvicday.shop.model.Product;

import static store.satvicday.shop.helper.ApiConfig.AddMultipleProductInCart;

public class CheckoutItemAdapter extends RecyclerView.Adapter<CheckoutItemAdapter.MyViewHolder> {

    private ArrayList<Product> products = new ArrayList<>();
    private Context context;
    private Session session;
    private Activity activity;
    private CheckoutFragment checkoutFragment;

    public CheckoutItemAdapter(ArrayList<Product> products, Context context, CheckoutFragment checkoutFragment) {
        this.products = products;
        this.context = context;
        this.activity = (Activity) context;
        this.checkoutFragment = checkoutFragment;
    }

    @NonNull
    @Override
    public CheckoutItemAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_checkout_items,
                parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CheckoutItemAdapter.MyViewHolder holder, int position) {
        session = new Session(activity);

        final Product product = products.get(position);
        holder.bindProduct(product);
    }

    @Override
    public int getItemCount() {
        return 50;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LytCheckoutItemsBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = LytCheckoutItemsBinding.bind(itemView);
        }

        public void bindProduct(Product product) {


            Picasso.get().load(product.getImage()).
                    placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher).
                    into(binding.ckImage);

            binding.ckPrice.setText(product.getPriceVariations().get(0).getDiscounted_price());


            Paper.init(activity);
            HashMap<String,String> cartData = Paper.book().read(Constant.CARTVALUES);

            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Bundle bundle = new Bundle();
                    bundle.putString("id", product.getId());
                    bundle.putInt("vpos",0);
                    bundle.putString("from", "regular");
                    bundle.putString("coming", "checkout");

                    ProductDetailFragment productDetailFragment = new ProductDetailFragment();
                    productDetailFragment.setArguments(bundle);
                    Paper.init(activity);
                    Paper.book().write(Constant.CUSTOM_TRANSACTION_TYPE, PaymentFragment.TypePurchase.REGULAR);
                    Paper.book().write(Constant.FROM_CHECKOUT, true);

                    MainActivity.fm.beginTransaction().add(
                            R.id.container, productDetailFragment).addToBackStack(null).commit();

                }
            });


            if (session.isUserLoggedIn()) {




                if (Constant.CheckOutCartValues.containsKey(product.getPriceVariations().get(0).getId())) {
                    binding.txtqty.setText("" +
                            Constant.CheckOutCartValues.get(product.getPriceVariations().get(0).getId()));

                } else {

                    binding.txtqty.setText(product.getPriceVariations().get(0).getCart_count());
                }
            }


                binding.btnaddqty.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int count = Integer.parseInt(binding.txtqty.getText().toString());
                        binding.txtqty.setText("" + count);

                        if (count < Float.parseFloat(product.getPriceVariations().
                                get(0).getStock())) {
                            if (count < Constant.MAX_PRODUCT_LIMIT) {
                                count++;
                                binding.txtqty.setText("" + count);
                                Paper.book().write(Constant.CART_COUNT, count);

                                final PriceVariation priceVariation = product.getPriceVariations().get(0);


                                if (  Constant.CheckOutCartValues.containsKey(priceVariation.getId())) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        Constant.CheckOutCartValues.replace(priceVariation.getId(), "" + count);
                                    } else {
                                        Constant.CheckOutCartValues.remove(priceVariation.getId());
                                        Constant.CheckOutCartValues.put(priceVariation.getId(), "" + count);
                                    }
                                } else {
                                    Constant.CheckOutCartValues.put(priceVariation.getId(), "" + count);
                                }
                                AddMultipleProductInCart(session, activity, Constant.CheckOutCartValues);


                            CheckoutFragment.carts.clear();
                            checkoutFragment.getCartData();

                            }

                        }
                    }
                });

                binding.btnminusqty.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int count = Integer.parseInt(binding.txtqty.getText().toString());
                        binding.txtqty.setText("" + count);

                        if (!(count <= 0)) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                if (count != 0) {
                                    count--;
                                    binding.txtqty.setText("" + count);
                                    Paper.book().write(Constant.CART_COUNT, count);
                                }
                                final PriceVariation priceVariation = product.getPriceVariations().get(0);



                                if (Constant.CheckOutCartValues.containsKey(priceVariation.getId())) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        Constant.CheckOutCartValues.replace(priceVariation.getId(), "" + count);
                                    } else {
                                        Constant.CheckOutCartValues.remove(priceVariation.getId());
                                        Constant.CheckOutCartValues.put(priceVariation.getId(), "" + count);
                                    }
                                } else {
                                    Constant.CheckOutCartValues.put(priceVariation.getId(), "" + count);
                                }

                                AddMultipleProductInCart(session, activity, Constant.CheckOutCartValues);
                                CheckoutFragment.carts.clear();
                                checkoutFragment.getCartData();

                            }



                        }


                    }
                });
            }
        //}
    }
}

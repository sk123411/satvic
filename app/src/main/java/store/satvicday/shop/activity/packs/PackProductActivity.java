package store.satvicday.shop.activity.packs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.LoginActivity;
import store.satvicday.shop.activity.checkout.CheckoutActivity;
import store.satvicday.shop.activity.packs.adapter.PackProductAdapter;
import store.satvicday.shop.databinding.ActivityPackProductBinding;
import store.satvicday.shop.fragment.PaymentFragment;
import store.satvicday.shop.helper.ApiConfig;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.helper.Utils;
import store.satvicday.shop.helper.VolleyCallback;
import store.satvicday.shop.model.Cart;
import store.satvicday.shop.model.CartItems;
import store.satvicday.shop.model.packs.Pack;

public class PackProductActivity extends LocalizationActivity {

    private static final List<Pack.PackProduct> packProducts = new ArrayList<>();
    private static final List<Pack> packs = new ArrayList<>();

    public static ActivityPackProductBinding binding;
    private PackProductAdapter packProductAdapter;
    private Pack pack;
    private Session session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getSharedPreferences("language", MODE_PRIVATE);
        setLanguage(sharedPreferences.getString("LANG", "en"));

        binding = ActivityPackProductBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Paper.init(getApplicationContext());
        pack = Paper.book().read(Constant.PACK_DETAILS);
        binding.toolbar.toolbar.setTitle(getResources().getString(R.string.rupee_symbol)
                + " " + pack.getPackValue() + " " + getResources().getString(R.string.pack));
        setSupportActionBar(binding.toolbar.toolbar);
        session = new Session(PackProductActivity.this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        binding.packProductlist.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        packProductAdapter = new PackProductAdapter(getApplicationContext());
        packProductAdapter.setFixedProductIds(pack.getFixedProduct());

        packProductAdapter.setActivity(this);

    }

    private void getPackProductsAndLeftPrice() {
        getVariableProductsPack(new GetVariableProducts() {
            @Override
            public void Success(List<Pack.PackProduct> packProductList) {

                packProductAdapter.setProducts(pack.getProduct());

                packProductAdapter.setVariabledProducts(packProductList);
                binding.packProductlist.setAdapter(packProductAdapter);
                setTotal(packProductAdapter.getProducts());

            }
        });


        getLeftPrice(new GetLeftPrice() {
            @Override
            public void onSuccess(String result) {


                if (Integer.parseInt(result)>=0) {
                    binding.toolbar.tbDaysLeftText.setText(
                            getResources().getString(R.string.rupee_symbol) + " " + result + " \n "
                                    + getResources().getString(R.string.left));

                }
                Constant.PACK_LEFT_PRICE = result;

                if (Integer.parseInt(Constant.PACK_LEFT_PRICE) == 0) {
                    binding.lytMainTotal.setBackgroundResource(R.drawable.bg_button);
                    binding.lytMainTotal.setEnabled(true);
                } else {
                    binding.lytMainTotal.setBackgroundColor(getResources().getColor(R.color.gray));
                    binding.lytMainTotal.setEnabled(false);

                }


            }
        });


    }


    private void getVariableProductsPack(GetVariableProducts getVariableProducts) {

        List<Pack> packs = new ArrayList<>();

        HashMap<String, String> param = new HashMap<>();
        param.put(Constant.USER_ID, session.getData(Constant.ID));
        param.put(Constant.PACK_ID, pack.getId());

        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {

                try {
                    packs.addAll(Utils.decryptData(new JSONObject(message), Pack.class));

                    packProducts.clear();
                    for (Pack pack : packs) {
                        packProducts.addAll(pack.getProduct());
                    }

                    getVariableProducts.Success(packProducts);

                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, this, Constant.GET_VARIABLE_PRODUCTS, param, true);

    }


    public void getLeftPrice(GetLeftPrice getLeftPrice) {

        List<Pack> packs = new ArrayList<>();
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id", session.getData(Constant.ID));


        ApiConfig.RequestToVolley(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result, String message) {


                try {
                    packs.addAll(Utils.decryptData(new JSONObject(message), Pack.class));


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        packs.stream().filter(
                                pack1 -> {

                                    if (pack1.getId().equals(pack.getId())) {

                                        getLeftPrice.onSuccess(String.valueOf(pack1.getLeftPrice()));
                                        Paper.book().write(Constant.PACK_LEFT_PRICE, String.valueOf(pack1.getLeftPrice()));

                                        return true;
                                    }
                                    return false;
                                }
                        ).collect(Collectors.toList());
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, this, Constant.GET_PACKS, params, true);

    }


    public void proceedWithAddingProduct(View view) {


        if (session.isUserLoggedIn()) {
            Paper.init(this);
            Paper.book().write(Constant.CUSTOM_TRANSACTION_TYPE, PaymentFragment.TypePurchase.PACK);
            startActivity(new Intent(view.getContext(), AddCustomProductActivity.class).putExtra("from", "pack_cat"));

        } else {
            startActivity(new Intent(this,LoginActivity.class).putExtra("fromto", "pack"));

        }
    }

    public void proceedToCheckout(List<Pack.PackProduct> products) {
        if (session.isUserLoggedIn()) {
            boolean isWrongInPack = false;
            if (Integer.parseInt(Constant.PACK_LEFT_PRICE) == 0) {

                binding.lytMainTotal.setBackgroundResource(R.drawable.bg_button);
                binding.lytMainTotal.setEnabled(true);

                ArrayList<Cart> carts = new ArrayList<>();
                ArrayList<String> variantids = new ArrayList<>();
                ArrayList<String> qtylist = new ArrayList<>();

                int count = 0;

                for (Pack.PackProduct packProduct : products) {

                    try {
                        count++;
                        ArrayList<CartItems> cartItemsArrayList = new ArrayList<>();
                        Cart cart = new Cart();
                        cart.setId(String.valueOf(count));
                        cart.setUser_id(session.getData(Constant.ID));
                        cart.setProduct_id(packProduct.getId());
                        cart.setProduct_variant_id(packProduct.getVariants().get(0).getId());
                        cart.setQty("1");
                        CartItems cartItems = new CartItems();
                        cartItems.setId(packProduct.getVariants().get(0).getId());
                        cartItems.setProduct_id(packProduct.getId());
                        cartItems.setPrice(packProduct.getVariants().get(0).getDiscounted_price());
                        cartItems.setDiscounted_price(packProduct.getVariants().get(0).getDiscounted_price());
                        cartItems.setName(packProduct.getName());
                        cartItems.setUnit(packProduct.getVariants().get(0).getStock_unit_name());
                        cartItems.setTax_percentage("0");
                        cartItems.setMeasurement(packProduct.getVariants().get(0).getMeasurement());
                        cartItems.setImage(Constant.PACKIMAGE_PATH + packProduct.getImage());
                        cartItemsArrayList.add(cartItems);
                        cart.setItems(cartItemsArrayList);
                        carts.add(cart);

                        variantids.add(packProduct.getVariants().get(0).getId());
                        qtylist.add("1");



                    }catch (IndexOutOfBoundsException e){
                        isWrongInPack= true;
                        binding.txtcheckout.setEnabled(false);
                        binding.txtcheckout.setBackgroundColor(getResources().getColor(R.color.gray));
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.something_wrong_in_pack),Toast.LENGTH_LONG).show();


                    }


                }
                if (!isWrongInPack) {
                    Paper.book().write(Constant.CART_ITEMS, carts);
                    Paper.book().write(Constant.CUSTOM_TRANSACTION_TYPE, PaymentFragment.TypePurchase.PACK);
                    Paper.book().write(Constant.CUSTOM_TRANSACTION_TYPE, PaymentFragment.TypePurchase.PACK);
                    Paper.book().write(Constant.FROM_CHECKOUT, false);

                    pack.setVariantIdList(variantids);
                    pack.setQtyList(qtylist);
                    pack.setUser_id(session.getData(Constant.ID));
                    Paper.book().write(Constant.PACK_DETAILS, pack);


                    startActivity(new Intent(this, CheckoutActivity.class).putExtra("from", "pack"));
                }else {
                    binding.txtcheckout.setEnabled(false);
                    binding.txtcheckout.setBackgroundColor(getResources().getColor(R.color.gray));
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.something_wrong_in_pack),Toast.LENGTH_LONG).show();

                }


            } else {
                binding.txttotal.setText(R.string.left_amount_should);
                binding.lytMainTotal.setBackgroundColor(getResources().getColor(R.color.gray));
                binding.lytMainTotal.setEnabled(false);
            }


        } else {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class).putExtra("from", "drawer"));

        }


    }


    private void setTotal(List<Pack.PackProduct> productList) {

        binding.lytdelivery.setVisibility(View.GONE);
        Paper.init(this);
        pack = Paper.book().read(Constant.PACK_DETAILS);
        int total = pack.getPack_date_count() * Integer.parseInt(pack.getPackValue());
        Constant.PACK_AMOUNT_TOTAL = String.valueOf(total);
        binding.ppsubtotalTag.setText("(" + pack.getPack_date_count() + "x" +
                pack.getPackValue() + ")");

        int items =  productList.size();
        binding.txttotal.setText("Total "
                + items + " items");
        binding.txtstotal.setText(getResources().getString(R.string.rupee_symbol) + String.valueOf(total));
        binding.txtsubtotal.setText(getResources().getString(R.string.rupee_symbol) + String.valueOf(total));


        binding.lytMainTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceedToCheckout(productList);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        getPackProductsAndLeftPrice();
    }
}
package store.satvicday.shop.activity.packs.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.packs.SelectPackActivity;
import store.satvicday.shop.databinding.PackItemListBinding;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.model.packs.Pack;

public class PackCategoryAdapter extends RecyclerView.Adapter<PackCategoryAdapter.MyViewHolder> {

    private List<Pack> packs = new ArrayList<>();
    private Context context;




    public PackCategoryAdapter(List<Pack> packs, Context context) {
        this.packs = packs;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.pack_item_list,
                parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.bindData(packs.get(position));
    }

    @Override
    public int getItemCount() {
        return packs.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        PackItemListBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            binding = PackItemListBinding.bind(itemView);




             }


        private void bindData(Pack pack){


            binding.packTitle.setText("Rs " + pack.getPackValue());
            binding.packDestitle.setText(pack.getDescription1());
            binding.packDescription.setText(pack.getDescription2());
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Paper.init(context);
                    Paper.book().write(Constant.PACK_DETAILS, pack);
                    v.getContext().startActivity(new Intent(v.getContext(), SelectPackActivity.class));


                }
            });

        }
    }
}

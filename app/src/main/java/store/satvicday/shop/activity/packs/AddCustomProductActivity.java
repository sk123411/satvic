package store.satvicday.shop.activity.packs;

import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;

import io.paperdb.Paper;
import store.satvicday.shop.R;
import store.satvicday.shop.activity.BackPressedOnPackSecond;
import store.satvicday.shop.databinding.ActivityAddCustomProductBinding;
import store.satvicday.shop.fragment.CategoryFragment;
import store.satvicday.shop.fragment.ProductListFragment;
import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.helper.Session;
import store.satvicday.shop.model.packs.Pack;

public class AddCustomProductActivity extends LocalizationActivity {

    public static FragmentManager fm;
    public static ActivityAddCustomProductBinding binding;
    private Session session;
    public static BackPressedOnPackSecond backPressedOnPackSecond;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getSharedPreferences("language", MODE_PRIVATE);
        setLanguage(sharedPreferences.getString("LANG", "en"));
        super.onCreate(savedInstanceState);
        binding = ActivityAddCustomProductBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar.toolbar);
        Paper.init(this);


        Pack pack = Paper.book().read(Constant.PACK_DETAILS);
        binding.toolbar.toolbar.setTitle(Constant.TOOLBAR_TITLE);

        fm = getSupportFragmentManager();

        Bundle bundle = new Bundle();
        bundle.putString("from","pack_cat");
        CategoryFragment categoryFragment = new CategoryFragment();
        categoryFragment.setArguments(bundle);
        fm.beginTransaction().add(R.id.container, categoryFragment).addToBackStack(null).commit();

        String leftPice = Paper.book().read(Constant.PACK_LEFT_PRICE);
        binding.toolbar.tbDaysLeftText.setText(
                getResources().getString(R.string.rupee_symbol) +" "+ leftPice+"\n"
                        + getResources().getString(R.string.left));


    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.toolbar_cart).setVisible(false);
        menu.findItem(R.id.toolbar_search).setVisible(false);
        invalidateOptionsMenu();



        if (fm.getBackStackEntryCount() > 0) {

            binding.toolbar.toolbar.setNavigationIcon(R.drawable.ic_back);
            binding.toolbar.toolbar.setTitle(Constant.TOOLBAR_TITLE);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            binding.toolbar.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    try {
                        ProductListFragment myFragment = (ProductListFragment)getSupportFragmentManager().findFragmentByTag("pr");

                        if (myFragment!=null){
                            myFragment.GetData();

                        }

                        fm.popBackStack();


                    }catch (Exception e){
                        fm.popBackStack();


                    }


                }
            });

        } else {

            binding.toolbar.toolbar.setNavigationIcon(R.drawable.ic_back);
            binding.toolbar.toolbar.setTitle(getString(R.string.app_name));
            onBackPressed();


        }

        return super.onPrepareOptionsMenu(menu);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        menu.findItem(R.id.toolbar_cart).setVisible(false);
        menu.findItem(R.id.toolbar_search).setVisible(false);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                // onBackPressed();
                if (fm.getBackStackEntryCount() == 0) {
                    onBackPressed();
                }
        }
        return false;
    }
    public void navigateTopack(View view) {
        startActivity(new Intent(view.getContext(), PackProductActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }






}
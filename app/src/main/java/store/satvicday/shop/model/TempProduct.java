package store.satvicday.shop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TempProduct {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("row_order")
    @Expose
    private String rowOrder;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("tax_id")
    @Expose
    private String taxId;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private String subcategoryId;
    @SerializedName("indicator")
    @Expose
    private String indicator;
    @SerializedName("manufacturer")
    @Expose
    private String manufacturer;
    @SerializedName("made_in")
    @Expose
    private String madeIn;
    @SerializedName("return_status")
    @Expose
    private String returnStatus;
    @SerializedName("cancelable_status")
    @Expose
    private String cancelableStatus;
    @SerializedName("enabled_status")
    @Expose
    private String enabledStatus;
    @SerializedName("till_status")
    @Expose
    private String tillStatus;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("other_images")
    @Expose
    private String otherImages;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("date_added")
    @Expose
    private String dateAdded;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("variants")
    @Expose
    private List<Variant> variants = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRowOrder() {
        return rowOrder;
    }

    public void setRowOrder(String rowOrder) {
        this.rowOrder = rowOrder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getMadeIn() {
        return madeIn;
    }

    public void setMadeIn(String madeIn) {
        this.madeIn = madeIn;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getCancelableStatus() {
        return cancelableStatus;
    }

    public void setCancelableStatus(String cancelableStatus) {
        this.cancelableStatus = cancelableStatus;
    }

    public String getEnabledStatus() {
        return enabledStatus;
    }

    public void setEnabledStatus(String enabledStatus) {
        this.enabledStatus = enabledStatus;
    }

    public String getTillStatus() {
        return tillStatus;
    }

    public void setTillStatus(String tillStatus) {
        this.tillStatus = tillStatus;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOtherImages() {
        return otherImages;
    }

    public void setOtherImages(String otherImages) {
        this.otherImages = otherImages;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Variant> getVariants() {
        return variants;
    }

    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }
    public class Variant {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("product_id")
        @Expose
        private String productId;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("measurement")
        @Expose
        private String measurement;
        @SerializedName("measurement_unit_id")
        @Expose
        private String measurementUnitId;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("discounted_price")
        @Expose
        private String discountedPrice;
        @SerializedName("serve_for")
        @Expose
        private String serveFor;
        @SerializedName("stock")
        @Expose
        private String stock;
        @SerializedName("stock_unit_id")
        @Expose
        private String stockUnitId;
        @SerializedName("measurement_unit_name")
        @Expose
        private String measurementUnitName;
        @SerializedName("stock_unit_name")
        @Expose
        private String stockUnitName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMeasurement() {
            return measurement;
        }

        public void setMeasurement(String measurement) {
            this.measurement = measurement;
        }

        public String getMeasurementUnitId() {
            return measurementUnitId;
        }

        public void setMeasurementUnitId(String measurementUnitId) {
            this.measurementUnitId = measurementUnitId;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDiscountedPrice() {
            return discountedPrice;
        }

        public void setDiscountedPrice(String discountedPrice) {
            this.discountedPrice = discountedPrice;
        }

        public String getServeFor() {
            return serveFor;
        }

        public void setServeFor(String serveFor) {
            this.serveFor = serveFor;
        }

        public String getStock() {
            return stock;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }

        public String getStockUnitId() {
            return stockUnitId;
        }

        public void setStockUnitId(String stockUnitId) {
            this.stockUnitId = stockUnitId;
        }

        public String getMeasurementUnitName() {
            return measurementUnitName;
        }

        public void setMeasurementUnitName(String measurementUnitName) {
            this.measurementUnitName = measurementUnitName;
        }

        public String getStockUnitName() {
            return stockUnitName;
        }

        public void setStockUnitName(String stockUnitName) {
            this.stockUnitName = stockUnitName;
        }

    }

}




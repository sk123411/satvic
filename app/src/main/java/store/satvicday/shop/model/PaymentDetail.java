package store.satvicday.shop.model;

public class PaymentDetail {
    private String vpa_address;
    private String vpa_name;
    private String vpa_merchant_code;
    private String txrRefId;
    private String description;
    private String amount;


    public PaymentDetail(String vpa_address, String vpa_name, String vpa_merchant_code, String txrRefId, String description, String amount) {
        this.vpa_address = vpa_address;
        this.vpa_name = vpa_name;
        this.vpa_merchant_code = vpa_merchant_code;
        this.txrRefId = txrRefId;
        this.description = description;
        this.amount = amount;
    }
}

package store.satvicday.shop.model.packs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import store.satvicday.shop.helper.Constant;
import store.satvicday.shop.model.PriceVariation;

public class Pack {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("pack_value")
    @Expose
    private String packValue;
    @SerializedName("description1")
    @Expose
    private String description1;
    @SerializedName("description2")
    @Expose
    private String description2;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private String subcategoryId;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("service_tax")
    @Expose
    private String serviceTax;
    @SerializedName("fixed_product")
    @Expose
    private String fixedProduct;
    @SerializedName("variable_product")
    @Expose
    private String variableProduct;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("product")
    @Expose
    private List<PackProduct> product = null;
    @SerializedName("left_price")
    @Expose
    private Integer leftPrice;
    @SerializedName("total_price")
    @Expose
    private Integer totalPrice;
    @SerializedName("pack_date")
    @Expose
    private String pack_date;

    private int pack_date_count;
    private String user_id;



    private ArrayList<String> variantIdList;
    private ArrayList<String> qtyList;


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public ArrayList<String> getVariantIdList() {
        return variantIdList;
    }

    public void setVariantIdList(ArrayList<String> variantIdList) {
        this.variantIdList = variantIdList;
    }

    public ArrayList<String> getQtyList() {
        return qtyList;
    }

    public void setQtyList(ArrayList<String> qtyList) {
        this.qtyList = qtyList;
    }

    public int getPack_date_count() {
        return pack_date_count;
    }

    public void setPack_date_count(int pack_date_count) {
        this.pack_date_count = pack_date_count;
    }

    public String getPack_date() {
        return pack_date;
    }

    public void setPack_date(String pack_date) {
        this.pack_date = pack_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPackValue() {
        return packValue;
    }

    public void setPackValue(String packValue) {
        this.packValue = packValue;
    }

    public String getDescription1() {
        return description1;
    }

    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
    }

    public String getFixedProduct() {
        return fixedProduct;
    }

    public void setFixedProduct(String fixedProduct) {
        this.fixedProduct = fixedProduct;
    }

    public String getVariableProduct() {
        return variableProduct;
    }

    public void setVariableProduct(String variableProduct) {
        this.variableProduct = variableProduct;
    }

    public String getImage() {
        return Constant.PACKIMAGE_PATH+image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<PackProduct> getProduct() {
        return product;
    }

    public void setProduct(List<PackProduct> product) {
        this.product = product;
    }

    public Integer getLeftPrice() {
        return leftPrice;
    }

    public void setLeftPrice(Integer leftPrice) {
        this.leftPrice = leftPrice;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public class PackProduct {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("row_order")
        @Expose
        private String rowOrder;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("tax_id")
        @Expose
        private String taxId;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("category_id")
        @Expose
        private String categoryId;
        @SerializedName("subcategory_id")
        @Expose
        private String subcategoryId;
        @SerializedName("indicator")
        @Expose
        private String indicator;
        @SerializedName("manufacturer")
        @Expose
        private String manufacturer;
        @SerializedName("made_in")
        @Expose
        private String madeIn;
        @SerializedName("return_status")
        @Expose
        private String returnStatus;
        @SerializedName("cancelable_status")
        @Expose
        private String cancelableStatus;
        @SerializedName("enabled_status")
        @Expose
        private String enabledStatus;
        @SerializedName("till_status")
        @Expose
        private String tillStatus;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("other_images")
        @Expose
        private Object otherImages;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("date_added")
        @Expose
        private String dateAdded;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("variants")
        @Expose
        private List<PriceVariation> variants = null;

        @SerializedName("variable_id")
        @Expose
        private String variable_id;
        @SerializedName("tax_percentage")
        @Expose
        private String tax_percentage;
        @SerializedName("is_favorite")
        @Expose
        private boolean is_favorite;


        public String getTax_percentage() {
            return tax_percentage;
        }

        public void setTax_percentage(String tax_percentage) {
            this.tax_percentage = tax_percentage;
        }

        public boolean isIs_favorite() {
            return is_favorite;
        }

        public void setIs_favorite(boolean is_favorite) {
            this.is_favorite = is_favorite;
        }

        private String parent_id;

        public String getParent_id() {
            return parent_id;
        }

        public void setParent_id(String parent_id) {
            this.parent_id = parent_id;
        }

        public String getVariable_id() {
            return variable_id;
        }

        public void setVariable_id(String variable_id) {
            this.variable_id = variable_id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRowOrder() {
            return rowOrder;
        }

        public void setRowOrder(String rowOrder) {
            this.rowOrder = rowOrder;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTaxId() {
            return taxId;
        }

        public void setTaxId(String taxId) {
            this.taxId = taxId;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getSubcategoryId() {
            return subcategoryId;
        }

        public void setSubcategoryId(String subcategoryId) {
            this.subcategoryId = subcategoryId;
        }

        public String getIndicator() {
            return indicator;
        }

        public void setIndicator(String indicator) {
            this.indicator = indicator;
        }

        public String getManufacturer() {
            return manufacturer;
        }

        public void setManufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
        }

        public String getMadeIn() {
            return madeIn;
        }

        public void setMadeIn(String madeIn) {
            this.madeIn = madeIn;
        }

        public String getReturnStatus() {
            return returnStatus;
        }

        public void setReturnStatus(String returnStatus) {
            this.returnStatus = returnStatus;
        }

        public String getCancelableStatus() {
            return cancelableStatus;
        }

        public void setCancelableStatus(String cancelableStatus) {
            this.cancelableStatus = cancelableStatus;
        }

        public String getEnabledStatus() {
            return enabledStatus;
        }

        public void setEnabledStatus(String enabledStatus) {
            this.enabledStatus = enabledStatus;
        }

        public String getTillStatus() {
            return tillStatus;
        }

        public void setTillStatus(String tillStatus) {
            this.tillStatus = tillStatus;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Object getOtherImages() {
            return otherImages;
        }

        public void setOtherImages(Object otherImages) {
            this.otherImages = otherImages;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDateAdded() {
            return dateAdded;
        }

        public void setDateAdded(String dateAdded) {
            this.dateAdded = dateAdded;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<PriceVariation> getVariants() {
            return variants;
        }

        public void setVariants(List<PriceVariation> variants) {
            this.variants = variants;
        }

    }

}






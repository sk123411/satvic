package store.satvicday.shop.helper;


import android.annotation.SuppressLint;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import store.satvicday.shop.R;

public class Utils {

    @SuppressLint("ClickableViewAccessibility")
    public static void setHideShowPassword(final EditText edtPassword) {
        edtPassword.setTag("show");
        edtPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (edtPassword.getRight() - edtPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (edtPassword.getTag().equals("show")) {
                            edtPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_password, 0, R.drawable.ic_hide, 0);
                            edtPassword.setTransformationMethod(null);
                            edtPassword.setTag("hide");
                        } else {
                            edtPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_password, 0, R.drawable.ic_show, 0);
                            edtPassword.setTransformationMethod(new PasswordTransformationMethod());
                            edtPassword.setTag("show");
                        }
                        return true;
                    }
                }
                return false;
            }
        });
    }



    public static<T> List<T> decryptData(JSONObject data, Class<T> model) {

        List<T> datalist = new ArrayList<>();

        try {
            if (!data.getBoolean(Constant.ERROR)) {
                JSONArray dataArray = data.getJSONArray(Constant.DATA);

                Gson gson = new Gson();

                for (int i = 0; i < dataArray.length(); i++) {

                    JSONObject jsonObject = dataArray.getJSONObject(i);
                    datalist.add(gson.fromJson(jsonObject.toString(), model));

                }
                return datalist;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return datalist;
    }

    public static<T> ArrayList<T> decryptDataProducts(JSONObject data, Class<T> model, String key) {

        ArrayList<T> datalist = new ArrayList<>();

        try {
//            if (!data.getBoolean(Constant.ERROR)) {
                JSONArray dataArray = data.getJSONArray(key);

                Gson gson = new Gson();

                for (int i = 0; i < dataArray.length(); i++) {

                    JSONObject jsonObject = dataArray.getJSONObject(i);
                    datalist.add(gson.fromJson(jsonObject.toString(), model));

                }
                return datalist;

          //  }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return datalist;
    }
}
